import { BagaimanaCaraMengelolaTransaksi } from "../components/salesGuide/BagaimanaCaraMengelolaTransaksi";
import { CaraMemprosesPesanan } from "../components/salesGuide/CaraMemprosesPesanan";
import { CaraMenambahkanProduk } from "../components/salesGuide/CaraMenambahkanProduk";
import { CaraMenerimaAtauMenolakTransaksi } from "../components/salesGuide/CaraMenerimaAtauMenolakTransaksi";
import { CaraMenerimaPesanan } from "../components/salesGuide/CaraMenerimaPesanan";
import { CaraMengeditAtauMenghapusProduk } from "../components/salesGuide/CaraMengeditAtauMenghapusProduk";
import { DashboardSeller } from "../components/salesGuide/DashboardSeller";
import { HalamanProduk } from "../components/salesGuide/HalamanProduk";
import { InformasiPentingTentangProduk } from "../components/salesGuide/InformasiPentingTentangProduk";
import { LangkahmulaiBerjualanSeller } from "../components/salesGuide/LangkahmulaiBerjualanSeller";
import { MelengkapiInfoPembayaran } from "../components/salesGuide/MelengkapiInfoPembayaran";
import { MelengkapiInformasiTokoDanSeller } from "../components/salesGuide/MelengkapiInformasiTokoDanSeller";
import { MengaturJasaPengiriman } from "../components/salesGuide/MengaturJasaPengiriman";

const subTab1 = [
  {
    id: "sub-tab-1",
    title: "Langkah mulai berjualan Seller",
    content: <LangkahmulaiBerjualanSeller />,
  },
  {
    id: "sub-tab-2",
    title: "Melengkapi informasi toko dan seller",
    content: <MelengkapiInformasiTokoDanSeller />,
  },
  {
    id: "sub-tab-3",
    title: "Mengatur Jasa pengiriman",
    content: <MengaturJasaPengiriman />,
  },
  {
    id: "sub-tab-4",
    title: "Melengkapi info pembayaran",
    content: <MelengkapiInfoPembayaran />,
  },
  {
    id: "sub-tab-5",
    title: "Dashboard Seller",
    content: <DashboardSeller />,
  },
];
const subTab2 = [
  {
    id: "sub-tab-1",
    title: "Halaman Produk",
    content: <HalamanProduk />,
  },
  {
    id: "sub-tab-2",
    title: "Cara menambahkan produk",
    content: <CaraMenambahkanProduk />,
  },
  {
    id: "sub-tab-3",
    title: "Cara mengedit / menghapus produk",
    content: <CaraMengeditAtauMenghapusProduk />,
  },
  {
    id: "sub-tab-4",
    title: "Informasi penting tentang produk",
    content: <InformasiPentingTentangProduk />,
  },
];

const subTab3 = [
  {
    id: "sub-tab-1",
    title: "Bagaimana Cara Mengelola Transaksi?",
    content: <BagaimanaCaraMengelolaTransaksi />,
  },
  {
    id: "sub-tab-2",
    title: "Cara menerima pesanan",
    content: <CaraMenerimaPesanan />,
  },
  {
    id: "sub-tab-3",
    title: "Cara menerima / menolak transaksi",
    content: <CaraMenerimaAtauMenolakTransaksi />,
  },
  {
    id: "sub-tab-4",
    title: "Cara Memproses Pesanan",
    content: <CaraMemprosesPesanan />,
  },
];

export const subTabData = {
  "tab-1": subTab1,
  "tab-2": subTab2,
  "tab-3": subTab3,
};
