export const size = ["S","M","L","XL"];




export const condition = ["Baru dengan Tag","Seperti Baru","Baik","Cukup"];

export const photosText = ["Foto utama","Detail kondisi 1","Detail kondisi 2","Detail bahan"];

export const category = ["pria", "wanita", "anak"];
export const subcategory1 = ["aksesoris", "sepatu", "luaran", "bawahan", "atasan"];

export const subcategory2Pria = [
  {
    type: "aksesoris",
    value: ["ikatpinggang", "jam", "kacamata", "topi"],
    text: ["ikat pinggang", "jam", "kacamata", "topi"],
  },
  {
    type: "sepatu",
    value: ["boots", "formal", "sendal", "slipon", "sneakers", "olahraga"],
    text: ["boots", "formal", "sendal", "slipon", "sneakers", "olahraga"],
  },
  {
    type: "luaran",
    value: ["crewneck", "hoodie", "jaket", "jas", "mantel", "sweater"],
    text: ["crewneck", "hoodie", "jaket", "jas", "mantel", "sweater"],
  },
  {
    type: "bawahan",
    value: ["celanapendek", "celanapanjang", "casual", "jeans"],
    text: ["celana pendek", "celana panjang", "casual", "jeans"],
  },
  {
    type: "atasan",
    value: ["kaoslenganpendek", "kaoslenganpanjang", "kemeja", "polo"],
    text: ["kaos lengan pendek", "kaos lengan panjang", "kemeja", "polo"],
  },
];

export const subcategory2Wanita = [
  {
    type: "aksesoris",
    value: ["ikatpinggang", "jam", "kacamata", "topi"],
    text: ["ikat pinggang", "jam", "kacamata", "topi"],
  },
  {
    type: "sepatu",
    value: [
      "boots",
      "heels&wedges",
      "sendal",
      "slipon",
      "sneakers",
      "olahraga",
    ],
    text: [
      "boots",
      "heels & wedges",
      "sendal",
      "slipon",
      "sneakers",
      "olahraga",
    ],
  },
  {
    type: "luaran",
    value: [
      "cardigan",
      "crewneck",
      "hoodie",
      "jaket",
      "jas",
      "mantel",
      "sweater",
    ],
   text: [
      "cardigan",
      "crewneck",
      "hoodie",
      "jaket",
      "jas",
      "mantel",
      "sweater",
    ],
  },
  {
    type: "bawahan",
    value: ["celanapendek", "celanapanjang", "rokpendek&3/4", "rokpanjang"],
    text: ["celana pendek", "celana panjang", "rok pendek & 3/4", "rok panjang"],
  },
  {
    type: "atasan",
    value: [
      "kaoslenganpendek&3/4",
      "kaoslenganpanjang",
      "kemejadanblus",
      "gaun",
    ],
    text: [
      "kaos lengan pendek & 3/4",
      "kaos lengan panjang",
      "kemeja dan blus",
      "gaun",
    ],
  },
];

export const subcategory2Anak = [
  {
    type: "aksesoris",
    value: ["kacamata", "topi"],
    text: ["kacamata", "topi"],
  },
  {
    type: "sepatu",
    value: [
      "boots",
      "formal",
      "heels&wedges",
      "sendal",
      "slipon",
      "sneakers",
      "olahraga",
    ],
    text: [
      "boots",
      "formal",
      "heels & wedges",
      "sendal",
      "slipon",
      "sneakers",
      "olahraga",
    ],
  },
  {
    type: "luaran",
    value: [
      "cardigan",
      "crewneck",
      "hoodie",
      "jaket",
      "jas",
      "mantel",
      "sweater",
    ],
    text: [
      "cardigan",
      "crewneck",
      "hoodie",
      "jaket",
      "jas",
      "mantel",
      "sweater",
    ],
  },
  {
    type: "bawahan",
    value: ["celanapendek", "celanapanjang", "rokpendek&3/4", "rokpanjang"],
    text: ["celana pendek", "celana panjang", "rok pendek & 3/4", "rokpanjang"],
  },
  {
    type: "atasan",
    value: [
      "kaoslenganpendek",
      "kaoslenganpanjang",
      "kemejadanblus",
      "gaun",
      "polo",
    ],
    text: [
      "kaos lengan pendek",
      "kaos lengan panjang",
      "kemeja dan blus",
      "gaun",
      "polo",
    ],
  },
];
        
        