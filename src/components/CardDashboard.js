import React from 'react'

export const CardDashboard = ({title, total, img}) => {
  return (
    <div className="w-full bg-white rounded-md text-center py-4 shadow-md ">
      <img src={img} className="w-12 inline-block" alt="" />
      <p className="capitalize mt-3 ">{title}</p>
      <p className="capitalize font-light text-gray-500">{total}</p>
    </div>
  );
}
