import React from 'react'

export const Card = (props) => {
  return <div className={`bg-white rounded-md shadow-md p-6 ${props.className}`}>{props.children}</div>;
}
