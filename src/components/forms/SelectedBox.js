import React from 'react'

export const SelectedBox = (props) => {
  
  return (
    <div>
      <select
        {...props}
        disabled={props.disabled || !props.datas}
        className={`w-full mt-2 border-[1px] border-[#CFCFCF] p-2 rounded-md placeholder:text-[#CFCFCF] focus:outline-none focus:ring-1 block focus:ring-[#4DB680] text-sm ${
          props.className
        } ${props.disabled || !props.datas ? "cursor-not-allowed" : ""} `}
        defaultValue={props.selected}
      >
        {(props.default) && (
          <option value="">
             anda belum memilih
          </option>
        )}
        {props.datas &&
          props.datas.map((data, idx) => (
            <option key={idx}  value={data}>
              {data}
            </option>
          ))}
      </select>
    </div>
  );
}
