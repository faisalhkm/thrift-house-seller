import React from 'react'

export const InputTextGroup = (props) => {
  return (
    <div className="relative">
      {props.iconposition === "left" && (
        <div className="absolute text-[#CFCFCF] text-lg flex items-center top-0 left-0 h-full px-4 rounded-tl-md border-[1px] border-[#CFCFCF] rounded-bl-md bg-[#F2F2F2] w-14">
          <p>{props.icon}</p>
        </div>
      )}
      <input
        {...props}
        type={`${props.type ? props.type : "text"}`}
        className={`${
          props.iconposition === "left"
            ? "input-group-p-left"
            : "input-group-p-right"
        } w-full mt-2 border-[1px] border-[#CFCFCF] p-2 rounded-md placeholder:text-[#CFCFCF] focus:outline-none focus:ring-1 block focus:ring-[#4DB680] text-sm ${props.className}`}
      />
      {props.iconposition === "right" && (
        <div className="absolute text-[#CFCFCF] flex items-center top-0 right-0 h-full px-4 rounded-tr-md border-[1px] border-[#CFCFCF] rounded-br-md bg-[#F2F2F2] w-14">
          <p>{props.icon}</p>
        </div>
      )}
    </div>
  );
};
