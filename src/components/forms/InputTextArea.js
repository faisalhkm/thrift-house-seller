import React from 'react'

export const InputTextArea = (props) => {
  return (
    <textarea
      {...props}
      type="text"
      className={`w-full mt-2 border-[1px] border-[#CFCFCF] p-2 rounded-md placeholder:text-[#CFCFCF] focus:outline-none focus:ring-1 block focus:ring-[#4DB680] text-sm ${props.className}`}
    ></textarea>
  );
}
