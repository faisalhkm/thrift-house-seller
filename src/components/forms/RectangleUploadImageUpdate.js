import React, { useState } from 'react'
import buttonCircleClose from "../../assets/icon/button-circle-close.svg";

export const RectangleUploadImageUpdate = ({ title, onChange, imgId, onCancel, value }) => {
  const [dataImg, setDataImg] = useState("");

  const handleUploadClose = (e) => {
    setDataImg("");
  };

  const handleUploadChange = (e) => {
    setDataImg(e.target.files[0]);
    onChange(imgId, e.target.files[0]);
  };

  const handleCancel = () => {
    onCancel(imgId);
  };
  
  return (
    <>
      <div className="relative text-center">
        {/* Disaat Image Sudah Terisi */}
        {dataImg && (
          <div>
            <img
              src={window.URL.createObjectURL(dataImg)}
              className="h-52 w-full rounded-md"
              alt=""
            />
            <div
              className="absolute -top-[10px] -right-[10px] cursor-pointer"
              onClick={handleUploadClose}
            >
              <img src={buttonCircleClose} alt="" onClick={handleCancel} />
            </div>
          </div>
        )}

        {/* Disaat Image Masih Kosong */}
        {!dataImg && (
          <div>
            <label
              htmlFor={`actual-btn${imgId}`}
              className="h-52 w-full border-2 rounded-md border-dashed flex justify-center items-center"
            >
              <img
                src={value}
                className="h-52 w-full opacity-[0.7] cursor-pointer"
                alt=""
              />
            </label>

            <input
              type="file"
              id={`actual-btn${imgId}`}
              hidden
              onChange={handleUploadChange}
            />
          </div>
        )}
        <p className="text-sm mt-2">{title}</p>
      </div>
    </>
  );
};
