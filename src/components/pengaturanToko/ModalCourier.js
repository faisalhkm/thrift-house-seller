import { useRef, useState } from "react";
import { Icon } from "@iconify/react";
import axios from "axios";
import {useSelector } from "react-redux";

/* @Props
isCourierOpen: handle if modal is open or close
setIsCourierOpen: the setstate of isCourierOpen
*/
const ModalCourier = ({ isCourierOpen, setIsCourierOpen, setSelectedCourier, selectedCourier, setKurir }) => {
  const closeModalOutside = useRef(); // handle close if click outside
  const [tempSelectedCourier, setTempSelectedCourier] = useState(""); // state that store which courier is selected
  const {store_id} = useSelector((state) => state.user);


  // handle submit when choose courier on modal
  const submitSelectedCourierHandler = (e) => {
    e.preventDefault()
    setSelectedCourier(tempSelectedCourier);
    axios.put(`https://thrifthouse.herokuapp.com:443/api/v1/stores/${store_id}/delivery-service`,
    {
      "deliveryService": tempSelectedCourier
    })
    .then(res => {
      console.log(res)
      setKurir(tempSelectedCourier)
    }).catch(err=>{
      console.log(err)
    })
    setIsCourierOpen(false);
  };

  return (
    <div
      className={`modal ${
        isCourierOpen ? "block" : "hidden"
      } justify-center items-center fixed z-10 left-36 top-0 w-full h-full overflow-auto bg-[#1F1F1F]/60 py-[145px]`}
      ref={closeModalOutside}
      onClick={(e) =>
        e.target === closeModalOutside.current && setIsCourierOpen(false)
      }
    >
      <div className="modalContent bg-white max-w-[947px] mx-6 md:mx-auto px-6 md:px-20 py-10 rounded-lg relative">
        <Icon
          icon="ph:x-circle-fill"
          height="40"
          className="absolute right-5 md:right-10 top-7 cursor-pointer"
          onClick={() => setIsCourierOpen(false)}
        />

        <>
          <h3 className="font-bold text-sm md:text-2xl text-center mb-3">
            Pilih Jasa Pengiriman
          </h3>
          <p className="text-[#8F8F8F] text-center text-lg mb-10">
            Pilih layanan kurir yang ingin kamu gunakan
          </p>

          <div className="thisiscouriercontainer mb-16">
            <div
              className={`thisiscourierpengiriman mb-10 border-[1px] p-4 rounded-lg flex flex-col md:flex-row items-center cursor-pointer hover:border-gogreen ${
                tempSelectedCourier === "jne"
                  ? "border-gogreen"
                  : "border-[#CECFD7]"
              }`}
              onClick={() => setTempSelectedCourier("jne")}
            >
              <div className="md:mr-9 w-[234px] h-[120px]">
                <img
                  src="/images/jne.png"
                  alt="jne"
                  className="w-full h-full object-contain"
                />
              </div>
              <div className="flex-1">
                <p className="mb-2 font-bold text-xl">
                  Jalur Nugraha Ekakurir (JNE)
                </p>
                <p className="text-lg">
                  Ongkos Kirim Ekonomis (OKE) | Layanan Reguler (REG) | Super
                  Speed (SPS) | Yakin Esok Sampai (YES)
                </p>
              </div>
            </div>
            <div
              className={`thisiscourierpengiriman mb-10 border-[1px] p-4 rounded-lg flex flex-col md:flex-row items-center cursor-pointer hover:border-gogreen ${
                tempSelectedCourier === "pos"
                  ? "border-gogreen"
                  : "border-[#CECFD7]"
              }`}
              onClick={() => setTempSelectedCourier("pos")}
            >
              <div className="md:mr-9 w-[234px] h-[120px]">
                <img
                  src="/images/pos.png"
                  alt="pos"
                  className="w-full h-full object-contain"
                />
              </div>
              <div className="flex-1">
                <p className="mb-2 font-bold text-xl">POS INDONESIA</p>
                <p className="text-lg">Pos Kilat Khusus (Reguler) - 2-4 hari</p>
              </div>
            </div>
            <div
              className={`thisiscourierpengiriman border-[1px] p-4 rounded-lg flex flex-col md:flex-row items-center cursor-pointer hover:border-gogreen ${
                tempSelectedCourier === "tiki"
                  ? "border-gogreen"
                  : "border-[#CECFD7]"
              }`}
              onClick={() => setTempSelectedCourier("tiki")}
            >
              <div className="md:mr-9 w-[234px] h-[120px]">
                <img
                  src="/images/tiki.png"
                  alt="tiki"
                  className="w-full h-full object-contain"
                />
              </div>
              <div className="flex-1">
                <p className="mb-2 font-bold text-xl">TIKI</p>
                <p className="text-lg">
                  Reguler Service (REG) | Over Night Service (ONS)
                </p>
              </div>
            </div>
          </div>

          <div
            className="font-medium text-lg text-center text-white bg-gogreen py-[14px] rounded-lg cursor-pointer"
            onClick={submitSelectedCourierHandler}
          >
            Pilih
          </div>
        </>
      </div>
    </div>
  );
};

export default ModalCourier;
