import axios from 'axios';
import React, { useEffect, useState } from 'react'
import {  useSelector } from "react-redux";
import Select from "react-select";
import { listprovinsi } from '../../utils/provinsi';
import ModalCourier from './ModalCourier';


const customStyles = {
    control: (provided, state) => ({
      ...provided,
      padding: "6px",
      marginTop: "8px",
      borderRadius: "6px",
      borderColor: "#CFCFCF",
    }),
    placeholder: (provided, state) => ({ ...provided, color: "#CFCFCF" }),
  };

    // set province select input to state


export default function Pengiriman() {
    const [berhasil, setBerhasil] = useState (false);
    const [modal, setModal] = useState(false);
    const [gagal, setGagal] = useState (false);
    const [error, setError] = useState([])
    const [isCourierOpen, setIsCourierOpen] = useState(false);
    const [cityOptions, setCityOptions] = useState([]);
    const {store_id} = useSelector((state) => state.user);
    const [selectedCourier, setSelectedCourier] = useState("pos");
    const [kurir, setkurir] = useState()
    const [isloading, setisloading] = useState(false)
    const [form, setForm] = useState({
        fullAddress: "",
        province:"",
        city:""
      });

      useEffect(()=>{    
        axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/stores/${store_id}/store-address`)
        .then(res => {
          setForm(res.data.data)
        }).catch(err=>{
          console.log(err)
        })
      },[store_id])

  // set data to state
  const formHandler = (e) => {
    setForm((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };
      // set city select input to state
  const cityHandler = (e) => {
    setForm((prev) => ({
      ...prev,
      city: e.city_name,
      province: e.province,
    }));
  };

    const provinsiHandler = (e) => {
      setForm((prev) => ({
        ...prev,
        idProvince: e.province_id,
        province: e.province,
      }));
        axios
          .get(`https://rajaongkir.vercel.app/city?province=${e.province_id}`)
          .then((res) => {
            setCityOptions(res.data.rajaongkir.results);
          })
          .catch((err) => {
            console.log(err.message);
          });
      };

      useEffect(()=>{
        axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/stores/${store_id}/delivery-service`)
        .then(res => {
          setkurir(res.data.data.deliveryService)
        })
        .catch(err => {
          console.log(err)
        })
      },[store_id])

      const submitAlamat = (e) => {
        setisloading(true)
        e.preventDefault();
        if (store_id) {
          axios
            .put(
              `https://thrifthouse.herokuapp.com:443/api/v1/stores/${store_id}/store-address`,
              form
            )
            .then((res) => {
              setModal(false)
              setisloading(false)
              setBerhasil(true)
            })
            .catch((err) => {
              console.log(err);
                setError(err);
                setModal(false)
                setGagal(true)
                setisloading(false)
            });
        }
      };

      function handleModal(e) {
        e.preventDefault(); 
        setModal(true)
    }

    function sureModal(e) {
      return (
          <>
              {isloading ? (
                  <div className="w-full bg-black-rgba fixed z-50 top-0 bottom-0 left-0 right-0 p-6 flex justify-center items-center mx-auto "><div className='bg-white p-10'><div className="w-7 h-7 sm:w-9 sm:h-9 border-4 border-l-gogreen rounded-full animate-spin"></div></div></div>
              )
              :
              (
                  <div className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0 p-6">
              <div className="bg-white rounded-lg m-auto  overflow-hidden pb">
              <div className=" px-14 pt-7 pb-6">
                  <p className="font-bold text-sm mb-2">Apakah kamu yakin?</p>
                  <p className="text-xs">
                  Jika tekan <span className='text-gogreen font-semibold'>Konfirmasi</span> hanya jika data alamat toko kamu sudah sesuai.
                  </p>
              </div>
              <div className="flex justify-end p-2">
                  <button
                  type="button"
                  onClick={(e) => setModal(!modal)}
                  className="py-2 px-7 text-xs   hover:bg-gray-100 hover:text-gray-600 text-white rounded-md bg-gogreen font-semibold mr-5"
                  >
                  Batalkan
                  </button>
                  <button
                  onClick={submitAlamat}
                  className="py-2 px-7 text-xs font-medium border-2 border-gogreen rounded-md text-gogreen hover:bg-gray-50 "
                  >
                  Konfirmasi
                  </button>
              </div>
              </div>
          </div>
              )}
             
          </>
          
      );
    }

    function Berhasil(e) {
      return (
          <div className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0 p-6">
              <div className="bg-white rounded-lg m-auto  overflow-hidden">
              <div className=" px-14 pt-7 pb-6">
                  <p className="font-bold text-sm mb-2">Data pengiriman berhasil diubah</p>
                  <p className="text-xs">
                    Data alamat dan jasa pengiriman kamu berhasil diubah
                  </p>
              </div>
              <div className="flex">
                  <button
                  type="button"
                  onClick={(e) => setBerhasil(!berhasil)}
                  className="flex-grow py-4 text-xsborder-[0.3px] hover:bg-gray-100 text-gogreen font-semibold"
                  >
                  Ok
                  </button>
              </div>
              </div>
          </div>
      );
    }

    function Gagal(e) {
      return (
          <div className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0 p-6">
              <div className="bg-white rounded-lg m-auto  overflow-hidden">
              <div className=" px-14 pt-7 pb-6">
                  <p className="font-bold text-sm mb-2">{error.message}</p>
                  <p className="text-xs">
                  Mungkin kamu belum memasukan semua inputan
                  </p>
              </div>
              <div className="flex">
                  <button
                  type="button"
                  onClick={(e) => setGagal(!gagal)}
                  className="flex-grow py-4 text-xsborder-[0.3px] hover:bg-gray-100 text-gogreen font-semibold"
                  >
                  Ok
                  </button>
              </div>
              </div>
          </div>
      );
    }
  


  return (
    <>
    <form className='' onSubmit={handleModal}>
        <div className='p-4'>
            <label for="message" class="block mb-2 text-base font-medium text-gray-900 ">Alamat lengkap toko</label>
            <textarea onChange={formHandler} value={form.fullAddress} id="message" name='fullAddress' rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300" placeholder="Contoh: Perum Griya Asa Blok 5 No.76"></textarea>
        </div>

                  <div className="grid grid-cols-1 sm:grid-cols-2 gap-x-10 mt-16 p-4">
            <div className="mb-2 sm:mb-0">
              <label htmlFor="provinsi" className="text-xs sm:text-base">
                Provinsi
              </label>
              <Select
                options={listprovinsi}
                getOptionLabel={(option) => option.province}
                getOptionValue={(option) => option.province_id}
                styles={customStyles}
                inputId={"provinsi"}
                placeholder={"Pilih Provinsi"}
                onChange={provinsiHandler}
                classNamePrefix="custom"
                value={
                  form.province
                    ? {
                        province:form.province
                    }
                         
                      
                    : null
                }
              />
            </div>
            <div>
              <label htmlFor="kota" className="text-xs sm:text-base">
                Kota / Kabupaten
              </label>
              <Select
                options={cityOptions}
                getOptionLabel={(option) => option.city_name}
                getOptionValue={(option) => option.city_id}
                styles={customStyles}
                inputId={"kota"}
                placeholder={"Pilih Kota/Kabupaten"}
                onChange={cityHandler}
                classNamePrefix="custom"
                value={
                  form.city
                    ? {
                        city_name:form.city
                    }
                    : null
                }
              />
            </div>
          </div>
          
          <div className='mt-10 p-4'>
            <p className=' mb-2 text-base font-medium text-gray-900'>Layanan Pengiriman<span className='px-2 bg-gogreen ml-2 rounded-full text-white'>!</span></p>
            <div className=' flex justify-end'>
              <p className='bg-gogreen py-2 px-3  rounded-md text-white' onClick={() => setIsCourierOpen((prev) => !prev)}>Ubah Layanan Pengiriman</p>
            </div>            
          </div>

          {kurir === "pos" && (
          <div className='w-full h-auto  p-4'>
              <div className='w-full flex p-16 bottom-2 border-2'>
                <div className='w-[124px] h-[58px] '>
                <img
                  src="/images/pos.png"
                  alt="pos"
                  className="w-full h-full object-contain"
                />
                </div>
                <div className='w-1/2 flex justify-center items-center'>
                  <p className=''>{kurir.toUpperCase()}</p>
                </div>
                <div className='w-1/2 flex justify-center items-center'>
                  <p className='text-zinc-400'>
                  Pos Kilat Khusus (Reguler) - 2-4 hari
                  </p>
                </div>
              </div>
          </div>
          )}

          {kurir === "jne" && (
          <div className='w-full h-auto  p-4'>
              <div className='w-full flex p-16 bottom-2 border-2'>
                <div className='w-[124px] h-[58px] '>
                <img
                  src="/images/jne.png"
                  alt="pos"
                  className="w-full h-full object-contain"
                />
                </div>
                <div className='w-1/2 flex justify-center items-center'>
                  <p className=''>{kurir.toUpperCase()}</p>
                </div>
                <div className='w-1/2 flex justify-center items-center'>
                  <p className='text-zinc-400'>
                  Ongkos Kirim Ekonomis (OKE) | Layanan Reguler (REG) | Super Speed (SPS) | Yakin Esok Sampai (YES)
                  </p>
                </div>
              </div>
          </div>
          )}
          {kurir === "tiki" && (
          <div className='w-full h-auto  p-4'>
              <div className='w-full flex p-16 bottom-2 border-2'>
                <div className='w-[124px] h-[58px] '>
                <img
                  src="/images/tiki.png"
                  alt="pos"
                  className="w-full h-full object-contain"
                />
                </div>
                <div className='w-1/2 flex justify-center items-center'>
                  <p className=''>{kurir.toUpperCase()}</p>
                </div>
                <div className='w-1/2 flex justify-center items-center'>
                  <p className='text-zinc-400'>
                  Reguler Service (REG) | Over Night Service (ONS)
                  </p>
                </div>
              </div>
          </div>
          )}

          {isCourierOpen && (
            <ModalCourier
              isCourierOpen={isCourierOpen}
              setIsCourierOpen={setIsCourierOpen}
              setSelectedCourier={setSelectedCourier}
              selectedCourier ={selectedCourier}
              setKurir={setkurir}
            />
          )}

        <div className=' w-full flex justify-end mt-10'>
            
            <button
                className="py-3 px-7 text-xs   hover:bg-gray-100 hover:text-gray-600 text-white rounded-md bg-gogreen font-semibold mr-5"
                      >
                Simpan
            </button>
        </div>
        
        
    </form>
    {modal && sureModal()}
        {berhasil && Berhasil()}
        {gagal && Gagal()}
</>
  )
}
