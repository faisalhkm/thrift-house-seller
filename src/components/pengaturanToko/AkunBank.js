import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';

export default function AkunBank() {
    const[infotoko, setinfotoko]= useState([])
    const {store_id} = useSelector((state) => state.user);

    useEffect(()=>{    
        axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/stores/${store_id}/store-bank-account`)
        .then(res => {
          setinfotoko(res.data.data)
        }).catch(err=>{
          console.log(err)
        })
      },[store_id])
  return (
    <>
    <div className='border-2 rounded-lg p-5'>
        <div className='flex w-1/2 justify-between '>
            <div>
                <p className='text-base text-[#5D5E79] font-semibold'>Nama Pemilik Rekening:</p>
                <p>{infotoko.bankHolder}</p>
            </div>
            <div>
                <p className='text-base text-[#5D5E79] font-semibold'>Bank</p>
                <p>{infotoko.bankName}</p>
            </div>
        </div>
        <div className='mt-5'>
            <p className='text-base text-[#5D5E79] font-semibold'>No. Rekening</p>
            <p>{infotoko.bankNumber}</p>
        </div>
    </div>
</>
  )
}
