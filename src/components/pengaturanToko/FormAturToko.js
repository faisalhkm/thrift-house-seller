import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { globalAxiosWithVesion } from '../../config/configApi';
import { PlaceImgLogo } from './PlaceImageLogo';
import { PlaceImgBanner } from './PlaceImgBanner'

export default function FormAturToko() {
  const {store_id} = useSelector((state) => state.user);
    const [infoToko, setinfotoko] = useState([])
    const [photos, setPhotos] = useState(null)
    const [photosLogo, setPhotosLogo] = useState(null)
    const [berhasil, setBerhasil] = useState (false);
    const [modal, setModal] = useState(false);
    const [gagal, setGagal] = useState (false);
    const [isloading, setisloading] = useState(false)
    const [error, setError] = useState([])
    
  const [data, setData] = useState({
    storeName     : "",
    storeAbout   : "",
    storeStatus:"",
    storeBanner:"",
    storeLogo:""
  });

  useEffect(()=>{
    
    axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/stores/${store_id}/store-information`)
    .then(res => {
      setinfotoko(res.data.data)
      setData(res.data.data)
    }).catch(err=>{
      console.log(err)
    })
  },[store_id])
  

    const handleChange = (e) => {
      setData((prev) => ({ ...prev, [e.target.name]: e.target.value }));
    }

    const handleAddPhoto = (dataImg) => {
        setPhotos(dataImg);
      }

     const handleAddPhoto2 = ( dataImg) => {
    setPhotosLogo(dataImg);
  }
    
  const handleCancelPhotoBanner = (imgId) => {
    setPhotos(imgId);
  }
  const handleCancelPhotoLogo = (imgId) => {
    setPhotosLogo(imgId);
  }


    const handleSubmit= async (e) => {
    setisloading(true)
    e.preventDefault();

       const formData = new FormData();
       formData.append("storeAbout",(data.storeAbout === "" ? infoToko.storeAbout : data.storeAbout));
        formData.append("storeBanner", (photos === null? infoToko.storeBanner : photos));
        formData.append("storeLogo",  (photosLogo === null ? infoToko.storeLogo : photosLogo));
       formData.append("storeName",(data.storeName === "" ? infoToko.storeName : data.storeName));
       formData.append("storeStatus", (data.storeStatus === "" ? infoToko.storeStatus : data.storeStatus));

       try {
         const response = await globalAxiosWithVesion.putForm(
           `https://thrifthouse.herokuapp.com:443/api/v1/stores/${store_id}/store-information`,
           formData
         );
         setModal(false)
         setisloading(false)
         setBerhasil(true)
        //  const id = response.data.data.id;
        //  navigate(`/products`);
       } catch (error) {
        console.log(error)
         setError(error);
         setModal(false)
         setGagal(true)
         setisloading(false)
       }
   };
   function handleModal(e) {
    e.preventDefault(); 
    setModal(true)
}


    function sureModal(e) {
      return (
          <>
              {isloading ? (
                  <div className="w-full bg-black-rgba fixed z-50 top-0 bottom-0 left-0 right-0 p-6 flex justify-center items-center mx-auto "><div className='bg-white p-10'><div className="w-7 h-7 sm:w-9 sm:h-9 border-4 border-l-gogreen rounded-full animate-spin"></div></div></div>
              )
              :
              (
                  <div className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0 p-6">
              <div className="bg-white rounded-lg m-auto  overflow-hidden pb">
              <div className=" px-14 pt-7 pb-6">
                  <p className="font-bold text-sm mb-2">Apakah kamu yakin?</p>
                  <p className="text-xs">
                  Jika tekan <span className='text-gogreen font-semibold'>Konfirmasi</span> hanya jika informasi toko kamu sudah sesuai.
                  </p>
              </div>
              <div className="flex justify-end p-2">
                  <button
                  type="button"
                  onClick={(e) => setModal(!modal)}
                  className="py-2 px-7 text-xs   hover:bg-gray-100 hover:text-gray-600 text-white rounded-md bg-gogreen font-semibold mr-5"
                  >
                  Batalkan
                  </button>
                  <button
                  onClick={handleSubmit}
                  className="py-2 px-7 text-xs font-medium border-2 border-gogreen rounded-md text-gogreen hover:bg-gray-50 "
                  >
                  Konfirmasi
                  </button>
              </div>
              </div>
          </div>
              )}
             
          </>
          
      );
    }

    function Berhasil(e) {
      return (
          <div className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0 p-6">
              <div className="bg-white rounded-lg m-auto  overflow-hidden">
              <div className=" px-14 pt-7 pb-6">
                  <p className="font-bold text-sm mb-2">Informasi toko berhasil diubah</p>
                  <p className="text-xs">
                  Muat ulang halaman anda
                  </p>
              </div>
              <div className="flex">
                  <button
                  type="button"
                  onClick={(e) => setBerhasil(!berhasil)}
                  className="flex-grow py-4 text-xsborder-[0.3px] hover:bg-gray-100 text-gogreen font-semibold"
                  >
                  Ok
                  </button>
              </div>
              </div>
          </div>
      );
    }

    function Gagal(e) {
      return (
          <div className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0 p-6">
              <div className="bg-white rounded-lg m-auto  overflow-hidden">
              <div className=" px-14 pt-7 pb-6">
                  <p className="font-bold text-sm mb-2">{error.message}</p>
                  <p className="text-xs">
                  Mungkin kamu belum memasukan semua inputan
                  </p>
              </div>
              <div className="flex">
                  <button
                  type="button"
                  onClick={(e) => setGagal(!gagal)}
                  className="flex-grow py-4 text-xsborder-[0.3px] hover:bg-gray-100 text-gogreen font-semibold"
                  >
                  Ok
                  </button>
              </div>
              </div>
          </div>
      );
    }
  


  return (
    <>
        <form className='' onSubmit={handleModal}>
        <div className='h-full p-4 '>
                <p>Nama toko</p>
                <p className='text-sm text-zinc-400'>Gunakan nama terbaik untuk menarik pembeli belanja di tokomu ya!</p>
                <div className='mt-2'>
                <label htmlFor="nama"></label>
                {infoToko.storeName === null ? (
                <input type="text" name="storeName" id='nama'  placeholder={'Masukan nama toko'} className='border-2 w-80 sm:w-60 lg:w-80  p-2 rounded-md text-sm' onChange={handleChange}/>
                ):
                (
                  <input type="text" name="storeName" id='nama'  value={data.storeName}  className='border-2 w-80 sm:w-60 lg:w-80 p-2 rounded-md text-sm'  onChange={handleChange}/>
                )}
                </div>
            </div>
            <div className='w-full flex justify-between  p-4 '>
                <div className='w-8/12'>
                    <p className='mb-5'>Banner toko<span className='px-2 bg-gogreen ml-2 rounded-full text-white'>!</span></p>
                    <div class="flex justify-center items-center ">
                    <div className="w-full">
                        <PlaceImgBanner
                        imgId={0}
                        key={0}
                        onChange={(dataImg) => handleAddPhoto(dataImg)}
                        onCancel={(imgId) => handleCancelPhotoBanner(imgId)}
                        />
                        
                    </div>
                    </div> 

                </div>
                <div className='w-3/12'>
                    <p className='mb-5'>Logo toko<span className='px-2 bg-gogreen ml-2 rounded-full text-white'>!</span></p>
                    <div className="flex justify-center items-center ">
                    <div className="w-full">
                        <PlaceImgLogo
                        imgId={1}
                        key={1}
                        onChange={(imgId, dataImg) => handleAddPhoto2(imgId, dataImg)}
                        onCancel={(imgId) => handleCancelPhotoLogo(imgId)}
                        />
                        
                    </div>
                    </div> 
                </div>
            </div>
            
            {/* <div className='p-4'>
                <p>Status toko</p>
                <p className='text-sm text-zinc-400'>Apakah saat ini toko kamu buka?</p>
                {data.storeStatus === null ? (
              <div className='mt-2 text-sm flex'>
                    <input type="radio"  name="status" id='status'  value={'buka'} className='mr-2 scale-150 ' onChange={handleChange}/>
                    <label htmlFor="status" className='flex flex-col text-sm  mr-5'>Buka</label>
                    <input type="radio" name="status" id='status' value={'sementara tutup'} className='scale-150 mr-2' onChange={handleChange}/>
                    <label htmlFor="status" className='flex flex-col  text-sm '>Sementara tutup</label>
                </div>
        ):
        (
            (data.storeStatus === "buka") ? (
              <div className='mt-2 text-sm flex'>
                    <input type="radio"  name="status" id='status'  value={'buka'} className='mr-2 scale-150 ' defaultChecked={true} onChange={handleChange}/>
                    <label htmlFor="status" className='flex flex-col text-sm  mr-5'>Buka</label>
                    <input type="radio" name="status" id='status' value={'sementara tutup'} className='scale-150 mr-2' onChange={handleChange}/>
                    <label htmlFor="status" className='flex flex-col  text-sm '>Sementara tutup</label>
                </div>
        ) : (
          <div className='mt-2 text-sm flex'>
                    <input type="radio"  name="status" id='status'  value={'buka'} className='mr-2 scale-150 '  onChange={handleChange}/>
                    <label htmlFor="status" className='flex flex-col text-sm  mr-5'>Buka</label>
                    <input type="radio" name="status" id='status' value={'sementara tutup'} className='scale-150 mr-2' defaultChecked={true} onChange={handleChange}/>
                    <label htmlFor="status" className='flex flex-col  text-sm '>Sementara tutup</label>
                </div>
        ) 
        )
        }
            </div> */}

            

            
            
            <div className='p-4'>
                <p>Deskripsi toko</p>
                <label for="message" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Your message</label>
                <textarea value={data.storeAbout} id="message" name='storeAbout' rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300" placeholder="Masukan Deskripsi toko kamu disini!" onChange={handleChange}></textarea>

            </div>

            <div className=' w-full flex justify-end mt-10'>
                
                <button
                    className="py-3 px-7 text-xs   hover:bg-gray-100 hover:text-gray-600 text-white rounded-md bg-gogreen font-semibold mr-5"
                          >
                    Simpan
                </button>
            </div>
            
            
        </form>
        {modal && sureModal()}
        {berhasil && Berhasil()}
        {gagal && Gagal()}
    </>
  )
}
