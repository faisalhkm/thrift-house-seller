import React, { useEffect, useState } from 'react'
import plusCircle from "../../assets/icon/plus-circle.svg";
import buttonCircleClose from "../../assets/icon/button-circle-close.svg";
import axios from 'axios';
import { useSelector } from 'react-redux';

export const PlaceImgBanner= ({ title, onChange, imgId, onCancel }) => {
  const [dataImg, setDataImg] = useState("");
  const [defaultBanner, setDefaulBanner]=useState("")
  const {store_id} = useSelector((state) => state.user);
  const [isHovering, setIsHovering] = useState(false);

  const handleMouseOver = () => {
    setIsHovering(true);
  };

  const handleMouseOut = () => {
    setIsHovering(false);
  };

  useEffect(()=>{
    axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/stores/${store_id}/banner`)
    .then(resposne=>{
      setDefaulBanner(resposne.data.data.banner)
    }).catch(err=>{console.log(err)})
  },[store_id])

  const handleUploadClose = (e) => {
    setDataImg("");
  };

  const handleUploadChange = (e) => {
    setDataImg(e.target.files[0]);
    onChange(e.target.files[0]);
  };

  const handleCancel = () => {
    onCancel(null);
  };
  
  return (
    <>
      <div className=" relative text-center">
        {/* Disaat Image Sudah Terisi */}
        {dataImg && (
          <div>
            <img
              src={window.URL.createObjectURL(dataImg)}
              className="h-52 w-full rounded-md"
              alt=""
            />
            <div
              className="absolute -top-[10px] -right-[10px] cursor-pointer"
              onClick={handleUploadClose}
            >
              <img src={buttonCircleClose} alt="" onClick={handleCancel} />
            </div>
          </div>
        )}

        {/* Disaat Image Masih Kosong */}
        {!dataImg && (
          <div className='relative'>
          {defaultBanner===""?(
            
            <label
              htmlFor={`actual-btn${imgId}`}
              className="h-52 w-full border-2 rounded-md border-dashed flex justify-center items-center "
            >
                {/* <img src={plusCircle} className="h-6 " alt="" /> */}
        </label>


          ):(
            <label
          onMouseOver={handleMouseOver} onMouseOut={handleMouseOut}
              htmlFor={`actual-btn${imgId}`}
              className="h-52 w-full border-2 rounded-md border-dashed flex justify-center items-center relative  cursor-pointer"
            >
           
              <img src={defaultBanner} className={`w-full h-full absolute`} alt="" />
              {isHovering && (
                <div className=" bg-black-rgba w-full h-full   absolute z-50 flex justify-center items-center">

                <img src={plusCircle} className="h-6 " alt="" />
                </div>
              )}
        </label>
          )}
         
            
         

            <input
              type="file"
              id={`actual-btn${imgId}`}
              hidden
              onChange={handleUploadChange}
            />
          </div>
        )}
        <p className="text-sm mt-2">{title}</p>
      </div>
    </>
  );
};
