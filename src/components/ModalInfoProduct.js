import React from 'react'
import { ModalInfo } from "./modals/ModalInfo";

export const ModalInfoProduct = () => {
  return (
    <div className="text-[#0C0D36]">
      {/* Modal Info  Pengambilan Foto Product*/}
      <ModalInfo id="ModalInfoImageProduct">
        <h1 className="font-bold text-2xl mt-6">Pengambilan Foto Produk</h1>
        <p className="my-5">
          Gunakan angle yang berbeda-beda dalam pengambilan foto produk agar
          Pembeli dapat memperoleh gambaran yang lebih jelas tentang produk
          kamu. Menggunakan berbagai angle membuat calon pembeli dapat
          mengetahui produk secara lebih detail sehingga meningkatkan
          kemungkinan terjadinya transaksi.
        </p>
        <ul>
          <li className="flex">
            <p className="font-bold w-1/5">&bull; Foto Utama </p>
            <p>: Foto produk secara keseluruhan.</p>
          </li>
          <li className="flex">
            <p className="font-bold w-1/5">&bull; Detail Kondisi 1 </p>
            <p>
              : Foto detail apabila produk kondisi kurang baik atau lainnya jika
              tidak ada.
            </p>
          </li>
          <li className="flex">
            <p className="font-bold w-1/5">&bull; Detail Kondisi 2 </p>
            <p>
              : Foto detail apabila produk kondisi cacat atau lainnya jika tidak
              ada.
            </p>
          </li>
          <li className="flex">
            <p className="font-bold w-1/5">&bull; Detail Bahan </p>
            <p>: Foto detail bahan produk.</p>
          </li>
        </ul>
      </ModalInfo>
      {/* Modal Info Kondisi Product */}
      <ModalInfo id="ModalInfoKondisiProduct">
        <h1 className="font-bold text-2xl mt-6">
          Peringkat kondisi untuk produk
        </h1>
        <p className="my-5">
          Tim dari ThriftHouse selalu mengevaluasi setiap produk yang dikirim
          untuk kondisi dan kualitas. Setiap produk yang tidak memenuhi kriteria
          penerimaan perusahaan kami ditolak. Peringkat kategori kondisi
          terdaftar dalam urutan sebagai berikut:
        </p>
        <ul>
          <li className="flex">
            <p className="font-bold w-[22%]">&bull; Baru dengan Tag </p>
            <p>: Barang yang terlihat baru dan masih memiliki tag.</p>
          </li>
          <li className="flex">
            <p className="font-bold w-[22%]">&bull; Seperti Baru </p>
            <p>: Barang kondisi sangat baik, tetapi tanpa tag.</p>
          </li>
          <li className="flex">
            <p className="font-bold w-[22%]">&bull; Baik </p>
            <p>
              : Barang bekas dalam kondisi sangat baik! Tidak ada noda atau
              cacat.
            </p>
          </li>
          <li className="flex">
            <p className="font-bold w-[22%]">&bull; Cukup </p>
            <p>
              : Barang bekas layak pakai dengan tanda pemakaian sehari-hari.
            </p>
          </li>
        </ul>
      </ModalInfo>
    </div>
  );
}
