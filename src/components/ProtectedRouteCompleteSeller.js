import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import jwt_decode from "jwt-decode";
import { userLogout } from "../action";
import { Navigate, Outlet} from "react-router-dom";

const ProtectedRouteCompleteSeller = () => {
  const { access_token: token, storeId } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  useEffect(() => {
    if (token) {
      const { exp } = jwt_decode(token);
      if (exp < Date.now() / 1000) {
        dispatch(userLogout());
      }
    }
  }, [dispatch, token]);

  if (!token) {
    return <Navigate to="/login" />;
  }else if(token && storeId){
    return <Navigate to="/" />;
  }

  return <Outlet />;
};

export default ProtectedRouteCompleteSeller;
