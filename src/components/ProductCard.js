import { Link } from "react-router-dom";
import { convertToRupiah } from "../utils/convert";

const ProductCard = ({
  id,
  name,
  brand,
  size,
  condition,
  price,
  photos,
  category,
}) => {

  return (
    <div className="w-full max-w-[260px] mr-8  mb-6 shadow-lg">
      <Link to={`/products/${id.toString()}`}>
        <div className="flex flex-col rounded-lg shadow-lg bg-white">
          <div className="h-[114px] lg:h-[232px] rounded-t-lg">
            <img
              src={photos}
              alt="burger"
              className="w-full h-full object-cover rounded-t-lg"
            />
          </div>
          <div className="px-4 py-3">
            <h4 className="font-bold text-sm lg:text-xl mb-1 truncate">
              {brand}
            </h4>
            <h5 className="font-medium text-xs lg:text-xl mb-1 truncate">
              {name}
            </h5>
            <div className="flex text-[8px] space-x-1 mb-2 text-base">
              <div className="py-[2px] px-2 bg-[#F2F2F2] rounded text-[#8F8F8F]">
                {condition}
              </div>
              <div className="py-[2px] px-2 bg-[#F2F2F2] rounded text-[#8F8F8F]">
                {size}
              </div>
            </div>
            <h5 className="text-gogreen font-medium text-sm lg:text-xl">
              {convertToRupiah(price)}
            </h5>
          </div>
        </div>
      </Link>

      <div className="absolute z-10 right-2 top-2 sm:right-3 sm:top-3 bg-white p-1 rounded-full cursor-pointer"></div>
    </div>
  );
};

export default ProductCard;
