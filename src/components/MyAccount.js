import { InputText } from "./forms/InputText";
import { ButtonSuccess } from "./ButtonSuccess";
import { ModalConfirm } from "./modals/ModalConfirm";
import { closeModal } from "../utils/modal";
import { globalAxiosWithVesion } from "../config/configApi";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { PulseLoader } from "react-spinners";

export default function MyAccount() {
  const [form, setForm] = useState({
    email: "",
    phone: "",
    fullname: "",
    gender: "",
    birth: "",
    ktpImg: "",
    ktpNumber: "",
  });
  const [isDisable, setIsDisable] = useState(true);
  const [loading, setLoading] = useState(false);
  const [loadId, setLoadId] = useState(true);
  const [initialData, setInitialData] = useState({
    email: "",
    phone: "",
    fullname: "",
    gender: "",
    birth: "",
    ktpImg: "",
    ktpNumber: "",
  });

  const user = useSelector((state) => state.user);

  const handleClickModal = () => {
    // const formData = {
    //   email: body.email ? body.email : "",
    //       phone: body.phone ? body.phone : "",
    //       fullname: body.fullname ? body.fullname : "",
    //       gender: body.gender ? body.gender : "",
    //       birth: body.birth ? body.birth.split("-").join("/") : "",
    // };
    const formData = new FormData();
    formData.append("fullname", form.fullname);
    formData.append("email", form.email);
    formData.append("birth", form.birth.split("-").join("/"));
    formData.append("phone", form.phone);
    formData.append("gender", form.gender);

    setLoading(true);
    globalAxiosWithVesion
      .putForm(`/stores/seller-account/${user.id}`, formData)
      .then((response) => {
        const body = response.data.data;
        const newForm = {
          email: body.email ? body.email : "",
          phone: body.phone ? body.phone : "",
          fullname: body.fullname ? body.fullname : "",
          gender: body.gender ? body.gender : "",
          birth: body.birth ? new Date(body.birth).toISOString().split("T")[0] : "",
          ktpImg: body.ktpImg ? body.ktpImg : "",
          ktpNumber: body.ktpNumber ? body.ktpNumber : "",
        };
        setForm({
          ...newForm,
        });
        setInitialData({
          ...newForm,
        });
        setIsDisable(true);
        setLoading(false);
        closeModal();
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
        closeModal();
      });
  };
  useEffect(() => {
    let newForm = {
      email: "",
      phone: "",
      fullname: "",
      gender: "",
      birth: "",
      ktpImg: "",
      ktpNumber: "",
    };
    globalAxiosWithVesion
      .get(`/users/${user.id}`)
      .then((response) => {
        const body = response.data.data;
        newForm = {
          ...newForm,
          email: body.email ? body.email : "",
          phone: body.phone ? body.phone : "",
          fullname: body.fullname ? body.fullname : "",
          gender: body.gender ? body.gender : "",
          birth: body.birth ? body.birth.split("/").join("-") : "",
        };
        setForm({
          ...newForm,
        });
        setInitialData({
          ...newForm,
        });
        setLoadId(true);
        globalAxiosWithVesion.get(`/stores/${user.storeId}/ktp`).then((response) => {
          const body = response.data.data;
          newForm = {
            ...newForm,
            ktpImg: body.ktpImg ? body.ktpImg : "",
            ktpNumber: body.ktpNumber ? body.ktpNumber : "",
          };
          setForm(newForm);
          setInitialData(newForm);
          setLoadId(false);
        });
      })
      .catch((error) => {
        console.log(error);
        setLoadId(false);
      });
  }, [user]);

  const handleForm = (value, key) => {
    const newForm = {
      ...form,
      [key]: value,
    };
    setIsDisable(newForm[key] === initialData[key]);
    setForm(newForm);
  };

  return (
    <div className="">
      <p className="text-2xl font-medium">Akun Saya</p>
      <p className="text-[#9191A9]">Kelola akunmu disini</p>
      <div className="px-16 py-11 bg-white mt-5 rounded-md shadow-md">
        <form className="flex flex-col gap-12">
          <label htmlFor="name" className="text-lg">
            Nama Pemilik
            <InputText
              id="name"
              name="fullname"
              value={form.fullname}
              onChange={(e) => handleForm(e.target.value, e.target.name)}
            />
          </label>
          <label htmlFor="email" className="text-lg">
            Email
            <InputText
              id="email"
              name="email"
              type="email"
              value={form.email}
              onChange={(e) => handleForm(e.target.value, e.target.name)}
            />
          </label>
          <div>
            <p>No. KTP</p>
            <div className="border-[#AEAEBC] border-dashed p-2 border rounded-lg w-1/3 min-h-[50px] max-w-xs flex justify-center">
              {loadId ? (
                <PulseLoader
                  cssOverride={{ display: "inline-block" }}
                  size={4}
                  color={"#cfcfcf"}
                />
              ) : (
                <img alt="" src={form.ktpImg} className="max-w-xs w-full" />
              )}
            </div>
            <InputText
              value={form.ktpNumber}
              disabled
              className="disabled:opacity-50"
            />
          </div>
          <label htmlFor="phone" className="text-lg">
            Nomor Telepon
            <InputText
              id="phone"
              name="phone"
              value={form.phone}
              onChange={(e) => handleForm(e.target.value, e.target.name)}
            />
          </label>
          <label htmlFor="birthday" className="text-lg">
            Tanggal Lahir
            <InputText
              id="birthday"
              type="date"
              name="birth"
              value={form.birth}
              onChange={(e) => handleForm(e.target.value, e.target.name)}
            />
          </label>
          <div>
            <p className="text-lg">Jenis Kelamin</p>
            <div className="mt-2 flex items-center gap-2">
              <input
                type="radio"
                id="lakiLaki"
                name="gender"
                className="w-5 h-5"
                value="M"
                checked={form.gender === "M"}
                onChange={(e) => handleForm(e.target.value, e.target.name)}
              />
              <label htmlFor="lakiLaki">Laki-Laki</label>
              <input
                type="radio"
                id="perempuan"
                name="gender"
                className="ml-6 w-5 h-5"
                value="F"
                checked={form.gender === "F"}
                onChange={(e) => handleForm(e.target.value, e.target.name)}
              />
              <label htmlFor="perempuan">Perempuan</label>
            </div>
          </div>
          <ButtonSuccess
            isBtnDisable={isDisable}
            className={`self-end rounded-lg`}
            databstoggle={`${!isDisable && "modal"}`}
            databstarget={`${!isDisable && "#exampleModalCenter"}`}
            type="button"
          >
            <p>Simpan</p>
          </ButtonSuccess>
        </form>
        <ModalConfirm
          actionTitle="Konfirmasi"
          cancelTitle="Batalkan"
          size="sm"
          onAction={handleClickModal}
          disabled={loading}
          actionClassName={`${
            !loading ? "bg-gogreen" : "bg-gray-disabled cursor-not-allowed"
          } text-white`}
        >
          <h1 className="font-bold text-2xl mb-2">Apakah kamu yakin?</h1>
          <p className="text-base">
            Lakukan <span className="text-[#29A867]">konfirmasi</span> hanya
            jika biodata diri sudah sesuai.
          </p>
        </ModalConfirm>
      </div>
    </div>
  );
}
