import React from 'react'

export const StepBar = (props) => {
  return (
    <div {...props}>
      <div className="flex justify-around border rounded-full px-10 py-3 bg-white shadow-md">
        <div className="flex items-center">
          <div className="relative h-8 w-8 text-center rounded-full bg-gogreen text-white">
            <p className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
              1
            </p>
          </div>
          <p className="ml-2">Informasi Toko dan Seller</p>
          <p></p>
        </div>

        <div className="flex items-center">
          <div
            className={`relative h-8 w-8 text-center rounded-full  text-white ${
              props.step >= 1 ? " bg-gogreen" : "bg-gray-disabled"
            }`}
          >
            <p className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
              2
            </p>
          </div>
          <p className="ml-2">Atur Jasa Pengiriman</p>
          <p></p>
        </div>

        <div className="flex items-center">
          <div
            className={`relative h-8 w-8 text-center rounded-full text-white ${
              props.step >= 2 ? "bg-gogreen" : "bg-gray-disabled"
            }`}
          >
            <p className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
              3
            </p>
          </div>
          <p className="ml-2">Atur Rekening</p>
          <p></p>
        </div>
      </div>
    </div>
  );
}
