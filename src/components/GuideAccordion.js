import { useRef } from "react";
import { InlineIcon } from "@iconify/react";

export default function GuideAccordion(props) {
  const content = useRef(null);

  return (
    <div className={`flex flex-col mt-4 ${props.className}`}>
      <button className={`flex border rounded-lg px-3 py-[10px] gap-2 ${props.classButton}`} onClick={props.onClick}>
        <p className={`${props.classTitle}`}>{props.title}</p>
        <InlineIcon icon="eva:arrow-ios-downward-outline" className={`ml-auto min-w-fit ${props.active ? "rotate-180" : ""}`} />
      </button>
      <div ref={content} className={`transition-[max-height] overflow-hidden ${props.active ? `max-h-[${content.current.scrollHeight}px]` : "h-0"} ease-in ${props.classContent}`}>
        <div className="text-xs">{props.content}</div>
      </div>
    </div>
  );
}
