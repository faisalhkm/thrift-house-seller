import React from 'react'
import iconCloseBlack from "../../assets/icon/close-black.svg";

export const ModalInfo = ({children, id}) => {

  return (
    <div
      className="modal fade fixed top-0 left-0 hidden w-full h-full outline-none overflow-x-hidden overflow-y-auto"
      id={id ? id : "exampleModalInfo"}
      tabIndex="-1"
      aria-labelledby="exampleModalCenterTitle"
      aria-modal="true"
      role="dialog"
    >
      <div className="modal-dialog modal-lg modal-dialog-centered relative w-auto pointer-events-none">
        <div className="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
          <div className="modal-body relative p-4 px-6 mt-2">
            <div className="flex justify-end">
              <img
                src={iconCloseBlack}
                className="h-7 w-7 cursor-pointer"
                alt=""
                data-bs-dismiss="modal"
                id={"closeModal"}
              />
            </div>
            <div>{children}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
