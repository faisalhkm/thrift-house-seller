import React from 'react'
import Spinner from '../Spinner';

export const ModalConfirm = ({children, cancelTitle, disabled, actionTitle, actionClassName, onAction, id}) => {
  return (
    <div
      className="modal fade fixed top-0 left-0 hidden w-full h-full outline-none overflow-x-hidden overflow-y-auto"
      id={id ? id : "exampleModalCenter" }
      tabIndex="-1"
      aria-labelledby="exampleModalCenterTitle"
      aria-modal="true"
      role="dialog"
    >
      <div className="modal-dialog modal-dialog-centered relative w-auto pointer-events-none">
        <div className="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
          <div className="modal-body relative p-4 mt-2">
            <div>{children}</div>
            <div className="flex justify-end mt-4">
              <button
                type="button"
                className="mt-1 mb-1  border rounded-md text-gogreen border-gogreen font-medium text-base mr-3 px-6 py-2"
                data-bs-dismiss="modal"
                id={"closeModal"}
              >
                {cancelTitle}
              </button>
              <button
                className={`mt-1 mb-1  border rounded-md  font-medium text-base mr-3 px-6 py-3 ${actionClassName} }`}
                type="button"
                onClick={() => onAction()}
              >
                {disabled ? <Spinner className={`border-l-white `} /> : actionTitle}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
