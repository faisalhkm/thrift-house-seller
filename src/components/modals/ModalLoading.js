import React from 'react'
import { FadeLoader } from 'react-spinners'

export const ModalLoading = () => {
  return (
    <div className="flex justify-center fixed top-[30%] w-full left-0 right-0">
      <div className="flex-row justify-center text-center bg-white border p-8 py-6 shadow-lg  rounded-lg">
        <FadeLoader/>
      </div>
    </div>
  );
}
