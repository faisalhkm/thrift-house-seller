import React from 'react'

export const MsgDataEmpty = (props) => {
  return (
      <div
        className={
          `flex mt-12 justify-center w-full font-light text-2xl text-gray-400 text-center ${props.className}` }
      >
        {props.children}
      </div>
  );
}
