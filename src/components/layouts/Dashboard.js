import React, { useState } from "react";
import transaction from "../../assets/icon/transaction.svg";
import transactionActive from "../../assets/icon/transaction-active.svg";
import store from "../../assets/icon/store.svg";
import storeActive from "../../assets/icon/store-active.svg";
import dashboard from "../../assets/icon/dashboard.svg";
import dashboardActive from "../../assets/icon/dashboard-active.svg";
import bag from "../../assets/icon/bag.svg";
import bagActive from "../../assets/icon/bag-active.svg";
import user from "../../assets/icon/user.svg";
import userActive from "../../assets/icon/user-active.svg";
import guideBook from "../../assets/icon/guide-book.svg";
import guideBookActive from "../../assets/icon/guide-book-active.svg";
import exit from "../../assets/icon/exit.svg";
import { Outlet, NavLink } from "react-router-dom";
import { HeaderAuth } from "./HeaderAuth";
import { useDispatch } from "react-redux";
import { userLogout } from "../../action";
import { useEffect } from "react";
import { ModalConfirm } from "../../components/modals/ModalConfirm";
import { closeModal } from "../../utils/modal";

export const Dashboard = () => {
  const [navActive, setActive] = useState("dashboard");
  const dispatch = useDispatch();

  const url = window.location.href;
  useEffect(() => {
    let urlActive = "dashboard"
    if (url.includes("transaction")) urlActive = "transaksi";
    if (url.includes("products")) urlActive = "katalog produk";
    if (url.includes("/pengaturan")) urlActive = "pengaturan toko";
    if (url.includes("/account")) urlActive = "akun saya";
    if (url.includes("/sales-guide")) urlActive = "panduan penjualan";
    setActive(urlActive)
    
  }, [url]);
  

  const checkNavActive = (params) => params === navActive;

  const handleLogout = () => {
    closeModal();
    dispatch(userLogout());
  }

  return (
    <>
      <header>
        <HeaderAuth />
      </header>
      {/* Side Nav */}
      <section className="fixed top-0 h-full bg-white w-1/5 mt-[4.5em] z-50 shadow-md">
        <ul className="capitalize px-5 pt-6 text-[#B4B4B4] cursor-pointer">
          <NavLink to={"/"}>
            <li
              className={`my-3 p-3 rounded-md flex ${
                checkNavActive("dashboard") && "text-white bg-[#29A867]"
              }`}
            >
              <div className="w-1/6">
                <img
                  src={
                    checkNavActive("dashboard") ? dashboardActive : dashboard
                  }
                  className="h-6 mr-2"
                  alt=""
                />
              </div>
              <div>
                <p>dashboard</p>
              </div>
            </li>
          </NavLink>
          <NavLink to={"/products"}>
            <li
              className={`my-3 p-3 rounded-md flex ${
                checkNavActive("katalog produk") && "text-white bg-[#29A867]"
              }`}
            >
              <div className="w-1/6">
                <img
                  src={checkNavActive("katalog produk") ? bagActive : bag}
                  className="h-6 mr-2"
                  alt=""
                />
              </div>
              <div>
                <p>katalog produk</p>
              </div>
            </li>
          </NavLink>
          <NavLink to={"/transaction"}>
            <li
              className={`my-3 p-3 rounded-md flex ${
                checkNavActive("transaksi") && "text-white bg-[#29A867]"
              }`}
            >
              <div className="w-1/6">
                <img
                  src={
                    checkNavActive("transaksi")
                      ? transactionActive
                      : transaction
                  }
                  className="h-6 mr-2"
                  alt=""
                />
              </div>
              <div>
                <p>transaksi</p>
              </div>
            </li>
          </NavLink>
          <NavLink to={"/pengaturan"}>
            <li
              className={`my-3 p-3 rounded-md flex ${
                checkNavActive("pengaturan toko") && "text-white bg-[#29A867]"
              }`}
            >
              <div className="w-1/6">
                <img
                  src={checkNavActive("pengaturan toko") ? storeActive : store}
                  className="h-6 mr-2"
                  alt=""
                />
              </div>
              <div>
                <p>pengaturan toko</p>
              </div>
            </li>
          </NavLink>
          <NavLink to={"/account"}>
            <li
              className={`my-3 p-3 rounded-md flex ${
                checkNavActive("akun saya") && "text-white bg-[#29A867]"
              }`}
            >
              <div className="w-1/6">
                <img
                  src={checkNavActive("akun saya") ? userActive : user}
                  className="h-6 mr-2"
                  alt=""
                />
              </div>
              <div>
                <p>akun saya</p>
              </div>
            </li>
          </NavLink>

          {/* Panduan Penjualan */}
          <NavLink to={"/sales-guide"}>
            <li
              className={`my-3 p-3 rounded-md flex ${
                checkNavActive("panduan penjualan") && "text-white bg-[#29A867]"
              }`}
            >
              <div className="w-1/6">
                <img
                  src={
                    checkNavActive("panduan penjualan")
                      ? guideBookActive
                      : guideBook
                  }
                  className="h-6 mr-2"
                  alt=""
                />
              </div>
              <div>
                <p>panduan penjualan</p>
              </div>
            </li>
          </NavLink>
          <li
            data-bs-toggle={"modal"}
            data-bs-target={"#logout"}
            className="my-2 p-3 flex"
          >
            <div className="w-1/6">
              <img src={exit} className="h-6 mr-2" alt="" />
            </div>
            <div>
              <p>keluar akun</p>
            </div>
          </li>
        </ul>
      </section>
      {/* Main */}
      <article className="w-4/5 ml-[20%] bg-[#F2F2F2] pt-28 h-full min-h-screen z-0 px-16 pb-12">
        <Outlet />
      </article>
      {/* Modal Logout*/}
      <ModalConfirm
        actionTitle="Ya"
        cancelTitle="Tidak"
        id="logout"
        onAction={handleLogout}
        actionClassName={` text-white bg-[#FD622A]`}
      >
        <h1 className="font-bold text-2xl mb-2">Apakah kamu yakin?</h1>
        <p className="text-base">ingin meninggalkan halaman ini.</p>
      </ModalConfirm>
    </>
  );
};
