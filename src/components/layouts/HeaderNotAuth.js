import React from "react";
import { Link } from "react-router-dom";
import thrifthouseSellerLogo from "../../assets/img/thrifthouse-seller-logo.png";

export const HeaderNotAuth = () => {
  return (
    <div className="fixed top-0 w-full z-50 bg-white">
      <div className="flex justify-between items-center py-3 px-16 shadow-md">
        <div className="basis-1/5">
          <Link to="/sellercenter">
            <img src={thrifthouseSellerLogo} className="h-12" alt="" />
          </Link>
        </div>

        <nav className="basis-3/5">
          <ul className="flex capitalize border-r-2 ml-20">
            <li className="inline-block mx-6">
              <a href="#home">beranda</a>
            </li>
            <li className="inline-block mx-6">
              <a href="#panduan-pengguna">panduan pengguna</a>
            </li>
            <li className="inline-block mx-6">
              <a href="#bantuan">bantuan</a>
            </li>
            <li className="inline-block mx-6">
              <a href="#mulai-jualan">mulai jualan</a>
            </li>
          </ul>
        </nav>

        <div className="basis-1/5 flex justify-end">
          <Link to="/login" className="py-2 px-4 border-[1px] border-gogreen text-gogreen rounded-lg mr-5">
            Masuk
          </Link>
          <Link to="/daftar" className="py-2 px-4 border-[1px] bg-gogreen text-white rounded-lg">
            Daftar
          </Link>
        </div>
      </div>
    </div>
  );
};
