import React from 'react';
import thLogo from "../../assets/icon/thrifthouse-seller-logo.svg";
import { useSelector } from "react-redux";
import { NavLink } from 'react-router-dom';

export const HeaderAuth = () => {
const user = useSelector((state) => state.user);     
 
  return (
    <div className="fixed w-full bg-white z-50 top-0">
      <div className="flex justify-between items-center py-3 px-16 shadow-md">
        <NavLink to={"/"} className="cursor-pointer">
          <div className="basis-1/5">
            <img src={thLogo} className="h-12" alt="" />
          </div>
        </NavLink>

        <NavLink to={"/account"} className="cursor-pointer">
          <div className="basis-1/5 flex justify-end items-center">
            <img
              src={user.profileImg}
              className="rounded-full h-10 mr-1"
              alt=""
            />
            <p> Hi, {user.username.substring(0, 6)}</p>
          </div>
        </NavLink>
      </div>
    </div>
  );
}
