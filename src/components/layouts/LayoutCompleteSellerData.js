import React from 'react'
import {HeaderAuth} from './HeaderAuth';

export const LayoutCompleteSellerData = ({children}) => {
  return (
    <>
      <header>
        <HeaderAuth />
      </header>

      <main className="bg-[#F2F2F2]">{children}</main>
    </>
  );
}
