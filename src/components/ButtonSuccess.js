import React from 'react'
import Spinner from "../components/Spinner";

export const ButtonSuccess = ({isBtnDisable, onClick, type, databstarget, databstoggle, styleOverride, className, isLoading, children}) => {
  return (
    <button
      disabled={isBtnDisable}
      type={type && type}
      onClick={onClick && (() => onClick())}
      data-bs-toggle={databstoggle}
      data-bs-target={databstarget}
      className={`${
        styleOverride
          ? className
          : `rounded mt-6 mb-5 w-min-72 h-12 text-white py-3 px-6 mr-5 flex items-center justify-center  ${className}  ${
              isBtnDisable
                ? " bg-gray-disabled hover:bg-gray-disabled cursor-not-allowed "
                : "bg-gogreen hover:bg-green-600"
            }
             ${isLoading ? " opacity-60 cursor-not-allowed" : ""}
            `
      }`}
    >
      {isLoading ? <Spinner className={"border-l-slate-300"} />  : children}
    </button>
  );
}
