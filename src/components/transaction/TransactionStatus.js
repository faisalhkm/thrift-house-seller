// component that displays the status of a transaction with different color
const TransactionStatus = ({ status, packagingStatus }) => {
  let statusStyle = "";
  if (status === "Menunggu pembayaran") statusStyle = "bg-[#FBCA03]";

  if (status === "Pembayaran terkonfirmasi")
    statusStyle = "text-white bg-secondary";

  if (status === "Pesanan diproses") statusStyle = "text-white bg-[#2E63EA]";

  if (status === "Selesai") statusStyle = "text-white bg-[#49BC19]";

  if (status === "Pesanan ditolak") statusStyle = "text-white bg-[#FD622A]";

  if (packagingStatus === "Menunggu dikemas")
    statusStyle = "text-[#D8A901] bg-[#FFF8CC]";

  if (packagingStatus === "Pesanan dikemas")
    statusStyle = "text-[#2E63EA] bg-[#D5E0FB]";

  if (packagingStatus === "Pesanan dikirim")
    statusStyle = "text-[#343557] bg-[#CECFD7]";

  if (packagingStatus === "Pesanan diterima")
    statusStyle = "text-secondary bg-[#D4EEE1]";

  return (
    <span className={`text-xs py-1 px-2 rounded ${statusStyle}`}>
      {packagingStatus ? packagingStatus : status}
    </span>
  );
};

export default TransactionStatus;
