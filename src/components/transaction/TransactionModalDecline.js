import axios from "axios";
import { useSelector } from "react-redux";
import { useState } from "react";
import Spinner from "../Spinner";

const TransactionModalDecline = ({ setIsDeclineOpen }) => {
  const { transactionId } = useSelector((state) => state.transaction);
  const [loading, setLoading] = useState(false);
  const { id } = useSelector((state) => state.user);

  // const id = "db219924-101a-4cbf-a5f5-4777d005d805"; // for testing

  // handle submit button when get clicked and change the order status to rejected
  const submitHandler = () => {
    setLoading(true);
    axios
      .put(
        `https://thrifthouse.herokuapp.com/api/v1/seller/${id}/transaction/${transactionId}?order_status=ditolak`
      )
      .then((res) => {
        console.log(res.data.data);
        setIsDeclineOpen((prev) => !prev);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };

  return (
    <div className="thisismodal fixed top-0 left-0 w-full h-full z-[60] bg-[#1F1F1F]/60 flex justify-center items-center overflow-auto">
      <div className="thisismodalcontent p-10 rounded-2xl bg-white inline-block">
        <div className="font-bold text-2xl mb-5">Apakah kamu yakin?</div>
        <div className="font-medium mb-8">
          Pesanan yang sudah kamu tolak, tidak akan dilanjutkan oleh sistem.
        </div>
        <div className="flex justify-end">
          <button
            className="mr-4 py-[14px] px-[67px] rounded-lg text-[#FD622A] font-medium bg-white border border-[#FD622A]"
            onClick={() => setIsDeclineOpen((prev) => !prev)}
          >
            Batalkan
          </button>
          <button
            className="py-[14px] px-[44px] rounded-lg text-white font-medium bg-[#FD622A] w-[207px]"
            onClick={submitHandler}
            disabled={loading}
          >
            {loading ? (
              <Spinner size={"sm"} className={"mx-auto"} />
            ) : (
              "Tolak Pesanan"
            )}
          </button>
        </div>
      </div>
    </div>
  );
};

export default TransactionModalDecline;
