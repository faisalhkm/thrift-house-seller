import { Icon } from "@iconify/react";
import { useState } from "react";

const TransactionSearch = ({ setSort, setSearch, status }) => {
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [isFilterOpen, setIsFilterOpen] = useState(false);
  const [sortState, setSortState] = useState("Urutkan Order");
  const [query, setQuery] = useState("");

  // handle sorting button
  const sortHandler = (param, state) => {
    setSortState(state);
    setSort(param);
    setIsFilterOpen(false);
  };

  // handle when submit button get clicked
  const submitHandler = () => {
    setSearch(query);
  };

  // handle when reset button get clicked
  const resetHandler = () => {
    setSortState("Urutkan Order");
    setQuery("");
    setSearch("");
  };

  return (
    <div className="flex space-x-2">
      <div className="thisissearch border border-[#CECFD7] rounded-lg flex flex-1">
        <div className="searchContainer relative">
          <div
            className="px-2 py-3 cursor-pointer"
            onClick={() => setIsSearchOpen((prev) => !prev)}
          >
            <span className="text-sm pr-3">ID Transaksi</span>
            <Icon
              icon="eva:arrow-ios-downward-outline"
              height="16"
              className="inline"
            />
          </div>

          <div
            className={`searchdropdown absolute top-14 border border-[#CECFD7] rounded-lg py-1 bg-white ${
              isSearchOpen ? "block" : "hidden"
            }`}
          >
            <ul className="text-sm whitespace-nowrap">
              <li className="px-2 py-2 hover:bg-[#E6E6E6] cursor-pointer">
                ID Transaksi
              </li>
              <li className="px-2 py-2 hover:bg-[#E6E6E6] cursor-pointer">
                Nama Pembeli
              </li>
              <li className="px-2 py-2 hover:bg-[#E6E6E6] cursor-pointer">
                Tanggal Transaksi
              </li>
              <li className="px-2 py-2 hover:bg-[#E6E6E6] cursor-pointer">
                Total Harga
              </li>
            </ul>
          </div>
        </div>

        <div className="flex-1">
          <input
            type="text"
            name="search"
            id="search"
            placeholder="Cari Pesanan"
            value={query}
            className="w-full h-full outline-none placeholder:text-sm placeholder:text-[#AEAEBC] rounded-lg"
            onChange={(e) => setQuery(e.target.value)}
          />
        </div>
        {/* <div className="flex items-center px-5 cursor-pointer">
          <Icon
            icon="akar-icons:search"
            height="16"
            color="#AEAEBC"
            className="inline"
          />
        </div> */}
      </div>

      {status === "semua" && (
        <div className="filtercontainer relative">
          <div
            className="border border-[#CECFD7] rounded-lg px-2 py-3 cursor-pointer w-[243px] h-full flex justify-between items-center"
            onClick={() => setIsFilterOpen((prev) => !prev)}
          >
            <span
              className={`text-sm ${
                sortState === "Urutkan Order" && "text-lightgray"
              }`}
            >
              {sortState}
            </span>
            <Icon
              icon="eva:arrow-ios-downward-outline"
              height="16"
              className="inline"
            />
          </div>

          <div
            className={`filterdropdown absolute top-14 border border-[#CECFD7] rounded-lg py-1 bg-white ${
              isFilterOpen ? "block" : "hidden"
            }`}
          >
            <ul className="text-sm">
              <li
                className="px-2 py-2 hover:bg-[#E6E6E6] cursor-pointer"
                onClick={() =>
                  sortHandler("dibuat_terbaru", "Pesanan Dibuat (terbaru)")
                }
              >
                Pesanan Dibuat (terbaru)
              </li>
              <li
                className="px-2 py-2 hover:bg-[#E6E6E6] cursor-pointer"
                onClick={() =>
                  sortHandler("dibuat_terlama", "Pesanan Dibuat (terakhir)")
                }
              >
                Pesanan Dibuat (terakhir)
              </li>
              <li
                className="px-2 py-2 hover:bg-[#E6E6E6] cursor-pointer"
                onClick={() =>
                  sortHandler("dibayar_terbaru", "Pesanan Dibayar (terbaru)")
                }
              >
                Pesanan Dibayar (terbaru)
              </li>
              <li
                className="px-2 py-2 hover:bg-[#E6E6E6] cursor-pointer"
                onClick={() =>
                  sortHandler("dibayar_terlama", "Pesanan Dibayar (terlama)")
                }
              >
                Pesanan Dibayar (terlama)
              </li>
            </ul>
          </div>
        </div>
      )}

      <button
        className="bg-gogreen hover:bg-gogreen-hover py-3 w-[97px] text-sm text-white font-medium rounded-lg"
        onClick={submitHandler}
      >
        Cari
      </button>
      <button
        className="bg-white border border-gogreen hover:border-gogreen-hover py-3 w-[97px] text-sm text-gogreen font-medium rounded-lg"
        onClick={resetHandler}
      >
        Reset
      </button>
    </div>
  );
};

export default TransactionSearch;
