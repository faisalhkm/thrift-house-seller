import { Icon } from "@iconify/react";
import axios from "axios";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { transactionAdd } from "../../action";
import TransactionStatus from "./TransactionStatus";

const TransactionList = ({ item, setTransaction }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // handle if the detail button get clicked
  const detailHandler = () => {
    dispatch(transactionAdd({ transactionId: item.orderId }));
    navigate(`/transaction/detail`);
  };

  // handle if the delete button get clicked
  const deleteHandler = (orderId) => {
    axios
      .delete(
        `https://thrifthouse.herokuapp.com/api/v1/seller/transaction/${orderId}`
      )
      .then((res) => {
        setTransaction((prev) => {
          const cloneTransactions = [
            ...prev[0].transactionDetails.transactions,
          ];
          const newTransactions = cloneTransactions.filter(
            (item) => item.orderId !== orderId
          );
          return [
            {
              ...prev[0],
              transactionDetails: {
                ...prev[0].transactionDetails,
                transactions: newTransactions,
              },
            },
          ];
        });
      })
      .catch((err) => console.log(err));
  };

  return (
    <tr className="whitespace-nowrap border-b border-b-[#CECFD7] last:border-b-0 ">
      <td className="p-6 text-sm">{item.kodeTransaksi}</td>
      <td className="p-6 text-sm">{item.buyerName}</td>
      <td className="p-6 text-sm">{item.orderedAt}</td>
      <td className="p-6 text-sm">Rp{item.price.toLocaleString("id-ID")}</td>
      <td className="p-6 text-sm">
        {/* <span className="py-1 px-2 rounded bg-[#FBCA03]">{item.status}</span> */}
        <TransactionStatus
          status={item.status}
          packagingStatus={item.packagingStatus}
        />
      </td>
      <td className="p-6 text-[8px]">
        <div className="flex space-x-2">
          <span className="cursor-pointer" onClick={detailHandler}>
            <div className="text-center">
              <Icon icon="bxs:edit" height="12" className="inline" />
            </div>
            <div className="text-center">Detail</div>
          </span>
          <span
            className="cursor-pointer"
            onClick={() => deleteHandler(item.orderId)}
          >
            <div className="text-center">
              <Icon
                icon="icon-park-outline:delete"
                height="12"
                color="#FD622A"
                className="inline"
              />
            </div>
            <div className="text-center text-[#FD622A]">Hapus</div>
          </span>
        </div>
      </td>
    </tr>
  );
};

export default TransactionList;
