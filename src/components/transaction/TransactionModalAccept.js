import axios from "axios";
import { useSelector } from "react-redux";
import { useMemo, useState } from "react";
import Spinner from "../Spinner";

// function that return obj with different text based on the order status
const statusText = (response) => {
  if (response.status === "Menunggu pembayaran")
    return {
      desc: "Lakukan konfirmasi pesanan hanya jika bukti transfer sudah sesuai.",
      button: "Konfirmasi Pesanan",
    };
  if (response.status === "Pembayaran terkonfirmasi")
    return {
      desc: "Lakukan konfirmasi proses pesanan agar pembeli mengetahui update status pengiriman. Proses pesanan harus diselesaikan maksimal 3 hari.",
      button: "Proses Pesanan",
    };
  if (response.packagingStatus === "Menunggu dikemas")
    return {
      desc: "Lakukan konfirmasi pesanan dikemas agar pembeli mengetahui update status pengiriman. ",
      button: "Pesanan Dikemas",
    };
  if (response.packagingStatus === "Pesanan dikemas")
    return {
      desc: "Lakukan konfirmasi pesanan dikirim agar pembeli mengetahui update status pengiriman.",
      button: "Pesanan Dikirim",
    };
};

const TransactionModalAccept = ({ setIsAcceptOpen, response }) => {
  const { transactionId } = useSelector((state) => state.transaction);
  const [loading, setLoading] = useState(false);
  const { id } = useSelector((state) => state.user);

  // const id = "84eda57f-2308-42b8-90f2-e6f329dacac2"; // for testing

  // value of modal text status
  const modalText = useMemo(() => statusText(response), [response]);

  // handle when submit button get clicked and change the order status
  const submitHandler = () => {
    const params = () => {
      if (response.status === "Menunggu pembayaran")
        return "?order_status=terkonfirmasi";
      if (response.status === "Pembayaran terkonfirmasi")
        return "?order_status=diproses";
      if (response.packagingStatus === "Menunggu dikemas")
        return "?packaging_status=dikemas";
      if (response.packagingStatus === "Pesanan dikemas")
        return "?packaging_status=dikirim";
    };

    setLoading(true);
    axios
      .put(
        `https://thrifthouse.herokuapp.com/api/v1/seller/${id}/transaction/${transactionId}${params()}`
      )
      .then((res) => {
        // console.log(res.data.data);
        setIsAcceptOpen((prev) => !prev);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };

  return (
    <div className="thisismodal fixed top-0 left-0 w-full h-full z-[60] bg-[#1F1F1F]/60 flex justify-center items-center overflow-auto">
      <div className="thisismodalcontent p-10 rounded-2xl bg-white inline-block max-w-[692px]">
        <div className="font-bold text-2xl mb-5">Apakah kamu yakin?</div>
        <div className="font-medium mb-8">{modalText.desc}</div>
        <div className="flex justify-end">
          <button
            className="mr-4 py-[14px] px-[67px] rounded-lg text-gogreen font-medium bg-white border border-gogreen"
            onClick={() => setIsAcceptOpen((prev) => !prev)}
          >
            Batalkan
          </button>
          <button
            className="py-[14px] px-[22px] rounded-lg text-white font-medium bg-gogreen w-[207px]"
            onClick={submitHandler}
            disabled={loading}
          >
            {loading ? (
              <Spinner size={"sm"} className={"mx-auto"} />
            ) : (
              modalText.button
            )}
          </button>
        </div>
      </div>
    </div>
  );
};

export default TransactionModalAccept;
