// this component render a accept button based on the order status
const TransactionButton = ({ acceptHandler, response, storeId }) => {
  if (response.status === "Menunggu pembayaran") {
    return (
      <button
        className={`py-[14px] px-[22px] rounded-lg ${
          response.receipt
            ? "text-white bg-gogreen"
            : "text-[#B4B4B4] bg-gogreen-disabled"
        }`}
        onClick={acceptHandler}
        disabled={!response.receipt}
      >
        Konfirmasi Pesanan
      </button>
    );
  }

  if (response.status === "Pembayaran terkonfirmasi") {
    return (
      <button
        className="py-[14px] px-[22px] rounded-lg text-white bg-gogreen"
        onClick={acceptHandler}
      >
        Proses Pesanan
      </button>
    );
  }

  if (
    response.status === "Pesanan diproses" &&
    response.packagingStatus === "Menunggu dikemas"
  ) {
    return (
      <button
        className="py-[14px] px-[22px] rounded-lg text-white bg-gogreen"
        onClick={acceptHandler}
      >
        Pesanan Dikemas
      </button>
    );
  }

  if (
    response.status === "Pesanan diproses" &&
    response.packagingStatus === "Pesanan dikemas"
  ) {
    return (
      <button
        className="py-[14px] px-[22px] rounded-lg text-white bg-gogreen"
        onClick={acceptHandler}
      >
        Pesanan dikirim
      </button>
    );
  }

  if (
    response.status === "Pesanan diproses" &&
    response.packagingStatus === "Pesanan dikirim"
  ) {
    return null;
  }

  if (
    response.status === "Selesai" &&
    response.packagingStatus === "Pesanan diterima"
  ) {
    return (
      <a
        href={`${process.env.REACT_APP_BUYERURL}/toko/${storeId}`}
        target="_blank"
        rel="noreferrer"
        className="py-[14px] px-[22px] rounded-lg text-gogreen bg-white border border-gogreen"
      >
        Lihat Ulasan
      </a>
    );
  }
};

export default TransactionButton;
