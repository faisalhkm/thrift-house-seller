import React from 'react'

export const Button = (props) => {
  return (
    <button
      {...props}
      className={`${
        props.styleOverride
          ? props.className
          : `rounded mt-6 mb-5 w-min-72 h-12 text-white py-3 px-6 mr-5 flex items-center justify-center hover:bg-green-600 ${
              props.className
            }  ${
              props.isBtnDisable
                ? " bg-gray-disabled hover:bg-gray-disabled cursor-not-allowed "
                : ""
            }
             ${props.isLoading ? " opacity-60 cursor-not-allowed" : ""}
            `
      }`}
    >
      {props.children}
    </button>
  );
}
