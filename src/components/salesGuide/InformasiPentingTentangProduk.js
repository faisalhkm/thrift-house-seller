import React from 'react'
import imgModalKondisiProduct from "../../assets/img/img-modal-kondisi-product.jpg";
import imgModalUploadProduct from "../../assets/img/img-modal-upload-product.jpg";

export const InformasiPentingTentangProduk = () => {
  return (
    <div className='mt-4'>
      <h1 className='font-bold'>Pengambilan Foto Produk</h1>
      <p>
        Di ThriftHouse kamu ga semena-mena mengupload foto gitu aja ya! harus
        mengikuti syarat yang tertera di tombol “informasi” disamping “Foto
        Produk” disitu tertera dengan jelas bagaimana kamu harus memfoto produk
        kamu dengan baik dan jelas.
      </p>

      <img src={imgModalUploadProduct} alt="" className='w-1/2 m-auto my-3' />

      <h1 className='font-bold'>Peringkat Kondisi Untuk Produk</h1>
      <p>
        Di ThriftHouse sangat penting untuk menunjukan sebagai mana kondisi
        produk yang akan kamu jual, karena yang kamu jual adalah barang Thrift
        maka kondisi produk yang paling utama disini, dan kamu harus jujur ya
        tentang kondisi produk kamu! karena ini akan mempengaruhi performa
        tokomu!
      </p>

      <img src={imgModalKondisiProduct} alt="" className='w-1/2 m-auto my-3' />
    </div>
  );
}
