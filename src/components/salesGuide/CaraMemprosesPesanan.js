import React from 'react'
import imgPesananDikonfirmasi from "../../assets/img/img-pesanan-dikonfirmasi.jpg";
import imgPesananDikemas from "../../assets/img/img-pesanan-dikemas.jpg";
import imgPesananDikirim from "../../assets/img/img-pesanan-dikirim.jpg";
import imgModalPesananDikonfirmasi from "../../assets/img/img-modal-pesanan-dikonfirmasi.jpg";
import imgModalPesananDikemas from "../../assets/img/img-modal-pesanan-dikemas.jpg";
import imgModalPesananDikirim from "../../assets/img/img-modal-pesanan-dikirim.jpg";

export const CaraMemprosesPesanan = () => {
  return (
    <div>
      <h1 className="mb-2 font-bold">1. Proses Pesanan</h1>
      <p>
        Setelah kamu mengkonfirmasi pesanan dari Buyer / Pembeli, kamu bisa klik
        “Proses Pesanan” untuk memberitahu bahwa pesanan sudah dalam proses
      </p>
      <img src={imgPesananDikonfirmasi} alt="" className='my-3 w-1/2 m-auto'/>
      <img src={imgModalPesananDikonfirmasi} alt="" className='my-3 w-3/4 m-auto'/>

      <p>Setelah itu pastikan kamu sudah akan memproses pesanan ya!</p>

      <h1 className='mb-2 font-bold mt-6'>2. Pesanan Dikemas</h1>
      <p>
        Kamu bisa melakukan pengemasan produk kamu setelah kamu proses pesanan
        ya, waktu proses pengemasan di ThriftHouse Maksimal 3 x 24 Jam Dan ada
        pemberitahuan status yang akan ditujukan buat kamu dan buyer / pembeli
        dengan update label pesanan
      </p>
      <img src={imgPesananDikemas} alt="" className='my-3 w-1/2 m-auto'/>
      <img src={imgModalPesananDikemas} alt="" className='my-3 w-3/4 m-auto'/>

      <p>
        Jadi pastikan proses pengemasan produk yang akan kamu kirim sudah sesuai
        dan jangan sampai salah ya!
      </p>

      <h1 className='mb-2 font-bold mt-8'> 3. Pesanan Dikirim</h1>
      <p>
        Setelah proses pengemasan telah dilakukan dan selesai, kamu bisa
        langsung mengirim produk kamu ke Buyer melalui Layanan pengiriman yang
        telah kamu pilih. dan Status akan ter-update untuk kamu dan buyer /
        pembeli
      </p>
      <img src={imgPesananDikirim} alt="" className='my-3 w-1/2 m-auto'/>
      <img src={imgModalPesananDikirim} alt="" className='my-3 w-3/4 m-auto'/>
      <p className='mb-12'>
        Jadi pastikan proses pengemasan produk yang akan kamu kirim sudah sesuai
        dan jangan sampai salah ya!
      </p>
    </div>
  );
}
