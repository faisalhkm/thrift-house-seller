import React from 'react';
import imgJasaPengiriman from "../../assets/img/img-jasa-pengiriman.jpg";

export const MengaturJasaPengiriman = () => {
  return (
    <div>
      <p className="my-6">
        untuk jasa pengiriman, seller hanya bisa pilih salah satu jasa
        pengiriman yang sudah kami sediakan dibawah ini ya!
      </p>
      <img className='w-1/2 m-auto' src={imgJasaPengiriman} alt="" />
      <p className='mb-12 mt-6'>
        Jangan lupa di langkah pertama isi detail alamat toko dengan baik dan
        benar ya!
      </p>
    </div>
  );
}
