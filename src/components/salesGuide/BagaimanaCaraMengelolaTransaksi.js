import React from 'react'
import imgHalamanTransaksi from "../../assets/img/img-halaman-transaksi.jpg";
import imgButtonTransaksi from "../../assets/img/img-button-transaksi.jpg";

export const BagaimanaCaraMengelolaTransaksi = () => {
  return (
    <div>
      <h1>Halo ThriftHouse Seller,</h1>
      <p className="my-6">
        Begini cara mengelola transaksi di ThriftHouse Seller Center
      </p>
      <p>
        untuk diawal kamu akan mempelajari struktur garis besar dari halaman
        Transaksi ini di toko kamu ya!
      </p>

      <img src={imgHalamanTransaksi} className="mb-6 mt-10 m-auto w-1/2" alt="" />

      <h1 className="mb-4">Halaman Transaksi</h1>
      <p>
        Di halaman ini kamu bisa melihat keseluruhan halaman pada seluruh
        transaksi yang telah dibuat entah pesanan transaksi yang diterima atau
        pesana transaksi yang ditolak
      </p>

      <img src={imgButtonTransaksi} className="my-3 m-auto w-1/4" alt="" />

      <h1 className="mb-4">Detail pesanan</h1>
      <p>
        Untuk melihat lebih detil pesanan dari pembeli kamu, kamu bisa
        melihatnya di Ikon samping kana yang bertuliskan “Detil”
      </p>
      <p className='mt-10 mb-4'>Selamat Berjualan di ThriftHouse!</p>
    </div>
  );
}
