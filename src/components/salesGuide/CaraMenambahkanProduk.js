import React from 'react'
import imgFormProduct from "../../assets/img/img-form-product.jpg";
import imgAddProduct from "../../assets/img/img-add-product.jpg";

export const CaraMenambahkanProduk = () => {
  return (
    <div>
      <h1>Halo ThriftHouse Seller,</h1>
      <p className="6">
        Pada Halaman ini kamu bisa menambahkan beberapa foto dari foto utama
        dari keseluruhan produk, sampai foto detail kondisi produk. semua bagian
        foto wajib terpenuhi dan harus sesuai dengan syarat yang tertera ya!
      </p>

      <img src={imgFormProduct} className="my-3 m-auto w-1/2" alt="" />

      <p>
        Selanjutnya kamu bisa menambahkan informasi produk mulai dari nama
        produk, lebar, bahan produk, kategori produk, dan deskripsi produk
        dengan benar dan sesuai ya! jangan sampai salah karena itu nanti akan
        mempengaruhi performa tokomu terhadap buyer / pembeli
      </p>

      <img src={imgAddProduct} className="my-3 m-auto w-1/2" alt="" />

      <p className='mb-16'>
        Setelah selesai kamu isi semua kelengkapan data untuk produk kamu, kamu
        bisa klik <span className="font-bold mb-12">“Tambah Produk Baru”</span>
      </p>
    </div>
  );
}
