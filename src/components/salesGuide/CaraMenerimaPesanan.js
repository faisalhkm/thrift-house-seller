import React from 'react'
import imgTransaksiKosong from "../../assets/img/img-transaksi-kosong.jpg";
import imgTransaksi from "../../assets/img/img-transaksi.jpg";

export const CaraMenerimaPesanan = () => {
  return (
    <div>
      <h1 className="mb-4">Halo ThriftHouse Seller, </h1>
      <p>
        Pada Halaman Detil pesanan ini kamu bisa melihat rincian pesanan yang
        telah dibuat oleh Pembeli kamu, mulai dari ID Pembayaran, Jumlah
        pesanan, Harga, siapa yang beli, tanggal berapa si pembeli checkout
      </p>
      <img src={imgTransaksiKosong} alt="" className="my-3 w-1/2 m-auto" />
      <p className="mb-4">
        Kondisi: pembeli <span className="font-bold">belum</span> mengupload
        bukti pembayaran Sayangnya,
      </p>
      <p>
        ThriftHouse baru bisa melakukan pembayaran dengan transfer manual, yaitu
        si pembeli harus meng-upload bukti pembayaran jika si pembeli sudah
        membayar, dan kita harus mengecek sendiri secara manual apakah bukti
        pembayaran valid atau tidak
      </p>
      <img src={imgTransaksi} alt="" className="my-3 w-1/2 m-auto" />
      <p className="mb-4">
        Kondisi: pembeli <span className="font-bold">sudah</span> mengupload
        bukti pembayaran
      </p>
      <p>
        Jika pembeli sudah mengupload bukti pembayaran, maka otomatis 2 tombol /
        button dibawah aktif (dalam kondisi kita penjual bisa menerima pesanan
        untuk dikemas dan dikirim atau ditolak karena bukti pembayaran tidak
        valid)
      </p>
    </div>
  );
}
