import React from 'react'
import imgStepBar from '../../assets/img/img-step-bar.jpg'

export const LangkahmulaiBerjualanSeller = () => {
  return (
    <div>
      <h1 className="mb-8">Halo ThriftHouse Seller,</h1>
      <p>
        Selamat bergabung sebagai ThriftSeller Seller! Untuk memulai kegiatan
        berjualan kamu, pastikan untuk menyelesaikan tiga langkah awal ini ya:
      </p>

      <img className="mt-8 mb-2" src={imgStepBar} alt="" />

      <h1 className="mb-1">1. Melengkapi informasi toko dan seller</h1>
      <p>
        Lengkapi data toko kamu, dan dokumen legal seperti KTP pemilik toko.
        Sebelum menyelesaikan bagian ini, kamu tidak dapat masuk ke langkah
        kedua, yaitu mengatur jasa pengiriman.
      </p>

      <h1 className="mb-1 mt-8">2. Mengatur Jasa pengiriman</h1>
      <p>
        Segera lengkapi Jasa pengiriman kamu, seperti lokasi alamat toko kamu
        supaya ThriftHouse mengetahui area pengiriman asal produk-produk kamu
        agar makin mudah untuk mengatur toko
      </p>

      <h1 className="mb-1 mt-8">3. Mengatur Rekening</h1>
      <p>
        Segera lengkapi informasi pembayaran seperti detail nomor rekening serta
        dokumen-dokumen legal di toko kamu. Sebelum kamu menyelesaikan bagian
        ini, kamu tidak dapat menambahkan produk di toko kamu.
      </p>

      <p className='my-12'>Selamat Berjualan di ThriftHouse!</p>
    </div>
  );
}
