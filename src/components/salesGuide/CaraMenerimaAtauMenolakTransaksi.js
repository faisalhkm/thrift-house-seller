import React from 'react'
import imgModalImgPembayaran from "../../assets/img/img-modal-img-pembayaran.jpg";
import imgModalKonfirmasiPesanan from "../../assets/img/img-modal-konfirmasi-pesanan.jpg";
import imgModalTolakPesanan from "../../assets/img/img-modal-tolak-pesanan.jpg";

export const CaraMenerimaAtauMenolakTransaksi = () => {
  return (
    <div>
      <h1 className="mb-4">Halo ThriftHouse Seller,</h1>
      <p>
        Langkah pertama, Cek ke valid-an bukti transfer dari pembeli, apakah
        sudah sesuai dengan data yang diberikan, atau belum sesuai. Untuk
        mengetahui detail lebih lanjut, bisa klik di kolom bukti pembayaran
      </p>

      <img src={imgModalImgPembayaran} alt="" className="my-3 m-auto w-1/2" />

      <p>
        Selanjutnya, jika bukti pembayaran
        <span className="font-bold">sudah valid,</span> kamu bisa konfirmasi
        pesanan dengan klik
        <span className="font-bold">“Konfirmasi Pesanan”</span> maka otomatis
        akan
      </p>
      <img src={imgModalKonfirmasiPesanan} alt="" className="my-3 m-auto w-1/2" />
      <p>
        atau, jika bukti pembayaran
        <span className="font-bold">tidak valid,</span> kamu bisa menolak
        pesanan dengan klik <span className="font-bold">“tolak Pesanan”</span>
        maka otomatis akan membatalkan pesanan produk yang di beli oleh pembeli
      </p>
      <img src={imgModalTolakPesanan} alt="" className="my-3 m-auto w-1/2" />
    </div>
  );
}
