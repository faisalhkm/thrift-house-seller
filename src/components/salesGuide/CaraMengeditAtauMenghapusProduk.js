import React from 'react'
import imgEditAtauHapusProduct from "../../assets/img/img-edit-atau-hapus.jpg";
import imgPilihProduct from "../../assets/img/img-pilih-product.jpg";

export const CaraMengeditAtauMenghapusProduk = () => {
  return (
    <div>
      <p className="mb-6">
        Untuk mengedit atau menghapus produk, langkah yang bisa kamu lakukan
        adalah pergi ke “katalog Produk” dan kamu bisa langsung “Klik” saja
        produk kamu
      </p>

      <img src={imgPilihProduct} className="my-3 m-auto w-1/2" alt="" />

      <p className="mb-8">
        Selanjutnya, jika bukti pembayaran
        <span className="font-bold">sudah valid,</span> kamu bisa konfirmasi
        pesanan dengan klik
        <span className="font-bold">“Konfirmasi Pesanan”</span> maka otomatis
        akan
      </p>

      <img src={imgEditAtauHapusProduct} className="my-3 m-auto w-1/2" alt="" />

      <p className="mb-12">
        Setelah kamu edit kamu bisa pilih tombol “Hapus Produk” jika ingin
        menghapus produk, atau kamu bisa pilih tombol “Edit Produk” agar produk
        yang kamu ubah detailnya akan tersimpan perubahannya
      </p>
    </div>
  );
}
