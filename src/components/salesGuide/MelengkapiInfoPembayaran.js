import React from 'react'
import imgFormPembayaran from "../../assets/img/img-form-pembayaran.jpg";

export const MelengkapiInfoPembayaran = () => {
  return (
    <div>
      <p>
        Segera lengkapi informasi pembayaran seperti detail nomor rekening serta
        dokumen-dokumen legal di toko Anda. Sebelum Anda menyelesaikan bagian
        ini, Anda tidak dapat menambahkan produk di toko Anda.
      </p>
      <img src={imgFormPembayaran} className="my-3 w-1/2 m-auto" alt="" />
      <p>
        Pastikan no. rekening dan pemilik benar dan sama ya! setelah itu klik
        simpan
      </p>
    </div>
  );
}
