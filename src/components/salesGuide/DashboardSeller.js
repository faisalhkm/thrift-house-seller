import React from 'react'
import imgDashboardHome from "../../assets/img/img-dashboard-home.jpg";

export const DashboardSeller = () => {
  return (
    <div>
      <p>
        Setelah semua telah selesai di Isi, kamu bakal langsung diarahkan ke
        halaman utama Dashboard nih!
      </p>
      <img
        src={imgDashboardHome}
        className="my-3 w-1/2 m-auto"
        alt=""
      />
      <h1 className='mt-6 font-bold'>&bull; Total Produk</h1>
      <p>
        Kamu bisa melihat berapa banyak produk keseluruhan yang pernah kamu
        jual/ upload
      </p>
      <h1 className='mt-6 font-bold'>&bull; Pendapatan</h1>
      <p>
        Kamu bisa melihat berapa banyak pendapatan bersih yang sudah kamu
        dapatkan dari hasil penjualan baju kamu di ThriftHouse
      </p>
      <h1 className='mt-6 font-bold'>&bull; Transaksi</h1>
      Kamu bisa melihat berapa banyak pendapatan Kotor belum dipotong pajak yang
      sudah kamu dapatkan dari hasil penjualan baju kamu di ThriftHouse
      <p></p>
      <h1 className='mt-6 font-bold'>&bull; Rata - Rata Ulasan</h1>
      Setelah pembeli menerima pesanan dari toko kamu, pembeli akan memberikan
      ulasan, danrata rata ulasan akan otomatis terkonfersi di halaman Dashboard
      <p>
        Setelah pembeli menerima pesanan dari toko kamu, pembeli akan memberikan
        ulasan, danrata rata ulasan akan otomatis terkonfersi di halaman
        Dashboard
      </p>
      <h1 className='mt-6 font-bold'>&bull; Total Favorit</h1>
      <p className='mb-8'>
        Kamu bisa tahu seberapa banyak orang yang menjadikan toko-mu sebagai
        toko Toko Favorit mereka!
      </p>
    </div>
  );
}
