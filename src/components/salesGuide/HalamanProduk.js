import React from 'react'
import imgHalamanProduct from "../../assets/img/img-halaman-product.jpg";
import imgHalamanAddProduct from "../../assets/img/img-halaman-addproduct.jpg";

export const HalamanProduk = () => {
  return (
    <div>
      <p> Halo ThriftHouse Seller,</p>
      <p className="my-6">
        Begini cara mengelola Produk kamu di ThriftHouse Seller Center
      </p>
      <p>
        untuk diawal kamu akan mempelajari struktur garis besar dari halaman
        Transaksi ini di toko kamu ya!
      </p>
      <img src={imgHalamanProduct} alt="" className="w-1/2 m-auto mb-3 mt-6" />
      <p>Halaman Katalog produk</p>
      <p className="mt-8">
        Di halaman ini kamu bisa semua produk yang kamu jual, dan kamu bisa
        mengelola produk yang kamu jual di halaman Katalog Produk ini.
      </p>
      <img src={imgHalamanAddProduct} className="w-1/2 m-auto my-3" alt="" />
      <p>
        Untuk memulainya kamu bisa klik
        <span className="font-bold">“Katalog Produk”</span> lalu bisa klik{" "}
        <span className="font-bold">“+ Tambah Produk Baru”</span>
      </p>
      <p className="mt-12 mb-16">Selamat Berjualan di ThriftHouse!</p>
    </div>
  );
}
