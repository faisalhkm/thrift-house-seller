import React from 'react';
import imgInformasiToko from "../../assets/img/img-form-informasi-toko.jpg";
import imgInformasiSeller from "../../assets/img/img-form-informasi-seller.jpg";

export const MelengkapiInformasiTokoDanSeller = () => {
  return (
    <div>
      <h1 className="mb-8"> Halo ThriftHouse Seller,</h1>
      <p>
        Yuk, mulai kegiatan berjualan kamu dengan terlebih dahulu mendaftarkan
        alamat toko kamu di ThriftHouse Seller dengan langkah berikut:
      </p>
      <p className="my-4">
        1. Pada bagian informasi toko dan seller, isi data dengan benar ya!
      </p>
      <img src={imgInformasiToko} className="w-1/2 m-auto" alt="" />
      <img src={imgInformasiSeller} className="w-1/2 m-auto" alt="" />
      <p className='mt-4 mb-12'>
        Isi detail, nama dan nomor telepon yang bisa dihubungi di lokasi alamat
        kamu, dan pastikan alamat toko yang kamu masukan benar ya! agar
        mempermudah pihak pengiriman menemukan lokasi toko kamu.
      </p>
    </div>
  );
}
