import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import imgRegister from "../assets/img/img-register.png";
import { InputText } from '../components/forms/InputText';
import { ButtonSuccess } from '../components/ButtonSuccess';
import { globalAxios } from '../config/configApi';
import { HeaderNotAuth } from '../components/layouts/HeaderNotAuth';
import { AiOutlineEyeInvisible, AiOutlineEye } from "react-icons/ai";

export const Register = () => {

 const [data, setData] = useState({
   username: "",
   email   : "",
   phone    : "",
   password: "",
   role    : "ROLE_SELLER",
 });

 const [isLoading, setIsLoading]           = useState(false);
 const [isBtnDisable, setisBtnDisable]     = useState(true);
 const [isLoginfail, setIsLoginfail]       = useState(false);
 const [errMsg, setErrMsg]                 = useState(false);
 const [isShowPassword, setIsShowPassword] = useState(false);

 const navigate = useNavigate();

 useEffect(() => {
   if (
     data.username !== "" &&
     data.phone !== "" &&
     data.email !== "" &&
     data.password !== ""
   ) {
     setisBtnDisable(false);
    }else{
     setisBtnDisable(true);
   }
  
 }, [data])
 

 const handleChange = (e) => {
      setData({ ...data, [e.target.name]: e.target.value });
 };

 const handleSubmit = async (e) => {
   e.preventDefault();
   setIsLoading(true);
   setIsLoginfail(false);

   try {
    console.log(data);
     const response = await globalAxios.post("/register", data);
     
     if (response.status === 200) navigate("/login")

     setIsLoading(false);
     setIsLoginfail(true);
      setErrMsg("");

   } catch (error) {
     setIsLoading(false);
     setIsLoginfail(true);
     const errMsg = "yang anda masukkan sudah digunakan";
     if (error.response.data.message === "Username already exists!"){
      setErrMsg(`Username ${errMsg}`);
    }
    if (error.response.data.message === "Email already exists!") {
       setErrMsg(`Email ${errMsg}`);
     }
     
   }
 };

  return (
    <>
      <header>
        <HeaderNotAuth />
      </header>

      <form onSubmit={(e) => handleSubmit(e)}>
        <div className="flex justify-center px-16 pt-32 min-h-screen bg-[#F2F2F2]">
          <div className="flex-1 flex justify-center">
            <div className="w-5/6 mt-16 ">
              <img src={imgRegister} alt="" />
            </div>
          </div>
          <div className="flex-1 mb-16">
            <div className="w-5/6 border px-8 shadow-md rounded-xl py-6 bg-white">
              <div>
                <div className="text-center mt-4">
                  <h1 className="font-bold text-[#29A867] text-3xl">
                    Daftar sebagai akun seller
                  </h1>
                  <p className="text-slate-400 text-sm mt-1 mb-6 sm:text-lg">
                    Masukkan data untuk melanjutkan
                  </p>
                </div>

                <div>
                  <label className="" htmlFor="username">
                    <span className="text-sm leading-5 mb-1 block">
                      Username
                    </span>
                  </label>
                  <InputText
                    placeholder="Masukkan username"
                    name="username"
                    value={data.username}
                    onChange={handleChange}
                  />
                </div>

                <div className="mt-4">
                  <label className="" htmlFor="password">
                    <span className="text-sm leading-5 mb-1 block">Email</span>
                  </label>
                  <InputText
                    name="email"
                    placeholder="Masukkan email mu"
                    value={data.email}
                    onChange={handleChange}
                  />
                </div>

                <div className="mt-4">
                  <label className="" htmlFor="password">
                    <span className="text-sm leading-5 mb-1 block">No HP</span>
                  </label>
                  <InputText
                    name="phone"
                    type="number"
                    placeholder="Masukkan Nomor Telephone kamu"
                    value={data.phone}
                    onChange={handleChange}
                  />
                </div>

                <div className="mt-4">
                  <label className="" htmlFor="password">
                    <span className="text-sm leading-5 mb-1 block">
                      Kata Sandi
                    </span>
                  </label>
                  <div className="relative">
                    <InputText
                      type={isShowPassword ? "text" : "password"}
                      name="password"
                      placeholder="Masukkan kata sandi kamu"
                      value={data.password}
                      onChange={(e) => handleChange(e)}
                    />

                    {isShowPassword ? (
                      <AiOutlineEye
                        className="absolute top-[4px] right-[8px] text-gray-400 hover:text-gray-600 cursor-pointer"
                        size={"28px"}
                        onClick={() => setIsShowPassword(!isShowPassword)}
                      />
                    ) : (
                      <AiOutlineEyeInvisible
                        className="absolute top-[4px] right-[8px] text-gray-400 hover:text-gray-600 cursor-pointer"
                        size={"28px"}
                        onClick={() => setIsShowPassword(!isShowPassword)}
                      />
                    )}
                  </div>
                </div>
              </div>
              <div className="flex flex-col mt-10 lg:mt-1">
                <ButtonSuccess
                  isBtnDisable={isBtnDisable}
                  disabled={isBtnDisable}
                  isLoading={isLoading}
                  type="submit"
                  className={" mt-6 w-full"}
                >
                  <p className="inline-block">Daftar</p>
                </ButtonSuccess>
              </div>
              <p className="text-gray-disabled">
                Belum jadi Seller ThriftHouse?
                <span className="text-gogreen ml-2 font-medium">
                  <Link to={"/login"}>Masuk Disini</Link>
                </span>
              </p>
            </div>
          </div>
        </div>
      </form>

      {/* Message Error When Login fail */}
      {isLoginfail && (
        <div className="absolute top-24 left-0 flex justify-center w-full text-sm">
          <div className="px-5 py-[5px] rounded-3xl  text-center text-[#D8421D] bg-[#FDEBD3] border-[#D8421D] border mb-2 font-medium capitalize">
            <p>{errMsg} </p>
          </div>
        </div>
      )}
    </>
  );
}
