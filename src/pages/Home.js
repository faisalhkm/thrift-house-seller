import React, { useEffect, useState } from 'react'
import star from "../assets/img/star.png";
import shippingBag from "../assets/img/shippingBag.png";
import transactionPng from "../assets/img/transaction.png";
import money from "../assets/img/money.png";
import love from "../assets/img/love.png";
import statistik from "../assets/img/statistik.png";
import detailIcon from "../assets/icon/detail.svg";
import { CardDashboard } from '../components/CardDashboard';
import { Card } from '../components/Card';
import Spinner from '../components/Spinner';
import { MsgDataEmpty } from '../components/MsgDataEmpty';
import { convertToRupiah } from "../utils/convert";
import { useDispatch, useSelector } from "react-redux";
import { globalAxiosWithVesion } from '../config/configApi';
import { transactionAdd } from '../action';
import { useNavigate } from 'react-router-dom';

export const Home = () => {
  const [cardData, setCardData] = useState("");
  const [transactionsData, setTransactionsData] = useState([]);
  const [isLoadingApi, setIsLoadingApi] = useState(true);
  const user = useSelector((state) => state.user);
  const [isLoadingLoadMore, setIsLoadingLoadMore] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    const loadData = async () => {
      setIsLoadingApi(true);
      try {
        const response = await globalAxiosWithVesion.get(`/seller/${user.id}/summary?size=8
    `);
        setCardData(response.data.data);
        setTransactionsData(response.data.data.transactionDetails);
        setIsLoadingApi(false);
      } catch (error) {
        console.log(error);
        setIsLoadingApi(false);
      }
    };

    loadData();
  }, []);

  const checkStatus = (status) => {
    if (status.toLowerCase() === "menunggu pembayaran")
      return " bg-yellow-400 ";
    if (status.toLowerCase() === "pembayaran terkonfirmasi")
      return " bg-gogreen text-white ";
    if (status.toLowerCase() === "pesanan diproses")
      return " bg-[#2E63EA] text-white ";
    if (status.toLowerCase() === "selesai") return " bg-[#49BC19] text-white ";
  };

  // handle if the detail button get clicked
  const detailHandler = (item) => {
    dispatch(transactionAdd({ transactionId: item.orderId }));
    navigate(`/transaction/detail`);
  };

   const handleLoadMore = async () => {
     const page = transactionsData.currentPage + 1;
     setIsLoadingLoadMore(true);

     try {
       const response = await globalAxiosWithVesion.get(`/seller/${user.id}/summary?size=8&page=${page}`);
       setIsLoadingLoadMore(false);
       setTransactionsData({
         ...response.data.data.transactionDetails,
         transactions: [
           ...transactionsData.transactions,
           ...response.data.data.transactionDetails.transactions,
         ],
       });
     } catch (error) {
       console.log(error);
       setIsLoadingLoadMore(false);
     }
   };

  return (
    <>
      <h1 className="capitalize font-medium text-2xl mb-1">dashboard</h1>
      <p className="text-gray-400 text-base mb-4">
        Lihat ringkasan tokomu disini
      </p>

      {/* Loading Bar Card Dashboard*/}
      {isLoadingApi && (
        <div className="flex fixed top-0 left-0 right-0 ml-[20%] justify-center h-screen items-center">
          <Spinner className="border-gray-disabled" />
        </div>
      )}
      {/* Card Dashboard */}
      {cardData && !isLoadingApi && (
        <section className="grid grid-cols-5 gap-8">
          <CardDashboard
            img={shippingBag}
            title={"total produk"}
            total={`${cardData.storeSummary.totalProduct} produk`}
          />
          <CardDashboard
            img={money}
            title={"pendapatan"}
            total={`${convertToRupiah(cardData.storeSummary.revenue)}`}
          />
          <CardDashboard
            img={transactionPng}
            title={"transaksi"}
            total={`${convertToRupiah(cardData.storeSummary.transaction)}`}
          />
          <CardDashboard
            img={star}
            title={"rata-rata ulasan"}
            total={`${cardData.storeSummary.averageReview} ulasan`}
          />
          <CardDashboard
            img={love}
            title={"total favorit"}
            total={`${cardData.storeSummary.favoriteStore} orang`}
          />
        </section>
      )}

      {transactionsData && !isLoadingApi && (
        <Card className="bg-white mt-6">
          {/* Transaksi Terbaru */}
          <h1 className="capitalize font-medium text-2xl mb-1 ml-3 ">
            Transaksi Terbaru
          </h1>

          <div className="px-4 mt-4 flex justify-between items-center text-sm">
            <div className="w-[19%]">ID Transaksi</div>
            <div className="w-[10%]">Pembeli</div>
            <div className="w-[13%]">Tgl Pembelian</div>
            <div className="w-[13%]">Total Harga</div>
            <div className="w-[27%]">Status</div>
            <div className="w-[10%]">Aksi</div>
          </div>

          {/* List Transaction */}
          {transactionsData.transactions.map((ts, i) => {
            return (
              <div
                key={i}
                className="py-2 mx-4 border-[#E5E5E5] border-b-2 text-sm  flex justify-between items-center"
              >
                <div className="w-[19%]">{ts.kodeTransaksi}</div>
                <div className=" w-[10%] ">{ts.buyerName.substring(0, 10)}</div>
                <div className=" w-[13%] ">{ts.orderedAt}</div>
                <div className=" w-[13%]">{convertToRupiah(ts.price)}</div>
                <div className=" w-[27%] ">
                  <p
                    className={`inline px-2 py-1 rounded-md ${checkStatus(
                      ts.status
                    )}`}
                  >
                    {ts.status}
                  </p>
                </div>
                <div className=" w-[10%] ">
                  <button
                    className="flex items-center p-2 border-2 rounded-md"
                    onClick={() => detailHandler(ts)}
                  >
                    <img src={detailIcon} className="h-4 mr-1" alt="" />
                    <p>Detail</p>
                  </button>
                </div>
              </div>
            );
          })}

          {/* Transaksi Empty */}
          {transactionsData.transactions.length === 0 && !isLoadingApi && (
            <MsgDataEmpty className="">
              <div className="flex-row text-center mb-44">
                <img
                  src={statistik}
                  className="h-72 inline-block mb-4"
                  alt=""
                />
                <p>Tidak ada transaksi terbaru saat ini</p>
              </div>
            </MsgDataEmpty>
          )}
        </Card>
      )}

      {transactionsData &&
        !isLoadingApi &&
        transactionsData.currentPage < transactionsData.totalPages - 1 && (
          <button
            className="py-3 bg-gogreen hover:bg-green-600 rounded-lg text-white w-[275px] mx-auto block mt-4"
            onClick={handleLoadMore}
            disabled={isLoadingLoadMore}
          >
            {isLoadingLoadMore ? (
              <Spinner size={"sm"} className={"mx-auto"} />
            ) : (
              "Lihat Lainnya"
            )}
          </button>
        )}
    </>
  );
}
