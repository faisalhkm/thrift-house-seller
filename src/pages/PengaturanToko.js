import { useState } from "react";
import AkunBank from "../components/pengaturanToko/AkunBank";
import FormAturToko from "../components/pengaturanToko/FormAturToko";
import Pengiriman from "../components/pengaturanToko/Pengiriman";

const Transaction = () => {
  const [tabs, setTabs] = useState("Informasi Toko");

  const tabsHandler = (e) => {
    // console.log(e.target.textContent);
    setTabs(e.target.textContent);
  };
  return (
    <div className=" inner">
      <h3 className="text-2xl font-medium mb-2">Pengaturan Toko</h3>
      <p className="text-[#9191A9] mb-4">Kelola tokomu disini</p>

      <div className="thisissubnav bg-white h-full overflow-auto rounded-md shadow-md">
        <div className="px-0">
          <div className="thisistabs flex mb-10 justify-around items-center ">
            <div
              className={`p-[10px] sm:px-1 sm:py-4 text-xs lg:text-base cursor-pointer ${
                tabs === "Informasi Toko"
                  ? "text-gogreen border-b-[1px] border-b-gogreen"
                  : "text-[#AEAEBC]"
              } `}
              onClick={tabsHandler}
            >
              Informasi Toko
            </div>
            <div
              className={`p-[10px] sm:px-1 sm:py-4 text-xs lg:text-base cursor-pointer ${
                tabs === "Akun Bank"
                  ? "text-gogreen border-b-[1px] border-b-gogreen"
                  : "text-[#AEAEBC]"
              }`}
              onClick={tabsHandler}
            >
              Akun Bank
            </div>

            <div
              className={`p-[10px] sm:px-1 sm:py-4 text-xs lg:text-base cursor-pointer ${
                tabs === "Pengiriman"
                  ? "text-gogreen border-b-[1px] border-b-gogreen"
                  : "text-[#AEAEBC]"
              }`}
              onClick={tabsHandler}
            >
              Pengiriman
            </div>
          </div>
        </div>
        <div className="thisissearch px-6 py-8">
          {tabs === "Informasi Toko" ? <FormAturToko /> : ""}
          {tabs === "Akun Bank" ? <AkunBank /> : ""}
          {tabs === "Pengiriman" ? <Pengiriman /> : ""}
        </div>
      </div>
    </div>
  );
};

export default Transaction;
