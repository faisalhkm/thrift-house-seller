import React, { useEffect, useState } from 'react'
import { Link, NavLink, useParams } from 'react-router-dom'
import { Button } from '../../components/Button'
import { InputText } from '../../components/forms/InputText'
import { InputTextArea } from '../../components/forms/InputTextArea'
import { InputTextGroup } from '../../components/forms/InputTextGroup'
import { SelectedBox } from '../../components/forms/SelectedBox'
import { globalAxiosWithVesion } from "../../config/configApi";
import { photosText } from '../../datas_statis/products'
import { ModalConfirm } from '../../components/modals/ModalConfirm'
import { useNavigate } from "react-router-dom";
import { closeModal } from '../../utils/modal'
import Spinner from '../../components/Spinner'
import iconInfo from "../../assets/icon/info.svg";
import { ModalInfoProduct } from '../../components/ModalInfoProduct'

export const ProductDetail = () => {

      const [data, setData] = useState("");
      const [isLoadingApi, setIsLoadingApi] = useState(false);
      const { productId } = useParams();
      const [isModalLoading, setIsModalLoading] = useState(false);
      const navigate = useNavigate();
  
      useEffect(() => {
        const getProduct = async () => {
            setIsLoadingApi(true);
          try {
            const response = await globalAxiosWithVesion.get(
              `/products/${productId}/edit`
            );
            // console.log(response.data.data);
            setIsLoadingApi(false);
            setData(response.data.data)
          } catch (error) {
            console.log(error);
            setIsLoadingApi(false);
          }
        };
        getProduct();
  }, []);

  const handleClickModal = async () => {
    setIsModalLoading(true);
   try {
        const response = await globalAxiosWithVesion.delete(
          `/products/${productId}`);
          navigate("/products");
          setIsModalLoading(false);
      } catch (error) {
        setIsModalLoading(false);
        console.log(error);
      }
     closeModal();
  }

  return (
    <>
      {/* Loading Bar Product*/}
      {isLoadingApi && (
        <div className="flex fixed top-0 left-0 right-0 ml-[20%] justify-center h-screen items-center">
          <Spinner className="border-gray-disabled" />
        </div>
      )}

      {data && (
        <>
          <p className="capitalize text-lg mb-2">
            <NavLink to="/products" className={"cursor-pointer"}>
              <span className="text-gray-400">katalog produk</span>
            </NavLink>
            <span className="mx-4">/</span>
            <span>tambah produk baru</span>
          </p>
          <div className="bg-white rounded-md  px-8 py-10 shadow-md">
            {/* Upload Foto Produk */}
            <p>
              Foto produk
              <img
                src={iconInfo}
                className="cursor-pointer inline-block w-5 h-5 ml-1"
                alt=""
                data-bs-toggle={"modal"}
                data-bs-target={"#ModalInfoImageProduct"}
              />
            </p>
            <div className="grid grid-cols-4 gap-x-10 my-3">
              {data.photos.map((photo, i) => (
                <div className="flex-row text-center" key={i}>
                  <img
                    src={photo}
                    className="h-52 w-full shadow-md rounded-md"
                    alt=""
                  />
                  <p className="text-sm mt-2">{photosText[i]}</p>
                </div>
              ))}
            </div>

            <div className="grid grid-cols-3 gap-x-12 gap-y-6">
              {/* Nama Brand */}
              <div className="">
                <label className="">Nama brand</label>
                <InputText
                  className="cursor-not-allowed"
                  value={data.brand}
                  disabled
                />
              </div>

              {/* Nama Produk */}
              <div className="col-span-2">
                <label className="">Nama produk</label>
                <InputText
                  className="cursor-not-allowed"
                  value={data.name}
                  disabled
                />
              </div>

              {/* Kategori */}
              <div className="col-span-1">
                <label className="">Kategori</label>
                <SelectedBox
                  className="cursor-not-allowed"
                  disabled
                  datas={[data.category]}
                />
              </div>

              {/* Sub-kategori 1 */}
              <div className="col-span-1">
                <label className="">Sub-kategori 1</label>
                <SelectedBox
                  className="cursor-not-allowed"
                  disabled
                  datas={[data.subcategory1]}
                />
              </div>

              {/* Sub-kategori 2 */}
              <div className="col-span-1">
                <label className="">Sub-kategori 2</label>
                <SelectedBox
                  className="cursor-not-allowed"
                  disabled
                  datas={[data.subcategory2]}
                />
              </div>

              {/* Panjang */}
              <div className="col-span-1">
                <label className="">Panjang</label>
                <InputTextGroup
                  className="cursor-not-allowed"
                  value={data.height}
                  disabled
                  placeholder="Masukkan panjang"
                  type="number"
                  icon={"cm"}
                  iconposition="right"
                />
              </div>

              {/* Lebar */}
              <div className="col-span-1">
                <label className="">Lebar</label>
                <InputTextGroup
                  className="cursor-not-allowed"
                  value={data.width}
                  disabled
                  placeholder="Masukkan lebar"
                  type="number"
                  icon={"cm"}
                  iconposition="right"
                />
              </div>

              {/* Berat */}
              <div className="col-span-1">
                <label className="">Berat</label>
                <InputTextGroup
                  className="cursor-not-allowed"
                  value={data.weight}
                  disabled
                  placeholder="Masukkan berat"
                  type="number"
                  icon={"gr"}
                  iconposition="right"
                />
              </div>

              {/* Kondisi */}
              <div className="col-span-1">
                <label className="">
                  Kondisi
                  <img
                    src={iconInfo}
                    className="ml-1 cursor-pointer inline-block w-5 h-5"
                    alt=""
                    data-bs-toggle={"modal"}
                    data-bs-target={"#ModalInfoKondisiProduct"}
                  />
                </label>
                <SelectedBox
                  className="cursor-not-allowed"
                  datas={[data.condition]}
                  disabled
                />
              </div>

              {/* Ukuran */}
              <div className="col-span-1">
                <label className="">Ukuran</label>
                <SelectedBox
                  className="cursor-not-allowed"
                  datas={[data.size]}
                  disabled
                />
              </div>

              {/* Harga */}
              <div className="col-span-1">
                <label className="">Harga</label>
                <InputTextGroup
                  className="cursor-not-allowed"
                  value={data.price}
                  disabled
                  type="number"
                  iconposition="left"
                  icon="Rp."
                />
              </div>

              {/* Nama Bahan produk */}
              <div className="col-span-2">
                <label className="">Bahan produk</label>
                <InputText
                  className="cursor-not-allowed"
                  value={data.material}
                  disabled
                />
              </div>

              {/* Deskripsi produk */}
              <div className="col-span-3">
                <label className="">Deskripsi produk</label>
                <InputTextArea
                  className="cursor-not-allowed"
                  value={data.description}
                  disabled
                  cols="30"
                  rows="6"
                />
              </div>
            </div>

            <div className="flex justify-between">
              <Button
                data-bs-toggle="modal"
                data-bs-target="#exampleModalCenter"
                className="bg-orange-500 w-full mr-4 hover:bg-orange-600"
              >
                Hapus Produk
              </Button>
              <div className="w-full">
                <Link to={`/products/${productId.toString()}/update`}>
                  <Button className="bg-gogreen w-full mr-0 ml-4">
                    Edit Produk
                  </Button>
                </Link>
              </div>
            </div>
          </div>

          {/* Modal */}
          <ModalConfirm
            actionTitle="Hapus Produk"
            actionClassName={` text-white  bg-[#FD622A]`}
            cancelTitle="Batalkan"
            onAction={handleClickModal}
            disabled={isModalLoading}
          >
            <h1 className="font-bold text-2xl mb-2">Apakah kamu yakin?</h1>
            <p className="text-base">
              Pastikan informasi data produk yang kamu ingin hapus sudah benar
              ya.
            </p>
          </ModalConfirm>
        </>
      )}

      <ModalInfoProduct />
    </>
  );
}
