import React, { useEffect, useState } from 'react'
import {InputText} from '../../components/forms/InputText'
import { InputTextArea } from '../../components/forms/InputTextArea';
import { SelectedBox } from '../../components/forms/SelectedBox';
import { InputTextGroup } from '../../components/forms/InputTextGroup';
import { RectangleUploadImage } from '../../components/forms/RectangleUploadImage';
import iconInfo from '../../assets/icon/info.svg';
import {
  category,
  condition,
  size,
  subcategory1,
  subcategory2Pria,
  subcategory2Wanita,
  subcategory2Anak,
} from "../../datas_statis/products";
import { globalAxiosWithVesion } from "../../config/configApi";
import { NavLink, useNavigate } from "react-router-dom";
import { ModalConfirm } from '../../components/modals/ModalConfirm';
import { ButtonSuccess } from '../../components/ButtonSuccess';
import { closeModal } from '../../utils/modal';
import { useSelector } from "react-redux";
import { ModalInfoProduct } from '../../components/ModalInfoProduct';

export const AddProducts = () => {
    
  const navigate = useNavigate();
  const [isModalLoading, setIsModalLoading] = useState(false);
  const [isBtnDisable, setisBtnDisable] = useState(true);
  const [subcategory2, setSubCategory2] = useState([]);
  
  const photosPattern = [
    {
      id: 0,
      value: "",
    },
    {
      id: 1,
      value: "",
    },
    {
      id: 2,
      value: "",
    },
    {
      id: 3,
      value: "",
    },
  ];
  const [photos, setPhotos] = useState(photosPattern)
  const [data, setData] = useState({
    brand       : "",
    category    : "",
    condition   : "",
    description : "",
    height      : 0,
    material    : "",
    name        : "",
    price       : 0,
    size        : "",
    subcategory1: "",
    subcategory2: "",
    weight      : 0,
    width       : 0,
  });

 const compareValue = (val) => val !== "";

  useEffect(() => {
    
        const getAllProperty = Object.keys(data);

        // Check All Property Not Empty
        const checkEmptyData = getAllProperty.map((property) =>
          compareValue(data[property])
        );

        // Check Photos Not empty
        const checkEmptyPhotos = photos.map(photo => 
          compareValue(photo.value)
        )
        const enableBtn =
          !checkEmptyPhotos.includes(false) && !checkEmptyData.includes(false) ? false : true;
         setisBtnDisable(enableBtn);
       
  }, [data, photos])
  

  useEffect(() => {
     let dataSubCategory = [];
     if(data.category === "pria" && data.subcategory1 !== ""){
      dataSubCategory = subcategory2Pria;
     }
     if(data.category === "wanita" && data.subcategory1 !== ""){
      dataSubCategory = subcategory2Wanita;
     }
     if(data.category === "anak" && data.subcategory1 !== ""){
      dataSubCategory = subcategory2Anak;
     }
     const subcate = dataSubCategory.filter(
       (dt) => dt.type === data.subcategory1
     );
     if(dataSubCategory.length !== 0){
      setSubCategory2(subcate[0].text);
     }
  
  }, [data.category, data.subcategory1])
  


  const handleChange = (e) => {
      setData({...data, [e.target.name] : e.target.value})
  }

  const handleAddPhoto = (imgId, dataImg) => {
    const arrPhotos = photos.map((photo) => {
      if (photo.id === imgId) {
       return {
          id: imgId,
          value: dataImg,
        };
      }
     return photo;
    });
    setPhotos(arrPhotos);
  }

  const handleCancelPhoto = (imgId) => {
    const arrPhotos = photos.map((photo) => {
      if (photo.id === imgId) {
       return {
          id: imgId,
          value: "",
        };
      }
     return photo;
    });
    setPhotos(arrPhotos);
  }

   const user = useSelector((state) => state.user); 
   const handleClickModal = async () => {
     setIsModalLoading(true);

       const formData = new FormData();
       formData.append("sellerId", user.id);
       formData.append("name", data.name);
       formData.append("brand", data.brand);
       formData.append("size", data.size);
       formData.append("price", data.price);
       formData.append("height", data.height);
       formData.append("width", data.weight);
       formData.append("material", data.material);
       formData.append("weight", data.weight);
       formData.append("description", data.description);
       photos.forEach((photo) => {
         formData.append("photos", photo.value);
       });
       formData.append("category", data.category);
       formData.append("condition", data.condition);
       formData.append("subcategory1", data.subcategory1);
       formData.append("subcategory2", data.subcategory2);

       try {
         const response = await globalAxiosWithVesion.postForm(
           `/products`,
           formData
         );
         setIsModalLoading(false);
        //  const id = response.data.data.id;
         navigate(`/products`);
       } catch (error) {
         console.log(error);
         setIsModalLoading(false);
       }
      closeModal();
   };

  return (
    <>
      <p className="capitalize text-lg mb-2">
        <NavLink to="/products" className={"cursor-pointer"}>
          <span className="text-gray-400">katalog produk</span>
        </NavLink>
        <span className="mx-4">/</span>
        <span>tambah produk baru</span>
      </p>
      <div className="bg-white px-8 py-10 rounded-md shadow-md">
        <form>
          {/* Upload Foto Produk */}
          <p>
            Foto produk
            <img
              src={iconInfo}
              className="cursor-pointer inline-block w-5 h-5 ml-1"
              alt=""
              data-bs-toggle={"modal"}
              data-bs-target={"#ModalInfoImageProduct"}
            />
          </p>
          <div className="grid grid-cols-4 gap-x-10 my-3">
            <RectangleUploadImage
              title="Foto Utama"
              imgId={0}
              key={0}
              onChange={(imgId, dataImg) => handleAddPhoto(imgId, dataImg)}
              onCancel={(imgId) => handleCancelPhoto(imgId)}
            />
            <RectangleUploadImage
              title="Detail kondisi 1"
              imgId={1}
              key={1}
              onChange={(imgId, dataImg) => handleAddPhoto(imgId, dataImg)}
              onCancel={(imgId) => handleCancelPhoto(imgId)}
            />
            <RectangleUploadImage
              title="Detail kondisi 2"
              imgId={2}
              key={2}
              onChange={(imgId, dataImg) => handleAddPhoto(imgId, dataImg)}
              onCancel={(imgId) => handleCancelPhoto(imgId)}
            />
            <RectangleUploadImage
              title="Detail bahan"
              imgId={3}
              key={3}
              onChange={(imgId, dataImg) => handleAddPhoto(imgId, dataImg)}
              onCancel={(imgId) => handleCancelPhoto(imgId)}
            />
          </div>

          <div className="grid grid-cols-3 gap-x-12 gap-y-6">
            {/* Nama Brand */}
            <div className="">
              <label className="">Nama brand</label>
              <InputText
                name="brand"
                onChange={handleChange}
                placeholder="Masukkan nama brand"
              />
            </div>

            {/* Nama Produk */}
            <div className="col-span-2">
              <label className="">Nama produk</label>
              <InputText
                name="name"
                onChange={handleChange}
                placeholder="Masukkan nama produk"
              />
            </div>

            {/* Kategori */}
            <div className="col-span-1">
              <label className="">Kategori</label>
              <SelectedBox
                name="category"
                onChange={handleChange}
                default={data.category === ""}
                datas={category}
              />
            </div>

            {/* Sub-kategori 1 */}
            <div className="col-span-1">
              <label className="">Sub-kategori 1</label>
              <SelectedBox
                name="subcategory1"
                default={data.subcategory1 === ""}
                onChange={handleChange}
                datas={subcategory1}
              />
            </div>

            {/* Sub-kategori 2 */}
            <div className="col-span-1">
              <label className="">Sub-kategori 2</label>
              <SelectedBox
                name="subcategory2"
                default={data.subcategory2 === ""}
                onChange={handleChange}
                disabled={subcategory2.length === 0}
                datas={subcategory2.length !== 0 ? subcategory2 : []}
              />
            </div>

            {/* Panjang */}
            <div className="col-span-1">
              <label className="">Panjang</label>
              <InputTextGroup
                name="height"
                onChange={handleChange}
                placeholder="Masukkan panjang"
                type="number"
                icon={"cm"}
                iconposition="right"
              />
            </div>

            {/* Lebar */}
            <div className="col-span-1">
              <label className="">Lebar</label>
              <InputTextGroup
                name="width"
                onChange={handleChange}
                placeholder="Masukkan lebar"
                type="number"
                icon={"cm"}
                iconposition="right"
              />
            </div>

            {/* Berat */}
            <div className="col-span-1">
              <label className="">Berat</label>
              <InputTextGroup
                name="weight"
                onChange={handleChange}
                placeholder="Masukkan berat"
                type="number"
                icon={"gr"}
                iconposition="right"
              />
            </div>

            {/* Kondisi */}
            <div className="col-span-1">
              <label className="">
                Kondisi
                <img
                  src={iconInfo}
                  className="ml-1 cursor-pointer inline-block w-5 h-5"
                  alt=""
                  data-bs-toggle={"modal"}
                  data-bs-target={"#ModalInfoKondisiProduct"}
                />
              </label>
              <SelectedBox
                name="condition"
                onChange={handleChange}
                datas={condition}
                default={data.condition === ""}
              />
            </div>

            {/* Ukuran */}
            <div className="col-span-1">
              <label className="">Ukuran</label>
              <SelectedBox
                default={data.size === ""}
                name="size"
                onChange={handleChange}
                datas={size}
              />
            </div>

            {/* Harga */}
            <div className="col-span-1">
              <label className="">Harga</label>
              <InputTextGroup
                type="number"
                name="price"
                onChange={handleChange}
                placeholder="Masukkan harga"
                iconposition="left"
                icon="Rp."
              />
            </div>

            {/* Nama Bahan produk */}
            <div className="col-span-2">
              <label className="">Bahan produk</label>
              <InputText
                name="material"
                onChange={handleChange}
                placeholder="Masukkan bahan produk"
              />
            </div>

            {/* Deskripsi produk */}
            <div className="col-span-3">
              <label className="">Deskripsi produk</label>
              <InputTextArea
                name="description"
                onChange={handleChange}
                cols="30"
                rows="6"
                placeholder="Masukkan deskripsi produk"
              />
            </div>
          </div>
          <ButtonSuccess
            isBtnDisable={isBtnDisable}
            databstoggle={!isBtnDisable && "modal"}
            databstarget={!isBtnDisable && "#exampleModalCenter"}
            type="button"
            className={` w-full`}
          >
            Tambah Produk Baru
          </ButtonSuccess>
        </form>
      </div>

      {/* Modal  */}
      <ModalConfirm
        actionTitle="Tambah Produk Baru"
        cancelTitle="Batalkan"
        onAction={handleClickModal}
        disabled={isModalLoading}
        actionClassName={` text-white bg-gogreen`}
      >
        <h1 className="font-bold text-2xl mb-2">Apakah kamu yakin?</h1>
        <p className="text-base">
          Pastikan informasi data produk baru yang kamu tambahkan sudah benar
          ya.
        </p>
      </ModalConfirm>

      <ModalInfoProduct />
    </>
  );
}
