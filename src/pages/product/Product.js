import React, { useEffect, useState } from 'react'
import Spinner from '../../components/Spinner'
import ProductCard from '../../components/ProductCard'
import { globalAxiosWithVesion } from '../../config/configApi'
import addPlus from "../../assets/icon/add_plus.svg";
import { useNavigate } from "react-router-dom";
import { Button } from '../../components/Button';
import { MsgDataEmpty } from '../../components/MsgDataEmpty';
import { useSelector } from "react-redux";

export const Product = () => {

  const [productData, setProductData]   = useState("")
  const [isLoadingApi, setIsLoadingApi] = useState(false);
  const [isLoadingLoadMore, setIsLoadingLoadMore] = useState(false);
  const navigate = useNavigate();

  const user = useSelector((state) => state.user); 
  useEffect(() => {
    
    const getProduct = async() => {
        setIsLoadingApi(true);

        try {
          const response = await globalAxiosWithVesion.get(
            `/products?storeId=${user.storeId}&size=6`
          );
          setIsLoadingApi(false);
          setProductData(response.data.data);
        } catch (error) {
          setIsLoadingApi(false);
       }
      }
      getProduct();
  }, [])

  const handleLoadMore = async () => {
    const page = productData.currentPage + 1;
    setIsLoadingLoadMore(true);

    try {
       const response = await globalAxiosWithVesion.get(
         `/products?storeId=${user.storeId}&size=6&page=${page}`
       );
       setIsLoadingLoadMore(false);
       setProductData({
         ...response.data.data,
         products: [...productData.products, ...response.data.data.products],
       });
      } catch (error) {
        console.log(error);
        setIsLoadingLoadMore(false);
    }
  }

  const handleAddProduct = () => {
    navigate("/products/add"); 
  }

  return (
    <>
      <h1 className="text-2xl capitalize mb-1">katalog produk</h1>
      <p className="text-base text-gray-400">Kelola produkmu disini</p>
      <Button onClick={handleAddProduct} className="bg-gogreen">
        <img src={addPlus} alt="" className="h-4 mr-2" />
        <p>Tambah Produk Baru</p>
      </Button>

      {/* Loading Bar Product*/}
      {isLoadingApi && (
        <div className="flex fixed top-0 left-0 right-0 ml-[20%] justify-center h-screen items-center">
          <Spinner className="border-gray-disabled" />
        </div>
      )}

      <div className="flex flex-wrap">
        {productData &&
          !isLoadingApi &&
          productData.products.map((e) => {
            return (
              <ProductCard
                id={e.id}
                key={e.id}
                name={e.name}
                brand={e.brand}
                size={e.size}
                condition={e.condition}
                price={e.price}
                photos={e.photos}
                category={e.category}
              />
            );
          })}

        {productData &&
          !isLoadingApi &&
          productData.currentPage < productData.totalPages - 1 && (
            <button
              className="py-3 bg-gogreen hover:bg-green-600 rounded-lg text-white w-[275px] mx-auto block mt-4"
              onClick={handleLoadMore}
              disabled={isLoadingLoadMore}
            >
              {isLoadingLoadMore ? (
                <Spinner size={"sm"} className={"mx-auto"} />
              ) : (
                "Lihat Lainnya"
              )}
            </button>
          )}

        {productData?.products?.length === 0 && !isLoadingApi && (
          <MsgDataEmpty>
            <p>Productmu masih kosong, Yuk segera lengkapi!</p>
          </MsgDataEmpty>
        )}
      </div>
    </>
  );
}
