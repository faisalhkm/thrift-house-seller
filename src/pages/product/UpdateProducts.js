import React, { useEffect, useState } from 'react'
import {InputText} from '../../components/forms/InputText'
import { InputTextArea } from "../../components/forms/InputTextArea";
import { SelectedBox } from "../../components/forms/SelectedBox";
import { InputTextGroup } from "../../components/forms/InputTextGroup";
import { RectangleUploadImageUpdate } from "../../components/forms/RectangleUploadImageUpdate";
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import { globalAxiosWithVesion } from '../../config/configApi';
import { photosText } from "../../datas_statis/products";
import { ButtonSuccess } from '../../components/ButtonSuccess';
import { ModalConfirm } from '../../components/modals/ModalConfirm';
import { closeModal } from '../../utils/modal';
import {
  category,
  condition,
  size,
  subcategory1,
  subcategory2Pria,
  subcategory2Wanita,
  subcategory2Anak,
} from "../../datas_statis/products";
import { useSelector } from "react-redux";
import iconInfo from "../../assets/icon/info.svg";
import { ModalInfoProduct } from '../../components/ModalInfoProduct';

export const UpdateProducts = () => {

  const [data, setData] = useState({
    brand: "",
    category: "",
    condition: "",
    description: "",
    height: 0,
    material: "",
    name: "",
    price: 0,
    size: "",
    subcategory1: "",
    subcategory2: "",
    weight: 0,
    width: 0,
  });
  const { productId } = useParams();
  const [isModalLoading, setIsModalLoading] = useState(false);
  const [isBtnDisable, setisBtnDisable] = useState(true);
  const [subcategory2, setSubCategory2] = useState([]);
  const navigate = useNavigate();

   const photosPattern = [
     {
       id: 0,
       value: "",
     },
     {
       id: 1,
       value: "",
     },
     {
       id: 2,
       value: "",
     },
     {
       id: 3,
       value: "",
     },
   ];
   const [oldPhotos, setOldPhotos] = useState(photosPattern);
   const [photos, setPhotos] = useState(photosPattern);


   useEffect(() => {
     const getProduct = async () => {

       try {
         const response = await globalAxiosWithVesion.get(
           `/products/${productId}/edit`
         );

         setData(response.data.data);
         const dataApiPhotos = response.data.data.photos;
          const arrPhotos = dataApiPhotos.map((photo, i) => {
              return {
                id: i,
                value: photo,
              };
          });
          setPhotos(arrPhotos);
          setOldPhotos(arrPhotos);
       } catch (error) {
         console.log(error);
       }
     };
     getProduct();
   }, []);
   
    useEffect(() => {
      let dataSubCategory = [];
      if (data.category === "pria" && data.subcategory1 !== "") {
        dataSubCategory = subcategory2Pria;
      }
      if (data.category === "wanita" && data.subcategory1 !== "") {
        dataSubCategory = subcategory2Wanita;
      }
      if (data.category === "anak" && data.subcategory1 !== "") {
        dataSubCategory = subcategory2Anak;
      }
      const subcate = dataSubCategory.filter(
        (dt) => dt.type === data.subcategory1
      );
      if (dataSubCategory.length !== 0) {
        setSubCategory2(subcate[0].text);
      }
    }, [data.category, data.subcategory1]);
  

    const compareValue = (val) => val !== "";

    useEffect(() => {
      const getAllProperty = Object.keys(data);

      // Check All Property Not Empty
      const checkEmptyData = getAllProperty.map((property) =>
        compareValue(data[property])
      );
      setisBtnDisable(checkEmptyData.includes(false));
      
    }, [data]);

const handleAddPhoto = (imgId, dataImg) => {
  const arrPhotos = photos.map((photo) => {
    if (photo.id === imgId) {
      return {
        id: imgId,
        value: dataImg,
      };
    }
    return photo;
  });
  setPhotos(arrPhotos);
};

const handleCancelPhoto = (imgId) => {
  const arrPhotos = photos.map((photo) => {
    if (photo.id === imgId) {
      return {
        id: imgId,
        value: oldPhotos[imgId].value,
      };
    }
    return photo;
  });
  setPhotos(arrPhotos);
};
  const user = useSelector((state) => state.user);     

   const handleClickModal = async () => {
     setIsModalLoading(true);

     const formData = new FormData();
     formData.append("sellerId", user.id);
     formData.append("name", data.name);
     formData.append("brand", data.brand);
     formData.append("size", data.size);
     formData.append("price", data.price);
     formData.append("height", data.height);
     formData.append("width", data.weight);
     formData.append("material", data.material);
     formData.append("weight", data.weight);
     formData.append("description", data.description);

    // Check New Photo
    const checkNewPhoto = photos.filter(
      (photo) => typeof photo.value !== "string"
    );
     if(checkNewPhoto.length !== 0){
      const idChangePhoto = [];
      photos.forEach((photo) => {
        if (typeof photo.value !== "string") {
          formData.append("photos", photo.value);
          idChangePhoto.push(photo.id);
        }
    });
    
     formData.append("changePhoto", idChangePhoto);
     }
     formData.append("category", data.category);
     formData.append("condition", data.condition);
     formData.append("subcategory1", data.subcategory1);
     formData.append("subcategory2", data.subcategory2);

     try {
       const response = await globalAxiosWithVesion.putForm(
         `/products/${productId}`,
         formData
       );
       setIsModalLoading(false);
       //  const id = response.data.data.id;
       navigate(`/products`);
     } catch (error) {
       console.log(error);
       setIsModalLoading(false);
     }
     closeModal();
   };

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  return (
    <>
      <p className="capitalize text-lg mb-2">
        <NavLink to="/products" className={"cursor-pointer"}>
          <span className="text-gray-400">katalog produk</span>
        </NavLink>
        <span className="mx-4">/</span>
        <span>tambah produk baru</span>
      </p>
      <div className="bg-white rounded-md  px-8 py-10 shadow-md">
        <form>
          {/* Upload Foto Produk */}
          <p>
            Foto produk
            <img
              src={iconInfo}
              className="cursor-pointer inline-block w-5 h-5 ml-1"
              alt=""
              data-bs-toggle={"modal"}
              data-bs-target={"#ModalInfoImageProduct"}
            />
          </p>
          <div className="grid grid-cols-4 gap-x-10 my-3">
            {photos.map((photo, i) => (
              <RectangleUploadImageUpdate
                key={i}
                onChange={(imgId, dataImg) => handleAddPhoto(imgId, dataImg)}
                onCancel={(imgId) => handleCancelPhoto(imgId)}
                imgId={i}
                title={photosText[i]}
                value={photo.value}
              />
            ))}
          </div>

          <div className="grid grid-cols-3 gap-x-12 gap-y-6">
            {/* Nama Brand */}
            <div className="">
              <label className="">Nama brand</label>
              <InputText
                name="brand"
                value={data.brand}
                onChange={handleChange}
              />
            </div>

            {/* Nama Produk */}
            <div className="col-span-2">
              <label className="">Nama produk</label>
              <InputText
                name="name"
                value={data.name}
                onChange={handleChange}
              />
            </div>

            {/* Kategori */}
            <div className="col-span-1">
              <label className="">Kategori</label>
              <SelectedBox
                name="category"
                selected={data.category}
                datas={category}
                onChange={handleChange}
              />
            </div>

            {/* Sub-kategori 1 */}
            <div className="col-span-1">
              <label className="">Sub-kategori 1</label>
              <SelectedBox
                name="subcategory1"
                selected={data.subcategory1}
                datas={subcategory1}
                onChange={handleChange}
              />
            </div>

            {/* Sub-kategori 2 */}
            <div className="col-span-1">
              <label className="">Sub-kategori 2</label>
              <SelectedBox
                name="subcategory2"
                selected={data.subcategory2}
                datas={subcategory2}
                onChange={handleChange}
              />
            </div>

            {/* Panjang */}
            <div className="col-span-1">
              <label className="">Panjang</label>
              <InputTextGroup
                name="height"
                onChange={handleChange}
                value={data.height}
                placeholder="Masukkan panjang"
                type="number"
                icon={"cm"}
                iconposition="right"
              />
            </div>

            {/* Lebar */}
            <div className="col-span-1">
              <label className="">Lebar</label>
              <InputTextGroup
                onChange={handleChange}
                name="width"
                value={data.width}
                placeholder="Masukkan lebar"
                type="number"
                icon={"cm"}
                iconposition="right"
              />
            </div>

            {/* Berat */}
            <div className="col-span-1">
              <label className="">Berat</label>
              <InputTextGroup
                name="weight"
                onChange={handleChange}
                value={data.weight}
                placeholder="Masukkan berat"
                type="number"
                icon={"gr"}
                iconposition="right"
              />
            </div>

            {/* Kondisi */}
            <div className="col-span-1">
              <label className="">
                Kondisi
                <img
                  src={iconInfo}
                  className="ml-1 cursor-pointer inline-block w-5 h-5"
                  alt=""
                  data-bs-toggle={"modal"}
                  data-bs-target={"#ModalInfoKondisiProduct"}
                />
              </label>
              <SelectedBox
                onChange={handleChange}
                selected={data.condition}
                name="condition"
                datas={condition}
              />
            </div>

            {/* Ukuran */}
            <div className="col-span-1">
              <label className="">Ukuran</label>
              <SelectedBox
                name="size"
                onChange={handleChange}
                selected={data.size}
                datas={size}
              />
            </div>

            {/* Harga */}
            <div className="col-span-1">
              <label className="">Harga</label>
              <InputTextGroup
                name="price"
                value={data.price}
                onChange={handleChange}
                type="number"
                iconposition="left"
                icon="Rp."
              />
            </div>

            {/* Nama Bahan produk */}
            <div className="col-span-2">
              <label className="">Bahan produk</label>
              <InputText
                name="material"
                value={data.material}
                onChange={handleChange}
              />
            </div>

            {/* Deskripsi produk */}
            <div className="col-span-3">
              <label className="">Deskripsi produk</label>
              <InputTextArea
                name="description"
                onChange={handleChange}
                value={data.description}
                cols="30"
                rows="6"
              />
            </div>
          </div>

          <ButtonSuccess
            disabled={isBtnDisable}
            isBtnDisable={isBtnDisable}
            databstoggle={!isBtnDisable && "modal"}
            databstarget={!isBtnDisable && "#exampleModalCenter"}
            type="button"
            className={` w-full`}
          >
            Perbarui Produk
          </ButtonSuccess>
        </form>
      </div>

      {/* Modal */}
      <ModalConfirm
        actionTitle="Perbarui Produk"
        cancelTitle="Batalkan"
        onAction={handleClickModal}
        disabled={isModalLoading}
        actionClassName={` text-white bg-gogreen`}
      >
        <h1 className="font-bold text-2xl mb-2">Apakah kamu yakin?</h1>
        <p className="text-base">
          Pastikan informasi data produk yang kamu perbarui sudah benar ya.
        </p>
      </ModalConfirm>

      <ModalInfoProduct />
    </>
  );
};
