import axios from "axios";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import TransactionList from "../components/transaction/TransactionList";
import TransactionSearch from "../components/transaction/TransactionSearch";
import Spinner from "../components/Spinner";
import { transactionAdd } from "../action";

const Transaction = () => {
  const [transaction, setTransaction] = useState([]);
  const [status, setStatus] = useState("semua");
  const [isLoading, setIsLoading] = useState(false);
  const [sort, setSort] = useState("dibuat_terbaru");
  const [search, setSearch] = useState("");
  const [loadmoreLoading, setLoadmoreLoading] = useState(false);
  const { id } = useSelector((state) => state.user);

  const dispatch = useDispatch();
  // const id = "84eda57f-2308-42b8-90f2-e6f329dacac2"; // for testing

  // get transaction list
  useEffect(() => {
    const controller = new AbortController();
    setIsLoading(true);
    // note: change to userId get from login & if (userId)
    axios
      .get(
        `https://thrifthouse.herokuapp.com/api/v1/seller/${id}/summary?size=8&page=0&status=${status}&sort_order=${sort}${
          search && "&id_transaksi=" + search
        }`,
        {
          signal: controller.signal,
        }
      )
      .then((res) => {
        setTransaction([res.data.data]);
        setIsLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });

    return () => {
      controller.abort();
    };
  }, [status, sort, search, id]);

  // set store id to reducer
  useEffect(() => {
    if (transaction.length > 0) {
      dispatch(transactionAdd({ storeId: transaction[0].storeId }));
    }
  }, [dispatch, transaction]);

  // handle sub navigation
  const statusHandler = (selectedStatus) => {
    setStatus(selectedStatus);
  };

  // handle loadmore button
  const loadmoreHandler = () => {
    const page = transaction[0].transactionDetails.currentPage + 1;
    setLoadmoreLoading(true);
    axios
      .get(
        `https://thrifthouse.herokuapp.com/api/v1/seller/${id}/summary?size=8&page=${page}&status=${status}&sort_order=${sort}${
          search && "&id_transaksi=" + search
        }`
      )
      .then((res) => {
        setTransaction((prev) => [
          {
            ...res.data.data,
            transactionDetails: {
              ...res.data.data.transactionDetails,
              transactions: [
                ...prev[0].transactionDetails.transactions,
                ...res.data.data.transactionDetails.transactions,
              ],
            },
          },
        ]);
        setLoadmoreLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoadmoreLoading(false);
      });
  };

  return (
    <div className="innertransaction">
      <h3 className="text-2xl font-medium mb-2">Transaksi</h3>
      <p className="text-[#9191A9] mb-4">Kelola transaksimu disini</p>

      <div className="thisissubnav bg-white  rounded-md shadow-md mb-8">
        <ul className="flex">
          <li
            className={`flex-1 py-4 font-medium text-center cursor-pointer border-b ${
              status === "semua"
                ? "text-gogreen border-gogreen"
                : "text-[#AEAEBC] border-[#CFCFCF] hover:text-[#0C0D36]"
            }`}
            onClick={() => statusHandler("semua")}
          >
            Semua
          </li>
          <li
            className={`flex-1 py-4 font-medium text-center cursor-pointer border-b ${
              status === "dikemas"
                ? "text-gogreen border-gogreen"
                : "text-[#AEAEBC] border-[#CFCFCF] hover:text-[#0C0D36]"
            }`}
            onClick={() => statusHandler("dikemas")}
          >
            Dikemas
          </li>
          <li
            className={`flex-1 py-4 font-medium text-center cursor-pointer border-b ${
              status === "dikirim"
                ? "text-gogreen border-gogreen"
                : "text-[#AEAEBC] border-[#CFCFCF] hover:text-[#0C0D36]"
            }`}
            onClick={() => statusHandler("dikirim")}
          >
            Dikirim
          </li>
          <li
            className={`flex-1 py-4 font-medium text-center cursor-pointer border-b ${
              status === "selesai"
                ? "text-gogreen border-gogreen"
                : "text-[#AEAEBC] border-[#CFCFCF] hover:text-[#0C0D36]"
            }`}
            onClick={() => statusHandler("selesai")}
          >
            Selesai
          </li>
          <li
            className={`flex-1 py-4 font-medium text-center cursor-pointer border-b ${
              status === "dibatalkan"
                ? "text-gogreen border-gogreen"
                : "text-[#AEAEBC] border-[#CFCFCF] hover:text-[#0C0D36]"
            }`}
            onClick={() => statusHandler("dibatalkan")}
          >
            Dibatalkan
          </li>
        </ul>

        <div className="thisissearch px-6 py-8">
          <TransactionSearch
            setSort={setSort}
            setSearch={setSearch}
            status={status}
          />
        </div>
      </div>

      <div className="listTransaction ">
        <table className="table-auto w-full ">
          <thead className="text-left">
            <tr>
              <th className="py-4 font-medium text-sm px-6">ID Transaksi</th>
              <th className="py-4 font-medium text-sm px-6">Nama Pembeli</th>
              <th className="py-4 font-medium text-sm px-6">
                Tanggal Pembelian
              </th>
              <th className="py-4 font-medium text-sm px-6">Total Harga</th>
              <th className="py-4 font-medium text-sm px-6">Status</th>
              <th className="py-4 font-medium text-sm px-6">Aksi</th>
            </tr>
          </thead>
          <tbody className="transactiontbody bg-white rounded-md shadow-md">
            {transaction.length === 0 || isLoading ? (
              <tr>
                <td colSpan="6" className="py-6">
                  <Spinner className={"mx-auto"} />
                </td>
              </tr>
            ) : transaction[0].transactionDetails.transactions.length === 0 ? (
              <tr>
                <td colSpan="6" className="py-6 text-center">
                  No transaction found
                </td>
              </tr>
            ) : (
              transaction[0].transactionDetails.transactions.map((item) => (
                <TransactionList
                  key={item.orderId}
                  item={item}
                  setTransaction={setTransaction}
                />
              ))
            )}
          </tbody>
        </table>

        {transaction.length > 0 &&
          transaction[0].transactionDetails.currentPage <
            transaction[0].transactionDetails.totalPages - 1 && (
            <button
              className="py-3 bg-gogreen hover:bg-green-600 rounded-lg text-white w-[275px] mx-auto block mt-4"
              onClick={loadmoreHandler}
              disabled={loadmoreLoading}
            >
              {loadmoreLoading ? (
                <Spinner size={"sm"} className={"mx-auto"} />
              ) : (
                "Lihat Lainnya"
              )}
            </button>
          )}
      </div>
    </div>
  );
};

export default Transaction;
