import React, { useState } from 'react'
import carretDown from "../assets/icon/carret-down.svg";
import carretUp from "../assets/icon/carret-up.svg";
import {subTabData} from "../datas_statis/sales_guide";


export const SalesGuide = () => {

  const [tab, setTab] = useState("tab-1");
  const [subtabActive, setSubtabActive] = useState("");

  const handleChangeTab = (val) => {
    setSubtabActive("");
    setTab(val);
  }
  const handleChangeSubtab = (val) => {
    const activeSubTab = subtabActive === val ? "" : val;
    setSubtabActive(activeSubTab);
  }

  return (
    <>
      <h1 className="capitalize font-medium text-2xl mb-1">Panduan Pengguna</h1>
      <p className="text-gray-400 text-base mb-4">
        Pelajari bagaimana cara kamu mengelola toko di ThriftHouse
      </p>

      {/* Main */}
      <div className="bg-white rounded-md pb-6 shadow-md">
        {/* List Tab */}
        <div className="my-3 ">
          <ul className="flex text-center cursor-pointer">
            <li
              onClick={() => handleChangeTab("tab-1")}
              className={`flex-1 py-4 font-medium  ${
                tab === "tab-1"
                  ? "text-gogreen border-gogreen border-b-2"
                  : "text-lightgray"
              }`}
            >
              Informasi untuk Seller Baru
            </li>
            <li
              onClick={() => handleChangeTab("tab-2")}
              className={`flex-1 py-4 font-medium ${
                tab === "tab-2"
                  ? "text-gogreen border-gogreen border-b-2"
                  : "text-lightgray"
              }`}
            >
              Produk
            </li>
            <li
              onClick={() => handleChangeTab("tab-3")}
              className={`flex-1 py-4 font-medium ${
                tab === "tab-3"
                  ? "text-gogreen border-gogreen border-b-2"
                  : "text-lightgray"
              }`}
            >
              Transaksi
            </li>
          </ul>
        </div>

        {/* List Sub Tab */}
        <div className="p-4 py-1">
          {subTabData[tab].map((dt, i) => {
            return (
              <div
                key={i}
                id={dt.id}
                className="my-3  bg-white font-bold p-2 px-6 text-[#0A0B2D] border-[#8F8F8F] border-[1.5px] rounded-md cursor-pointer"
                onClick={() => handleChangeSubtab(dt.id)}
              >
                <div className="flex justify-between">
                  <p>{dt.title}</p>
                  <img
                    src={subtabActive === dt.id ? carretUp : carretDown}
                    alt=""
                  />
                </div>
                {subtabActive === dt.id && (
                  <div className="mt-3 text-base font-normal">
                    {dt.content}
                  </div>
                )}
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}
