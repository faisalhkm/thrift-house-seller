import axios from "axios";
import { useEffect, useState, Fragment } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Spinner from "../components/Spinner";
import TransactionButton from "../components/transaction/TransactionButton";
import TransactionModalAccept from "../components/transaction/TransactionModalAccept";
import TransactionModalDecline from "../components/transaction/TransactionModalDecline";
import TransactionStatus from "../components/transaction/TransactionStatus";

// calculate products total price - unused due to backend change
// const calculateProductsPrice = (data) => {
//   let total = 0;
//   data.forEach((e) => (total += e.price));
//   return total;
// };

const TransactionDetail = () => {
  const { storeId, transactionId } = useSelector((state) => state.transaction);
  const [isAcceptOpen, setIsAcceptOpen] = useState(false);
  const [isDeclineOpen, setIsDeclineOpen] = useState(false);
  const [response, setResponse] = useState([]);
  const navigate = useNavigate();

  // open close modal if accept button clicked
  const acceptHandler = () => setIsAcceptOpen((prev) => !prev);

  // products price - unused due to backend change
  // const productsPrice = useMemo(() => {
  //   if (response.length > 0)
  //     return calculateProductsPrice(response[0].products);
  // }, [response]);

  // get transaction detail from server
  useEffect(() => {
    if ((storeId, transactionId)) {
      axios
        .get(
          `https://thrifthouse.herokuapp.com/api/v1/seller/${storeId}/transaction/${transactionId}`
        )
        .then((res) => {
          // console.log(res.data.data);
          setResponse([res.data.data]);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      navigate("/transaction");
    }
  }, [storeId, transactionId, navigate, isAcceptOpen, isDeclineOpen]);

  return (
    <div className="innerdetailtransaction">
      <div className="bg-white py-8 px-16">
        {response.length === 0 ? (
          <Spinner className={"mx-auto"} />
        ) : (
          <>
            <div className="text-[#B4B4B4] mb-2">ID pembayaran</div>
            <div className="font-bold text-lg mb-8">
              {response[0].pembayaranId}
            </div>

            <div className="thisispesanan mb-8">
              <div className="font-bold text-lg text-gogreen mb-4">
                Produk Pesanan
              </div>
              <div className="border border-[#CECFD7] rounded-lg grid grid-cols-[50%_auto_auto_auto]">
                <div className="border-b border-b-[#CECFD7] p-4 font-medium text-lg text-[#5D5E79]">
                  Produk
                </div>
                <div className="border-b border-b-[#CECFD7] p-4 font-medium text-lg text-[#5D5E79]">
                  Jumlah
                </div>
                <div className="border-b border-b-[#CECFD7] p-4 font-medium text-lg text-[#5D5E79]">
                  Harga
                </div>
                <div className="border-b border-b-[#CECFD7] p-4 font-medium text-lg text-[#5D5E79]">
                  Harga Total
                </div>

                {response[0].products.map((product) => (
                  <Fragment key={product.productId}>
                    <div className="border-b border-b-[#CECFD7] p-4 text-lg">
                      <div className="flex items-center">
                        <div className="w-9 h-9 bg-gogreen mr-3"></div>
                        <div>{product.productName}</div>
                      </div>
                    </div>
                    <div className="border-b border-b-[#CECFD7] p-4 text-lg">
                      <div className="flex items-center h-full">
                        {product.quantity}
                      </div>
                    </div>
                    <div className="border-b border-b-[#CECFD7] p-4 text-lg">
                      <div className="flex items-center h-full">
                        Rp{product.price.toLocaleString("id-ID")}
                      </div>
                    </div>
                    <div className="border-b border-b-[#CECFD7] p-4 text-lg">
                      <div className="flex items-center h-full">
                        Rp
                        {(product.price * product.quantity).toLocaleString(
                          "id-ID"
                        )}
                      </div>
                    </div>
                  </Fragment>
                ))}

                <div className="col-span-4 p-4">
                  <div className="grid grid-cols-2 max-w-[40%] ml-auto text-right">
                    <div className="text-[#5D5E79] text-left">Subtotal</div>
                    <div className="text-[#5D5E79]">
                      Rp{response[0].price.toLocaleString("id-ID")}
                    </div>
                    <div className="text-[#5D5E79] text-left">
                      Ongkos Pengiriman
                    </div>
                    <div className="text-[#5D5E79]">
                      Rp
                      {response[0].shippingCost.toLocaleString("id-ID")}
                    </div>
                    <div className="font-medium text-lg text-left">
                      Total Harga
                    </div>
                    <div className="font-medium text-lg text-gogreen">
                      Rp{response[0].totalPrice.toLocaleString("id-ID")}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="thisisinfopembelian mb-8">
              <div className="font-bold text-lg text-gogreen mb-4">
                Informasi Pembelian
              </div>

              <div className="flex">
                <div className="grow">
                  <div className="text-[#B4B4B4] mb-2">Nama pelanggan</div>
                  <div className="text-lg">{response[0].buyerName}</div>
                </div>
                <div className="grow">
                  <div className="text-[#B4B4B4] mb-2">Tanggal pembelian</div>
                  <div className="text-lg">{response[0].orderedAt}</div>
                </div>
                <div className="grow">
                  <div className="text-[#B4B4B4] mb-2">Nomor HP</div>
                  <div className="text-lg">{response[0].phoneNumber}</div>
                </div>
                <div className="grow">
                  <div className="mb-4">
                    <div className="text-[#B4B4B4] mb-2">Status pemesanan</div>
                    <TransactionStatus status={response[0].status} />
                  </div>
                  {response[0].packagingStatus && (
                    <div>
                      <div className="text-[#B4B4B4] mb-2">
                        Status pengiriman
                      </div>
                      <TransactionStatus
                        packagingStatus={response[0].packagingStatus}
                      />
                    </div>
                  )}
                </div>
              </div>
            </div>

            <div className="thisisbuktipembayaran mb-8">
              <div className="font-bold text-lg text-gogreen mb-4">
                Bukti Pembayaran
              </div>
              <div className="w-[172px] h-[182px] rounded-lg border border-dashed border-[#AEAEBC] flex justify-center items-center">
                {response[0].receipt ? (
                  <img
                    src={response[0].receipt}
                    alt="Bukti pembayaran"
                    className="w-full h-full object-contain"
                  />
                ) : (
                  <p className="text-[10px]">File Bukti Pembayaran</p>
                )}
              </div>
            </div>

            <div className="thisisinfoalamatpengiriman mb-8">
              <div className="font-bold text-lg text-gogreen mb-4">
                Informasi Alamat Pengiriman
              </div>
              <div className="mb-2">
                <div className="text-[#B4B4B4] mb-2">Alamat</div>
                <div className="text-lg">{response[0].addressDetail}</div>
              </div>
              <div className="flex">
                <div className="grow">
                  <div className="text-[#B4B4B4] mb-2">Provinsi</div>
                  <div className="text-lg">{response[0].province}</div>
                </div>
                <div className="grow">
                  <div className="text-[#B4B4B4] mb-2">Kota/Kabupaten</div>
                  <div className="text-lg">{response[0].city}</div>
                </div>
                <div className="grow">
                  <div className="text-[#B4B4B4] mb-2">Kecamatan</div>
                  <div className="text-lg">{response[0].district}</div>
                </div>
                <div className="grow">
                  <div className="text-[#B4B4B4] mb-2">Kelurahan</div>
                  <div className="text-lg">{response[0].village}</div>
                </div>
                <div className="grow">
                  <div className="text-[#B4B4B4] mb-2">Kode pos</div>
                  <div className="text-lg">{response[0].postalCode}</div>
                </div>
              </div>
            </div>

            <div className="text-right">
              {response[0].status === "Menunggu pembayaran" && (
                <button
                  className={`mr-10 py-[14px] px-[22px] rounded-lg  ${
                    response[0].receipt
                      ? "text-white bg-[#FD622A]"
                      : "text-[#B4B4B4] bg-gogreen-disabled"
                  }`}
                  onClick={() => setIsDeclineOpen((prev) => !prev)}
                  disabled={!response[0].receipt}
                >
                  Tolak Pesanan
                </button>
              )}
              <TransactionButton
                acceptHandler={acceptHandler}
                response={response[0]}
                storeId={storeId}
              />
            </div>

            {/* modals */}
            {isAcceptOpen && (
              <TransactionModalAccept
                setIsAcceptOpen={setIsAcceptOpen}
                response={response[0]}
              />
            )}
            {isDeclineOpen && (
              <TransactionModalDecline setIsDeclineOpen={setIsDeclineOpen} />
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default TransactionDetail;
