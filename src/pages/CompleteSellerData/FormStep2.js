import React, { useState } from 'react'
import { Card } from '../../components/Card';
import jne from "../../assets/img/jne.png";
import posIndonesia from "../../assets/img/pos_indonesia.png";
import tiki from "../../assets/img/tiki.png";
import carretDown from "../../assets/icon/carret-down.svg";
import carretUp from "../../assets/icon/carret-up.svg";

export const FormStep2 = (props) => {

  const [subTextActive, setSubTextActive] = useState({
    jne : false,
    pos_indonesia : false,
    tiki : false,
  });

  const subTexData = {
    jne: [
      {
        title: "JNE Reguler",
        desc: "JNE Reguler adalah jasa kirim dari JNE Express dengan pengiriman ke seluruh Indonesia. JNE Reguler beroperasi setiap hari kecuali hari Minggu dan hari libur nasional.",
      },
      {
        title: "Ongkos Kirim Ekonomis (OKE)",
        desc: "OKE adalah Layanan pengiriman ke seluruh wilayah Indonesia dengan tarif ekonomis dengan perkiraan waktu penyampaian kiriman lebih lama dari Service REGULER. Tergantung pada zona daerah yang menjadi tujuan pengiriman.",
      },
      {
        title: "Yakin Esok Sampai (YES) ",
        desc: "JNE YES (Yakin Esok Sampai) adalah jasa kirim dengan tipe layanan pengiriman Next Day, di mana pesanan akan sampai keesokan hari (termasuk hari Minggu dan hari libur nasional).",
      },
      {
        title: "Super Speed (SS)",
        desc: "SUPER SPEED (SS) adalah layanan pengiriman dengan mengutamakan kecepatan dan penyampaiannya sesuai dengan waktu yang telah ditentukan/disepakati.",
      },
    ],
    pos_indonesia: [
      {
        title: "Pos Kilat Khusus",
        desc: "Pos Kilat Khusus merupakan layanan milik Pos Indonesia untuk pengiriman dengan jangkauan luas ke seluruh wilayah Indonesia.",
      },
    ],
    tiki: [
      {
        title: "Reguler Service (REG)",
        desc: "JNE Reguler adalah jasa kirim dengan pengiriman ke seluruh Indonesia. beroperasi setiap hari kecuali hari Minggu dan hari libur nasional.",
      },
      {
        title: "Over Night Service (ONS)",
        desc: "Over Night Service (ONS) adalah Layanan pengiriman ke seluruh wilayah Indonesia dengan fitur pengiriman hari ini dan esko tiba (Next Day)",
      },
    ],
  };

  const data = [
    {
      img: jne,
      value: "jne",
      title: "Jalur Nugraha Ekakurir (JNE)",
      text: "Ongkos Kirim Ekonomis (OKE) | Layanan Reguler (REG) | Super Speed (SPS) | Yakin Esok Sampai (YES)",
    },
    {
      img: posIndonesia,
      value: "pos_indonesia",
      title: "POS INDONESIA",
      text: "Pos Kilat Khusus (Reguler) - 2-4 hari ",
    },
    {
      img: tiki,
      value: "tiki",
      title: "TIKI",
      text: "Reguler Service (REG) | Over Night Service (ONS)",
    },
  ];

  const handleActiveSubText = (value) => {
    setSubTextActive({...subTextActive, [value] : !subTextActive[value]});
  }

  const handleChange = (e) => props.handleChange(e);

  return (
    <>
      <Card className="mt-2">
        <h1 className="font-bold text-xl">Jasa Pengiriman</h1>
        <p className="text-gray-disabled text-xs mb-4">
          Atur jasa kirim yang kamu inginkan.
        </p>

        {/* Map Radio Button Delivery Service  */}
        {data.map((dt) => (
          <div
            key={dt.value}
            className={`my-4 border ${
              dt.value === props.data.deliveryService
                ? "border-2 border-gogreen shadow-md "
                : " border-gray-disabled"
            } p-4 rounded-md`}
          >
            <div className="flex items-center ">
              <div className="w-12">
                <input
                  type="radio"
                  id={dt.value}
                  className="h-6 w-6 cursor-pointer"
                  name="deliveryService"
                  value={dt.value}
                  checked={dt.value === props.data.deliveryService}
                  onChange={handleChange}
                />
              </div>
              <label
                className="w-full ml-2 flex justify-start items-center cursor-pointer"
                htmlFor={dt.value}
              >
                <div className="w-2/5 flex justify-center">
                  <img src={dt.img} alt="" className="h-20 mr-2 " />
                </div>
                <div className="w-full">
                  <h1 className="font-bold">{dt.title}</h1>
                  <p>{dt.text}</p>
                </div>
              </label>
              <div className="w-1/6 flex justify-center cursor-pointer">
                <img
                  src={subTextActive[dt.value] ? carretUp : carretDown}
                  alt=""
                  onClick={() => handleActiveSubText(dt.value)}
                />
              </div>
            </div>

            {/* Map Sub text devlivery service  */}
            {subTextActive[dt.value] && (
              <div className="border-t-[1.5px] py-2 mt-4">
                {subTexData[dt.value].map((st) => {
                  return (
                    <div className="my-3 text-sm text-[#5D5E79]">
                      <h1 className="font-bold">{st.title}</h1>
                      <p>{st.desc}</p>
                    </div>
                  );
                })}
              </div>
            )}
          </div>
        ))}
      </Card>
    </>
  );
};
