import React, { useState } from 'react'
import { ButtonSuccess } from '../../components/ButtonSuccess';
import { Button } from '../../components/Button';
import { globalAxiosWithVesion } from '../../config/configApi';
import { useNavigate } from "react-router-dom";
import { ModalConfirm } from '../../components/modals/ModalConfirm';
import { closeModal } from '../../utils/modal';
import { useDispatch } from "react-redux";
import { userStore } from "../../action/index";
import { useSelector } from "react-redux";

export const FooterCompleteSellerData = (props) => {
  
 const [isLoading, setIsLoading] = useState(false);
 const navigate = useNavigate();
 const dispatch = useDispatch();
 const user = useSelector((state) => state.user);

  const text = [
    "Informasi Toko dan Seller",
    "Atur Jasa Pengiriman",
    "Atur Rekening",
  ];


  const handleBtnDisable = () => {
   if(props.step === 0) return props.isDisableStep1;
   if(props.step === 1) return props.isDisableStep2;
   if(props.step === 2) return props.isDisableStep3;
  }

  const handleClick = async () => {
     if(props.step < 2){
      props.setStep(props.step >= 2 ? 2 : props.step + 1);
     }  
  }

  const handleClickModal = async () => {
    if(props.step === 2){
     
      const formData = new FormData();
      formData.append("name", props.data.name);
      formData.append("province", props.data.province);
      formData.append("city", props.data.city);
      formData.append("fullAddress", props.data.fullAddress);
      formData.append("ktpImg", props.data.ktpImg);
      formData.append("ktpNumber", props.data.ktpNumber);
      formData.append("deliveryService", props.data.deliveryService);
      formData.append("bankNumber", props.data.bankNumber);
      formData.append("bankName", props.data.bankName);
      formData.append("bankHolder", props.data.bankHolder);
      
      setIsLoading(true);
     try {
       const response = await globalAxiosWithVesion.postForm(
         `/stores/user/${user.id}`,
         formData
         );

         if (response.status === 200) {
          dispatch(userStore({ storeId : response.data.data.id }));
          navigate("/");
         }
           setIsLoading(false);

      } catch (error) {
       setIsLoading(false);
      console.log(error);
     }
     closeModal();
    }
  }

  return (
    <>
      {" "}
      <div className="fixed left-0 bottom-0 flex justify-center w-full z-50 bg-white ">
        <div className="flex justify-between w-4/6 items-center py-3 ">
          <div className="basis-2/5">
            <div className="flex items-center">
              <div className="relative h-8 w-8 text-center rounded-full bg-gogreen text-white">
                <p className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
                  {props.step + 1}
                </p>
              </div>
              <p className="ml-2">{text[props.step]}</p>
            </div>
          </div>

          <div className="basis-2/5 flex justify-end">
            {props.step !== 0 && (
              <Button
                onClick={() =>
                  props.setStep(props.step <= 0 ? 0 : props.step - 1)
                }
                className="mt-0 mb-0 py-2 px-4 border-[1px] border-gogreen text-gogreen rounded-lg mr-5 hover:bg-white"
              >
                Kembali
              </Button>
            )}
            <ButtonSuccess
              isBtnDisable={handleBtnDisable()}
              onClick={handleClick}
              className={`mt-0 mb-0 mr-0 py-2 px-4 `}
              databstoggle={`${props.step === 2 && "modal"}`}
              databstarget={`${props.step === 2 && "#exampleModalCenter"}`}
            >
              Simpan
            </ButtonSuccess>
          </div>
        </div>
      </div>
      <ModalConfirm
        actionTitle="Simpan"
        cancelTitle="Batalkan"
        onAction={handleClickModal}
        disabled={isLoading}
        actionClassName={` text-white bg-gogreen`}
      >
        <h1 className="font-bold text-2xl mb-2">Apakah kamu yakin?</h1>
        <p className="text-base">
          Pastikan informasi baru yang kamu tambahkan sudah benar ya.
        </p>
      </ModalConfirm>
    </>
  );
}
