import React from 'react'
import { Card } from '../../components/Card';
import { InputText } from '../../components/forms/InputText';
import { SelectedBox } from '../../components/forms/SelectedBox';
import { bank } from '../../datas_statis/bank';

export const FormStep3 = (props) => {

  const handleChange = (e) => props.handleChange(e);

  return (
    <>
      <Card className="mt-2">
        <h1 className="font-bold text-xl">Rekening Bank</h1>
        <p className="text-gray-disabled text-xs mb-4">
          Atur rekening bank untuk menyimpan uang hasil penjualanmu.
        </p>

        <div className="">
          <label className="">Pemilik Rekening</label>
          <InputText
            name="bankHolder"
            value={props.data.bankHolder}
            onChange={handleChange}
            placeholder="Masukkan nama pemilik rekening"
          />
          <p className="text-xs mt-1 text-gray-disabled">
            Nama toko dapat diganti di pengaturan akun
          </p>
        </div>

        <div className="mt-4">
          <label className="">No. Rekening</label>
          <p className="text-gray-disabled text-xs mb-1">
            Pastikan nomor rekeningmu benar dan tidak salah ya!
          </p>
          <InputText
            name="bankNumber"
            type="number"
            value={props.data.bankNumber}
            onChange={handleChange}
            placeholder="Masukan nomor rekening"
          />
        </div>

        <div className="mt-4 ">
          <label className="">Bank</label>
          <p className="text-gray-disabled text-xs mb-1">
            Pastikan nomor rekeningmu benar dan tidak salah ya!
          </p>
          <SelectedBox
            className="w-2/5"
            name="bankName"
            default={props.data.bankName === ""}
            selected={props.data.bankName}
            datas={bank}
            onChange={handleChange}
          />
        </div>
      </Card>
    </>
  );
}
