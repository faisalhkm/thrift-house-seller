import React, { useEffect, useState } from "react";
import { LayoutCompleteSellerData } from "../../components/layouts/LayoutCompleteSellerData";
import { FormStep1 } from "./FormStep1";
import { FormStep2 } from "./FormStep2";
import { FormStep3 } from "./FormStep3";
import { StepBar } from "../../components/StepBar";
import { FooterCompleteSellerData } from "./FooterCompleteSellerData";

export const CompleteSellerData = () => {

    const [step, setStep] = useState(0);
    const [isDisableStep1, setIsDisableStep1] = useState(true);
    const [isDisableStep2, setIsDisableStep2] = useState(true);
    const [isDisableStep3, setIsDisableStep3] = useState(true);

    const [data, setData] = useState({
      bankHolder: "",
      bankName: "",
      bankNumber: "",

      deliveryService: "",

      city: "",
      fullAddress: "",
      ktpImg: "",
      ktpNumber: "",
      name: "",
      province: "",
    });

    const [listCity, setListCity] = useState([]);

    const compareValue = (val) => val !== "";

    useEffect(() => {
      if(
        compareValue(data.city) && 
        compareValue(data.fullAddress) && 
        compareValue(data.province) &&
        compareValue(data.ktpImg) &&
        compareValue(data.ktpNumber) &&
        compareValue(data.name) 
      ){
        setIsDisableStep1(false);
      }else{
        if(!isDisableStep1) setIsDisableStep1(true)
      }
      
    }, [data]);

    useEffect(() => {
      if (
        compareValue(data.deliveryService) 
      ) {
        setIsDisableStep2(false);
      } else {
        if (!isDisableStep2) setIsDisableStep2(true);
      }
      
    }, [data]);

    useEffect(() => {
      if (
        compareValue(data.bankHolder) &&
        compareValue(data.bankName) &&
        compareValue(data.bankNumber)
      ) {
        setIsDisableStep3(false);
      } else {
        if (!isDisableStep3) setIsDisableStep3(true);
      }
      
    }, [data]);

    const handleChange = (e) => {
      if (e.target.name === "ktpImg") {
        setData({ ...data, [e.target.name]: e.target.files[0] });
      }else{
        setData({ ...data, [e.target.name]: e.target.value });

      }
    }

    const handleCancelUploadImg = () => {
        setData({ ...data, ["ktpImg"]: "" });
    }

  return (
    <LayoutCompleteSellerData>
      <div className="pt-24 flex justify-center pb-28">
        <div className="w-4/6 min-h-screen ">
          <StepBar className="mb-6 mt-7" step={step} />
          <form>
            {step === 0 && (
              <FormStep1
                listCity={listCity}
                setListCity={(data) => setListCity(data)}
                data={data}
                handleChange={handleChange}
                handleCancelUploadImg={handleCancelUploadImg}
              />
            )}
            {step === 1 && (
              <FormStep2 data={data} handleChange={handleChange} />
            )}
            {step === 2 && (
              <FormStep3 data={data} handleChange={handleChange} />
            )}
          </form>
        </div>
      </div>

      {/* Footer */}
      <FooterCompleteSellerData
        step={step}
        setStep={(step) => setStep(step)}
        isDisableStep1={isDisableStep1}
        isDisableStep2={isDisableStep2}
        isDisableStep3={isDisableStep3}
        data={data}
      />
    </LayoutCompleteSellerData>
  );
}
