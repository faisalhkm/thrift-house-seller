import React, { useState } from 'react'
import { InputText } from '../../components/forms/InputText';
import { InputTextArea } from "../../components/forms/InputTextArea";
import { MdAdd } from "react-icons/md";
import { Card } from '../../components/Card';
import { SelectedBox } from '../../components/forms/SelectedBox';
import {  listprovinsi } from '../../datas_statis/provinsi';
import { globalAxiosWithVesion } from '../../config/configApi';
import buttonCircleClose from "../../assets/icon/button-circle-close.svg";

export const FormStep1 = (props) => {

  const listProvince = listprovinsi.map((dt) => dt.province);

  const provinsiHandler = (provinceId) => {
    globalAxiosWithVesion
      .get(`https://rajaongkir.vercel.app/city?province=${provinceId}`)
      .then((res) => {
        const city_data = res.data.rajaongkir.results.map((dt) => {
          return {
            city_id   : dt.city_id,
            city_name : dt.city_name,
          };
        });
        props.setListCity(city_data);
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  const handleChangeSelectBox = (e) => {
    const getIdProvince = listprovinsi.filter(
      (dt) => dt.province === e.target.value
    );
    props.handleChange(e);
    provinsiHandler(getIdProvince[0].province_id);
  }
  
  const handleChange = (e) => props.handleChange(e);

  return (
    <>
      <Card className="">
        <h1 className="font-bold text-xl mb-2">Informasi Toko</h1>

        <div className="">
          <label className="">Nama Toko</label>
          <InputText
            name="name"
            value={props.data.name}
            onChange={handleChange}
            placeholder="Masukkan nama toko"
          />
          <p className="text-xs mt-1 text-gray-disabled">
            Nama toko dapat diganti di pengaturan akun
          </p>
        </div>

        <div className="mt-4">
          <label className="">Alamat lengkap toko</label>
          <InputTextArea
            name="fullAddress"
            value={props.data.fullAddress}
            onChange={handleChange}
            cols="30"
            rows="4"
            placeholder="Contoh: Perum Griya Asa Blok 5 No.76"
          />
        </div>

        <div className="flex ">
          <div className="mt-4 w-2/5">
            <label className="">Provinsi</label>
            <SelectedBox
              name="province"
              selected={props.data.province}
              datas={listprovinsi.map((dt) => dt.province)}
              onChange={handleChangeSelectBox}
              default={props.data.province === ""}
            />
          </div>
          <div className="mt-4 ml-8 w-2/5">
            <label className="">Kota / Kabupaten</label>
            <SelectedBox
              name="city"
              selected={props.data.city}
              datas={props.listCity.map((dt) => dt.city_name)}
              disabled={props.listCity.length === 0}
              onChange={handleChange}
              default={props.data.city === ""}
            />
          </div>
        </div>
      </Card>

      <Card className="mt-6">
        <h1 className="font-bold text-xl mb-2">Informasi Seller</h1>

        <div className="">
          <label className="">Nomor Kartu Tanda Pengenal (KTP)</label>
          <InputText
            name="ktpNumber"
            value={props.data.ktpNumber}
            type="number"
            onChange={handleChange}
            placeholder="Masukkan nomor KTP kamu"
          />
        </div>

        <div className="mt-4">
          <label className="">
            Unggah file identitas seller (Kartu Tanda Pengenal/KTP)
          </label>
          <div className="rounded-md bg-[#F2F2F2] border-2 border-[#E6E6E6] p-6">
            <p className="mb-6 text-sm">
              Pastikan file yang diunggah berformat .jpg, .jpeg, .pdf atau .png
              dengan ukuran dibawah 10 MB
            </p>
            <div className="relative w-80">
              {props.data.ktpImg !== "" && (
                <>
                  <img
                    src={window.URL.createObjectURL(props.data.ktpImg)}
                    className="h-44 w-80 rounded-md"
                    alt=""
                  />
                  <img
                    onClick={() => props.handleCancelUploadImg()}
                    src={buttonCircleClose}
                    className="absolute -top-[12px] -right-[12px] cursor-pointer"
                    alt=""
                  />
                </>
              )}
              {props.data.ktpImg === "" && (
                <>
                  <label
                    htmlFor="actual-btn"
                    className="h-44 w-80 border-2 rounded-md border-dashed flex justify-center items-center bg-white"
                  >
                    <div className="flex-row">
                      <div className="w-full text-center">
                        <MdAdd size="3em" className="inline" />
                      </div>
                      <p className="text-sm">Tambah File</p>
                    </div>
                  </label>

                  <input
                    type="file"
                    id="actual-btn"
                    name="ktpImg"
                    hidden
                    onChange={handleChange}
                  />
                </>
              )}
            </div>
          </div>
        </div>
      </Card>
    </>
  );
}
