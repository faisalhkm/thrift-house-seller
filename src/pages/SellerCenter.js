import { InlineIcon } from "@iconify/react";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import GuideAccordion from "../components/GuideAccordion";
import { HeaderNotAuth } from "../components/layouts/HeaderNotAuth";
import Footer from "../components/Footer";

const SellerCenter = () => {
  const [openAccordion, setOpenAccordion] = useState(null);

  const handleToogle = (index) => {
    if (openAccordion === index) {
      setOpenAccordion(null);
    } else {
      setOpenAccordion(index);
    }
  };

  return (
    <>
      <div className="px-6 py-4 lg:px-20 lg:py-24 flex flex-col items-center">
        <HeaderNotAuth />
        <div id="home" className="scroll-mt-24 mt-16 lg:mt-0">
          <Link to="/daftar">
            <img alt="banner" src="./images/banner.png" />
          </Link>
        </div>
        <div className="text-center mt-4 lg:mt-6">
          <p className="text-sm font-bold lg:text-xl">Kenapa Berjualan di ThriftHouse ?</p>
          <p className="text-xs text-[#8F8F8F] mt-1 lg:mt-2 lg:text-base">Bergabung bersama kami dan dapatkan berbagai benefit dari Thrifthouse</p>
        </div>
        <div className="flex flex-col items-center lg:mt-6 lg:grid grid-cols-2 xl:grid-flow-col lg:gap-x-10 lg:gap-y-6 lg:max-w-fit">
          <div className="text-center mt-4 flex flex-col items-center max-w-fit lg:mt-0">
            <img className="max-w-[270px]" alt="home-1" src="/images/home-1.png" />
            <p className="text-lg font-bold mt-4">Bebas Biaya Pendaftaran</p>
            <p className="text-sm mt-4 leading-5 max-w-[221px]">Sekarang buka toko di Thrifthouse, bebas biaya pendaftaran loh. Yuk daftar!</p>
          </div>
          <div className="text-center mt-4 flex flex-col items-center max-w-fit lg:mt-0">
            <img className="max-w-[270px]" alt="home-1" src="/images/home-2.png" />
            <p className="text-lg font-bold mt-4">Jangkauan Lebih Luas</p>
            <p className="text-sm mt-4 leading-5 max-w-[221px]">Kamu sekarang bisa menjangkau pasar yang lebih luas bersama ThriftHouse.</p>
          </div>
          <div className="text-center mt-4 flex flex-col items-center max-w-fit lg:mt-0">
            <img className="max-w-[270px]" alt="home-1" src="/images/home-3.png" />
            <p className="text-lg font-bold mt-4">Kelola Produk Mudah</p>
            <p className="text-sm mt-4 leading-5 max-w-[221px]">Kini kelola katalog produk kamu menjadi lebih mudah dengan dashboard seller kami.</p>
          </div>
          <div className="text-center mt-4 flex flex-col items-center max-w-fit lg:mt-0 justify-self-center">
            <img className="max-w-[270px]" alt="home-1" src="/images/home-4.png" />
            <p className="text-lg text font-bold mt-4">Layanan Terpercaya</p>
            <p className="text-sm mt-4 leading-5 max-w-[221px]">Nikmati layanan sellercare setiap hari (08.00-22.00) melalui email dan telepon.</p>
          </div>
        </div>
        <div id="panduan-pengguna" className="flex flex-col mt-10 lg:mt-20 sm:w-4/5 lg:max-w-2xl xl:max-w-4xl scroll-mt-20">
          <div className="text-center w-52 self-center sm:w-full mb-4 lg:mb-6">
            <p className="text-sm font-bold lg:text-xl">Panduan Pengguna</p>
            <p className="text-xs text-[#8f8f8f] lg:text-base mt-1 lg:mt-2">Pelajari lebih lanjut tentang panduan pengguna di sini</p>
          </div>
          <GuideAccordion
            classTitle="text-xs lg:text-base font-bold text-left"
            active={openAccordion === 1}
            onClick={() => handleToogle(1)}
            title="Bagaimana cara berjualan di ThriftHouse?"
            content={
              <div className="mt-2 px-5 py-3 border rounded-lg">
                <p className="font-medium lg:text-base">Informasi apa saja yang harus disiapkan saat akan mendaftar sebagai seller di ThriftHouse?</p>
                <p className="text-[10px] lg:text-sm">
                  Ada beberapa hal yang perlu kamu siapkan untuk daftar jadi penjual di ThriftHouse. Caranya simpel, kamu hanya perlu siapkan nama calon penjual toko online, email aktif, password, nomor handphone aktif dan nama toko online
                  yang akan didaftarkan.
                </p>
                <p className="font-medium lg:text-base mt-3">No HP yang digunakan adalah Nomor Pribadi atau Perusahaan?</p>
                <p className="text-[10px] lg:text-sm">
                  Jika kamu mendaftar toko online untuk seller perorangan, gunakan nomor handphone pribadi kamu. Tapi, jika kamu merupakan calon seller dari perusahaan, maka gunakan nomor handphone penanggung jawab. Pada saat pendaftaran
                  toko online, pastikan nomor handphone yang didaftarkan aktif dan sesuai. Nanti, kamu akan mendapatkan kode verifikasi pendaftaran toko online.
                </p>
                <p className="font-medium lg:text-base mt-3">Apa saja dokumen yang dibutuhkan untuk menjadi seller di ThriftHouse?</p>
                <p className="text-[10px] lg:text-sm">Kamu hanya perlu mempersiapkan nomor rekening penjual yang valid dan data diri berupa Kartu Tanda Penduduk (KTP)</p>
              </div>
            }
          />
          <GuideAccordion
            classTitle="text-xs lg:text-base font-bold text-left"
            active={openAccordion === 2}
            onClick={() => handleToogle(2)}
            title="Apakah penjualan di Thrifthouse dipungut biaya?"
            content={
              <div className="mt-2 px-5 py-3 border rounded-lg">
                <p className="text-[10px] lg:text-sm">Jika kamu membuka toko online di Thrifthouse kamu berkesempatan untuk membuka toko online kamu secara gratis, alias tidak dipungut biaya apapun.</p>
              </div>
            }
          />
          <GuideAccordion
            classTitle="text-xs lg:text-base font-bold text-left"
            active={openAccordion === 3}
            onClick={() => handleToogle(3)}
            title="Apakah setelah menjadi seller di ThriftHouse saya bisa langsung berjualan?"
            content={
              <div className="mt-2 px-5 py-3 border rounded-lg">
                <p className="text-[10px] lg:text-sm">
                  Ya, kamu bisa langsung mengakses akun ThriftHouse Seller Center kamu. Namun, sebelum menjual produk-produk kamu di ThriftHouse, lengkapi informasi toko online kamu terlebih dahulu seperti nomor rekening yang valid dan data
                  diri berupa KTP. Setelah itu, kamu sudah bisa langsung menambahkan dan mengelola produk baru kamu dan mulai berjualan.
                </p>
              </div>
            }
          />
          <GuideAccordion
            classTitle="text-xs lg:text-base font-bold text-left"
            active={openAccordion === 4}
            onClick={() => handleToogle(4)}
            title="Apakah ada sistem bagi hasil di ThriftHouse?"
            content={
              <div className="mt-2 px-5 py-3 border rounded-lg">
                <p className="text-[10px] lg:text-sm">Sistem ThriftHouse akan otomatis menarik biaya sebesar 6% (enam persen) dari total setiap transaksi jual-beli tersebut.</p>
              </div>
            }
          />
        </div>
        <div id="bantuan" className="flex flex-col mt-10 lg:mt-20 max-w-sm lg:min-w-[632px] scroll-mt-20">
          <div className="text-center self-center mb-4 lg:mb-6">
            <p className="text-sm font-bold lg:text-xl">Butuh Bantuan ?</p>
            <p className="text-xs text-[#8f8f8f] lg:text-base mt-1 lg:mt-2">Kamu dapat menghubungi melalui:</p>
          </div>
          <div className="flex flex-col lg:flex-row gap-3 lg:gap-8">
            <div className="flex items-center gap-4 text-white bg-[#0A0B2D] rounded-lg py-8 lg:py-10 px-6 lg:flex-col lg:basis-1/2">
              <InlineIcon icon="uiw:mail" className="lg:w-8 lg:h-8" />
              <p className="lg:text-lg">Sellercare@thrifthouse.com</p>
            </div>
            <div className="flex items-center gap-4 text-white bg-[#0A0B2D] rounded-lg py-8 lg:py-10 px-6 lg:flex-col lg:basis-1/2">
              <InlineIcon icon="ri:customer-service-fill" className="lg:w-8 lg:h-8" />
              <p className="lg:text-lg">+62 821 7362 7261</p>
            </div>
          </div>
        </div>
        <div id="mulai-jualan" className="flex flex-col mt-10 lg:mt-20 w-full scroll-mt-20">
          <div className="text-center self-center mb-4 lg:mb-6">
            <p className="text-sm font-bold lg:text-xl">Mulai Jualan</p>
            <p className="text-xs text-[#8f8f8f] lg:text-base mt-1 lg:mt-2">Mari bergabung bersama kami sekarang juga</p>
          </div>
          <img alt="mask" src="./images/mask.png" className="w-[95%] max-w-[300px] self-center" />
          <div className="h-[100px] bg-[#29A867] rounded-lg w-full max-w-[716px] self-center relative overflow-hidden">
            <img alt="vector-1" src="./images/Vector-1.png" className="absolute top-0 right-0" />
            <img alt="vector-2" src="./images/Vector-2.png" className="absolute bottom-0 left-0" />
            <div className="flex flex-col lg:flex-row items-center justify-center gap-3 h-full">
              <p className="text-sm text-white z-20 text-center">Yuk daftar sebagai seller di ThriftHouse!</p>
              <Link to="/daftar" className="bg-white text-xs text-[#4DB680] py-[10px] px-3 rounded-lg z-20">
                Daftar sebagai Seller
              </Link>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default SellerCenter;
