import React, { useEffect, useState } from "react";
import imgLogin from "../assets/img/img-login.png";
import { HeaderNotAuth } from "../components/layouts/HeaderNotAuth";
import { globalAxios } from "../config/configApi";
import { Link, useNavigate} from "react-router-dom";
import { InputText } from "../components/forms/InputText";
import { ButtonSuccess } from "../components/ButtonSuccess";
import { AiOutlineEyeInvisible, AiOutlineEye } from "react-icons/ai";
import { useDispatch } from "react-redux";
import { userLogin } from "../action/index";

export const Login = () => {

 const [data, setData]   = useState({
  username : "",
  password : "",
 });
 const [isLoading, setIsLoading]          = useState(false);
 const [isLoginfail, setIsLoginfail]      = useState(false)
 const [isBtnDisable, setisBtnDisable]    = useState(true);
 const [isShowPassword, setIsShowPassword] = useState(false);
 const dispatch = useDispatch();
 
 const navigate = useNavigate();

  useEffect(() => {
    if (
      data.username !== "" &&
      data.password !== ""
    ) {
      setisBtnDisable(false);
    }else{
      setisBtnDisable(true);
    }
   
  }, [data]);
 const handleChange = (e) => {
  if(isLoginfail) setIsLoginfail(false);
  setData({...data, [e.target.name] : e.target.value})
 }

 const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    setIsLoginfail(false);
    try {
      const response = await globalAxios.post("/login", data);
      if (
        response.data.hasOwnProperty("access_token") &&
        response.data.role === "ROLE_SELLER"
      ) {
        if (response.data.hasOwnProperty("store_id")) {
          dispatch(userLogin({ ...response.data, storeId: response.data.store_id }));
          navigate("/");
          
        } else {
          dispatch(userLogin(response.data));
          navigate("/complete-seller-data");
        }
      }
      setIsLoading(false);
      setIsLoginfail(true);
    } catch (error) {
      setIsLoading(false);
      setIsLoginfail(true)
        console.log(error);
    }
 }

 return (
   <>
     <header>
       <HeaderNotAuth />
     </header>

     <form onSubmit={(e) => handleSubmit(e)}>
       <div className="flex justify-center items-center px-16 pt-10 min-h-screen bg-[#F2F2F2]">
         <div className="flex-1 flex justify-center">
           <div className="w-5/6 mt-1">
             <img src={imgLogin} alt="" />
           </div>
         </div>
         <div className="flex-1">
           <div className="w-5/6 bg-white  border px-8 shadow-md rounded-xl py-6">
             <div>
               <div className="text-center mt-4">
                 <h1 className="font-bold text-[#29A867] text-3xl">
                   Masuk ke akun sellermu
                 </h1>
                 <p className="text-slate-400 text-sm mt-1 mb-6 sm:text-lg">
                   Masukkan data untuk melanjutkan
                 </p>
               </div>

               <div>
                 <label className="" htmlFor="username">
                   <span className="text-sm leading-5 mb-1 block">
                     Username
                   </span>
                 </label>
                 <InputText
                   placeholder="Masukkan username kamu"
                   name="username"
                   value={data.username}
                   onChange={(e) => handleChange(e)}
                 />
               </div>

               <div className="mt-4">
                 <label className="" htmlFor="password">
                   <span className="text-sm leading-5 mb-1 block">
                     Kata Sandi
                   </span>
                 </label>
                 <div className="relative">
                   <InputText
                     type={isShowPassword ? "text" : "password"}
                     name="password"
                     placeholder="Masukkan kata sandi kamu"
                     value={data.password}
                     onChange={(e) => handleChange(e)}
                   />

                   {isShowPassword ? (
                     <AiOutlineEye
                       className="absolute top-[4px] right-[8px] text-gray-400 hover:text-gray-600 cursor-pointer"
                       size={"28px"}
                       onClick={() => setIsShowPassword(!isShowPassword)}
                     />
                   ) : (
                     <AiOutlineEyeInvisible
                       className="absolute top-[4px] right-[8px] text-gray-400 hover:text-gray-600 cursor-pointer"
                       size={"28px"}
                       onClick={() => setIsShowPassword(!isShowPassword)}
                     />
                   )}
                 </div>
               </div>
             </div>
             <div className="flex flex-col mt-10 lg:mt-1">
               <ButtonSuccess
                 type="submit"
                 isBtnDisable={isBtnDisable}
                 isLoading={isLoading || isLoading}
                 className={"mt-6 w-full"}
               >
                 <p className="inline-block"> Masuk</p>
               </ButtonSuccess>
             </div>
             <p className="text-gray-disabled">
               Belum jadi Seller ThriftHouse?
               <span className="text-gogreen ml-2 font-medium">
                 <Link to={"/daftar"}>Daftar Disini</Link>
               </span>
             </p>
           </div>
         </div>
       </div>
     </form>

     {/* Message Error When Login fail */}
     {isLoginfail && (
       <div className="absolute top-24 left-0 flex justify-center w-full text-sm">
         <div className="px-5 py-[5px] rounded-3xl  text-center text-[#D8421D] bg-[#FDEBD3] border-[#D8421D] border mb-2 font-medium capitalize">
           <p>Username / Password yang anda masukkan salah</p>
         </div>
       </div>
     )}
   </>
 );
}
