// import axios from "axios";

const userLogin = (payload) => ({
  type: "user/login",
  payload,
});
const userStore = (payload) => ({
  type: "user/store",
  payload,
});

const userLogout = () => ({
  type: "user/logout",
});

const transactionAdd = (payload) => ({
  type: "transaction/add",
  payload,
});

export { userLogin, userLogout, userStore, transactionAdd };
