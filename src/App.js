import "tw-elements";
import "./index.css";
import React from "react";
import { Route, Routes } from "react-router-dom";
import Transaction from "./pages/Transaction";
import TransactionDetail from "./pages/TransactionDetail";
import useScrollToTop from "./utils/useScrollToTop";
import PengaturanToko from "./pages/PengaturanToko";
import { Home } from "./pages/Home";
import { Dashboard } from "./components/layouts/Dashboard";
import { Product } from "./pages/product/Product";
import { AddProducts } from "./pages/product/AddProducts";
import { ProductDetail } from "./pages/product/ProductDetail";
import { Login } from "./pages/Login";
import { Register } from "./pages/Register";
import { CompleteSellerData } from "./pages/CompleteSellerData";
import { UpdateProducts } from "./pages/product/UpdateProducts";
import ProtectedRoute from "./components/ProtectedRoute";
import MyAccount from "./components/MyAccount";
import SellerCenter from "./pages/SellerCenter";
import ProtectedRouteCompleteSeller from "./components/ProtectedRouteCompleteSeller";
import { SalesGuide } from "./pages/SalesGuide";

function App() {
  useScrollToTop();

  return (
    <>
      <Routes>
        <Route element={<ProtectedRoute />}>
          <Route path="/" element={<Dashboard />}>
            <Route index element={<Home />} />
            <Route path="/products" element={<Product />} />
            <Route path="/products/add" element={<AddProducts />} />
            <Route
              path="/products/:productId/update"
              element={<UpdateProducts />}
            />
            <Route path="/products/:productId" element={<ProductDetail />} />
            <Route path="/transaction" element={<Transaction />} />
            <Route path="/transaction/detail" element={<TransactionDetail />} />
            <Route path="/account" element={<MyAccount />} />
            <Route path="/pengaturan" element={<PengaturanToko />} />
            <Route path="/sales-guide" element={<SalesGuide />} />
          </Route>
        </Route>

        <Route element={<ProtectedRouteCompleteSeller />}>
          <Route
            path="/complete-seller-data"
            element={<CompleteSellerData />}
          />
        </Route>

        <Route path="/login" element={<Login />} />
        <Route path="/daftar" element={<Register />} />
        <Route path="/sellercenter" element={<SellerCenter />} />
        {/* <Route path="*" element={<ErrorPage />} /> ini blom */}
      </Routes>
    </>
  );
}

export default App;
