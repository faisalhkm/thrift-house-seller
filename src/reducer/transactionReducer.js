const initialState = {
  storeId: "",
  transactionId: "",
};

export default function transactionReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "transaction/add":
      return {
        ...state,
        ...payload,
      };
    default:
      return state;
  }
}
