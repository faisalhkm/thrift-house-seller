import { combineReducers } from "redux";
import userReducer from "./userReducer";
import transactionReducer from "./transactionReducer";

const allReducer = combineReducers({
  user: userReducer,
  transaction: transactionReducer,
});

export default allReducer;
