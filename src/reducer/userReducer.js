const initialState = {};

export default function userReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "user/login":
      return {
        ...state,
        ...payload,
      };
    case "user/store":
      return {
        ...state,
        ...payload,
      };

    case "user/logout":
      return {};
    default:
      return state;
  }
}
