import axios from "axios";

export const globalAxios = axios.create({
  baseURL: "https://thrifthouse.herokuapp.com:443",
});

export const globalAxiosWithVesion = axios.create({
  baseURL:"https://thrifthouse.herokuapp.com:443/api/v1",
});
