module.exports = {
  content: [
    "./src/**/*.{html,js}",
    "./node_modules/tw-elements/dist/js/**/*.js",
  ],
  plugins: [require("tw-elements/dist/plugin")],
  theme: {
    extend: {
      fontFamily: {
        poppins: ["Poppins"],
      },
      spacing: {
        100: "30rem",
        99: "50rem",
        wbanner: "1200px",
        hbanner: "414px",
      },
      colors: {
        secondary: "#29A867",
        oxford: "#0C0D36",
        gogreen: "#4DB680",
        "gogreen-hover": "#29A867",
        "gogreen-pressed": "#228C56",
        "gogreen-disabled": "#F2F2F2",
        background: "#F2F2F2",
        lightgray: "#AEAEBC",
        "black-rgba": "rgba(0, 0, 0, 0.54)",
        "gray-disabled": "#CFCFCF",
      },
    },
  },
};
